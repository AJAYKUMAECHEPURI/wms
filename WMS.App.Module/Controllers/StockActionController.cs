﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using WMS.App.Module.Actions;
using WMS.App.Module.BusinessObjects.Stock;

namespace WMS.App.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class StockActionController : CustomBaseController
    {
        public StockActionController()
        {
            InitializeComponent();
            TargetObjectType = typeof(StockTransfer);
            TargetViewType = ViewType.DetailView;
            SimpleAction transferAction = new(
          this, "StockTransferAction", PredefinedCategory.View)
            {
                Caption = "Transfer"
            };
            transferAction.Execute += StockTransfer_Execute;
            Actions.Add(transferAction);
        }

        private void StockTransfer_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            StockTransfer stockTransfer = View.CurrentObject as StockTransfer;
            bool isValid = ValidateTransfer(stockTransfer);
            if (!isValid)
            {
                ShowErrorMessage("Input is wrong");
                return;
            }
            CreateTransaction(stockTransfer);
        }

        private void CreateTransaction(StockTransfer stockTransfer)
        {
            try
            {
                StockTransaction stockIn = View.ObjectSpace.CreateObject<StockTransaction>();
                stockIn.TransactionType = BusinessObjects.System.Helpers.StockConfig.TransactionType.StockAdjustmentIn;
                stockIn.Variant = stockTransfer.Variant;
                stockIn.Quantity = stockTransfer.Quantity;
                stockIn.BatchNumber = stockTransfer.BatchNumber;
                stockIn.SerialNumber = stockTransfer.SerialNumber;
                stockIn.Location = stockTransfer.Location;

                StockTransaction stockOut = View.ObjectSpace.CreateObject<StockTransaction>();
                stockOut.TransactionType = BusinessObjects.System.Helpers.StockConfig.TransactionType.StockAdjustmentOut;
                //stockOut.Variant = stockTransfer.From.Variant;
                //stockOut.BatchNumber = stockTransfer.From.BatchNumber;
                //stockOut.SerialNumber = stockTransfer.From.SerialNumber;
                //stockOut.Quantity = stockTransfer.Quantity;
                //stockOut.Location = stockTransfer.From.Location;

                using IStockAction stockAction = new StockAction(View.ObjectSpace);
                stockAction.CreateTransaction(stockOut);
                stockAction.CreateTransaction(stockIn);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool ValidateTransfer(StockTransfer stockTransfer)
        {
            if (stockTransfer.Variant == null || stockTransfer.Quantity <= 0 || string.IsNullOrEmpty(stockTransfer.SerialNumber) || string.IsNullOrEmpty(stockTransfer.BatchNumber))
            {
                return false;
            }
            return true;
        }

        protected override void OnActivated()
        {
            base.OnActivated();
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
        }
    }
}