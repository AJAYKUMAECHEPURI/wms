﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using WMS.App.Module.BusinessObjects.Stock;

namespace WMS.App.Module.Controllers
{
    public partial class StockItemController : ObjectViewController<ListView, StockItem>
    {
        private SimpleAction transferAction;
        private SimpleAction issueAction;
        private SimpleAction receiveAction;

        public StockItemController()
        {
            InitializeComponent();
            transferAction = new(
           this, "TransferAction", PredefinedCategory.View)
            {
                Caption = "Transfer"
            };
            transferAction.Execute += StockTransfer_Execute;
            Actions.Add(transferAction);
            transferAction.SelectionDependencyType = SelectionDependencyType.RequireSingleObject;

            issueAction = new(
          this, "IssueAction", PredefinedCategory.View)
            {
                Caption = "Issue"
            };
            issueAction.Execute += StockIssue_Execute;
            Actions.Add(issueAction);
            issueAction.SelectionDependencyType = SelectionDependencyType.RequireSingleObject;

            receiveAction = new(
          this, "ReceiveAction", PredefinedCategory.View)
            {
                Caption = "Receipt"
            };
            receiveAction.Execute += Receipt_Execute;
            Actions.Add(receiveAction);
            receiveAction.SelectionDependencyType = SelectionDependencyType.RequireSingleObject;
        }

        private void Receipt_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                StockItem currentObject = View.CurrentObject as StockItem;
                if (currentObject == null)
                {
                    ShowErrorMessage("Select one stock Item");
                    return;
                }

                StockReceipt stockReceive = ObjectSpace.CreateObject<StockReceipt>();
                stockReceive.Variant = currentObject.Variant;
                stockReceive.BatchNumber = currentObject.BatchNumber;
                stockReceive.SerialNumber = currentObject.SerialNumber;
                stockReceive.Location = currentObject.Location;

                View view = e.ShowViewParameters.CreatedView = Application.CreateDetailView(View.ObjectSpace, stockReceive, View);
                e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void StockIssue_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                StockItem currentObject = View.CurrentObject as StockItem;
                if (currentObject == null)
                {
                    ShowErrorMessage("Select one stock Item");
                    return;
                }
                else
                {
                    StockIssue stockIssue = ObjectSpace.CreateObject<StockIssue>();
                    stockIssue.Variant = currentObject.Variant;
                    stockIssue.BatchNumber = currentObject.BatchNumber;
                    stockIssue.SerialNumber = currentObject.SerialNumber;
                    stockIssue.Location = currentObject.Location;

                    View view = e.ShowViewParameters.CreatedView = Application.CreateDetailView(View.ObjectSpace, stockIssue, View);
                    e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void StockTransfer_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                StockItem currentObject = e.CurrentObject as StockItem;
                if (currentObject == null)
                {
                    ShowErrorMessage("Select one stock Item");
                    return;
                }
                else
                {
                    IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(StockTransaction));

                    StockTransfer stockTransfer = objectSpace.CreateObject<StockTransfer>();
                    //stockTransfer.Variant = currentObject.Variant;
                    //stockTransfer.BatchNumber = currentObject.BatchNumber;
                    //stockTransfer.SerialNumber = currentObject.SerialNumber;
                    //stockTransfer.Location = currentObject.Location;
                    //stockTransfer.Quantity = currentObject.Quantity;



                    View view = e.ShowViewParameters.CreatedView = Application.CreateDetailView(objectSpace, stockTransfer, View);
                    e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
                }
            }
            catch (Exception)
            {
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
        }

        private void ShowErrorMessage(string message)
        {
            MessageOptions options = new MessageOptions
            {
                Duration = 2000,
                Message = message,
                Type = InformationType.Error
            };
            Application.ShowViewStrategy.ShowMessage(options);
        }
    }
}