﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Blazor.Editors;

namespace WMS.App.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CustomizeLookupListViewController : ViewController<DetailView>
    {
        public CustomizeLookupListViewController()
        {
            InitializeComponent();
            TargetViewId = "Customer_DetailView;CustomerAddress_DetailView;CustomerAnalysis_DetailView;CustomerContact_DetailView;CustomerContactMarketing_DetailView;CustomerContactWebShop_DetailView;CustomerCreditDetails_DetailView;CustomerCreditDetailsGeneral_DetailView;CustomerCreditDetailsMore_DetailView;CustomerCreditDetailsStoredCards_DetailView;CustomerForecast_DetailView;CustomerProduct_DetailView;CustomerProfile_DetailView;CustomerSettingAccount_DetailView;CustomerSettingDespatch_DetailView;CustomerSettingInvoicing_DetailView;CustomerSettingLoyaltyScheme_DetailView;CustomerSettings_DetailView;CustomerSettingSales_DetailView;CustomerSpecialPrice_DetailView";
        }

        protected override void OnActivated()
        {
            base.OnActivated();

            View.CustomizeViewItemControl<LookupPropertyEditor>(this, e =>
            {
                e.HideNewButton();
                e.HideEditButton();
            });
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
        }
    }
}