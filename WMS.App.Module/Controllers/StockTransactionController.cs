﻿using DevExpress.ExpressApp;
using WMS.App.Module.Actions;
using WMS.App.Module.BusinessObjects.Stock;

namespace WMS.App.Module.Controllers
{
    public partial class StockTransactionController : ViewController<DetailView>
    {
        public StockTransactionController()
        {
            InitializeComponent();
            //TargetObjectType = typeof(StockTransaction);
            //TargetObjectType = typeof(StockIssue);
            //TargetObjectType = typeof(StockReceipt);
            TargetObjectType = typeof(StockTransfer);
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            View.ObjectSpace.Committing += ObjectSpace_Committing;
            View.ObjectSpace.CustomCommitChanges += ObjectSpace_CustomCommitChanges;
            View.ObjectSpace.ObjectSaving += ObjectSpace_Saving;
        }

        private void ObjectSpace_CustomCommitChanges(object sender, System.ComponentModel.HandledEventArgs e)
        {
            e.Handled = true;
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            View.ObjectSpace.Committing -= ObjectSpace_Committing;
            View.ObjectSpace.CustomCommitChanges -= ObjectSpace_CustomCommitChanges;
            View.ObjectSpace.ObjectSaving -= ObjectSpace_Saving;

        }

        private void ObjectSpace_Committing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                //if (View.ObjectSpace.IsModified)
                StockTransaction stockTransaction = View.CurrentObject as StockTransaction;

                if (stockTransaction.TransactionType == BusinessObjects.System.Helpers.StockConfig.TransactionType.StockTransfer)
                {
                    StockTransfer stockTransfer = View.CurrentObject as StockTransfer;
                }

                if (stockTransaction.TransactionType == BusinessObjects.System.Helpers.StockConfig.TransactionType.StockAdjustmentOut)
                {
                    StockReceipt stockReceipt = View.CurrentObject as StockReceipt;
                }

                if (stockTransaction.TransactionType == BusinessObjects.System.Helpers.StockConfig.TransactionType.StockAdjustmentIn)
                {
                    StockIssue stockIssue = View.CurrentObject as StockIssue;
                }

                //Make respective changes accoring to the logic
                if (ObjectSpace.IsNewObject(stockTransaction))
                {
                    using IStockAction stockAction = new StockAction(View.ObjectSpace);
                    stockAction.CreateTransaction(stockTransaction);
                }
                else
                {
                    ShowErrorMessage("Transaction can't be modified");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void ShowErrorMessage(string message)
        {
            MessageOptions options = new MessageOptions
            {
                Duration = 2000,
                Message = message,
                Type = InformationType.Error
            };
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ObjectSpace_Saving(object sender, System.EventArgs e)
        {
            // Custom logic before saving
        }
    }
}