﻿namespace WMS.App.Module.Models
{
    public class ResponseModel
    {
        public bool Result { get; set; }
        public string Message { get; set; }
    }
}