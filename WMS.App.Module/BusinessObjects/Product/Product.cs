﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.ProductVariant;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class Product : CustomBaseObject
    {
        public Product(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.Setting = new ProductSetting(Session);
            this.Matrix = new ProductMatrix(Session);
            this.More = new ProductMore(Session);
            this.Analysis = new ProductAnalysis(Session);
            this.AmazonListing = new ProductAmazonListing(Session);
        }

        private ProductAmazonListing amazonListing;
        private ProductAnalysis analysis;
        private ProductMore more;
        private ProductMatrix productMatrix;
        private ProductSetting setting;
        private string commodityCode;
        private string comments;
        private string abbreviatedDescription;
        private string description;
        private string code;

        [Size(SizeAttribute.DefaultStringMappingFieldSize), RuleRequiredField("Product.Code", DefaultContexts.Save, "Code Should not be Empty")]
        public string Code
        {
            get => code;
            set => SetPropertyValue(nameof(Code), ref code, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AbbreviatedDescription
        {
            get => abbreviatedDescription;
            set => SetPropertyValue(nameof(AbbreviatedDescription), ref abbreviatedDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CommodityCode
        {
            get => commodityCode;
            set => SetPropertyValue(nameof(CommodityCode), ref commodityCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Comments
        {
            get => comments;
            set => SetPropertyValue(nameof(Comments), ref comments, value);
        }

        [DevExpress.Xpo.Aggregated]
        [VisibleInListView(false)]
        public ProductSetting Setting
        {
            get => setting;
            set => SetPropertyValue(nameof(Setting), ref setting, value);
        }

        [DevExpress.Xpo.Aggregated]
        [VisibleInListView(false)]
        public ProductMatrix Matrix
        {
            get => productMatrix;
            set => SetPropertyValue(nameof(Matrix), ref productMatrix, value);
        }

        [DevExpress.Xpo.Aggregated]
        [VisibleInListView(false)]
        public ProductMore More
        {
            get => more;
            set => SetPropertyValue(nameof(More), ref more, value);
        }

        [DevExpress.Xpo.Aggregated]
        [VisibleInListView(false)]
        public ProductAnalysis Analysis
        {
            get => analysis;
            set => SetPropertyValue(nameof(Analysis), ref analysis, value);
        }

        [DevExpress.Xpo.Aggregated]
        [VisibleInListView(false)]
        public ProductAmazonListing AmazonListing
        {
            get => amazonListing;
            set => SetPropertyValue(nameof(AmazonListing), ref amazonListing, value);
        }

        [Association("Product-Variants"), Aggregated]
        public XPCollection<Variant> Variant
        {
            get
            {
                return GetCollection<Variant>(nameof(Variant));
            }
        }
    }
}