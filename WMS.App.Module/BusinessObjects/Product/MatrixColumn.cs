﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class MatrixColumn : CustomBaseObject
    {
        public MatrixColumn(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private ProductMatrix productMatrix;
        private int sortOrder;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public int SortOrder
        {
            get => sortOrder;
            set => SetPropertyValue(nameof(SortOrder), ref sortOrder, value);
        }

        [Association("ProductMatrix-MatrixColumn")]
        public ProductMatrix ProductMatrix
        {
            get => productMatrix;
            set => SetPropertyValue(nameof(ProductMatrix), ref productMatrix, value);
        }
    }
}