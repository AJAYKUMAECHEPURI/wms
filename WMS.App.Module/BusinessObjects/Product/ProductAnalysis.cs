﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductAnalysis : CustomBaseObject
    {
        public ProductAnalysis(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool exportToeCommerce;

        public bool ExportToeCommerce
        {
            get => exportToeCommerce;
            set => SetPropertyValue(nameof(ExportToeCommerce), ref exportToeCommerce, value);
        }

        //need to add the child
    }
}