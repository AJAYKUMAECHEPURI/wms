﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.ProductConfig;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductDocument : CustomBaseObject
    {
        public ProductDocument(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private ProductMore more;
        private ProductDocumentCustomTag customTag;
        private AutoPrintOption autoPrintOption;
        private FileData file;
        private string note;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Note
        {
            get => note;
            set => SetPropertyValue(nameof(Note), ref note, value);
        }

        public FileData File
        {
            get => file;
            set => SetPropertyValue(nameof(File), ref file, value);
        }

        public AutoPrintOption AutoPrintOption
        {
            get => autoPrintOption;
            set => SetPropertyValue(nameof(AutoPrintOption), ref autoPrintOption, value);
        }

        public ProductDocumentCustomTag CustomTag
        {
            get => customTag;
            set => SetPropertyValue(nameof(CustomTag), ref customTag, value);
        }

        [Association("ProductMore-Document")]
        public ProductMore More
        {
            get => more;
            set => SetPropertyValue(nameof(More), ref more, value);
        }
    }
}