﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.ProductConfig;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class TemplateProductDate : CustomBaseObject
    {
        public TemplateProductDate(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string attribute;
        private string recommendaded;
        private string value;
        private Mode mode;
        private DateTime productDate;

        public DateTime ProductDate
        {
            get => productDate;
            set => SetPropertyValue(nameof(ProductDate), ref productDate, value);
        }

        public Mode Mode
        {
            get => mode;
            set => SetPropertyValue(nameof(Mode), ref mode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Value
        {
            get => value;
            set => SetPropertyValue(nameof(Value), ref this.value, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Recommended
        {
            get => recommendaded;
            set => SetPropertyValue(nameof(Recommended), ref recommendaded, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Attribute
        {
            get => attribute;
            set => SetPropertyValue(nameof(Attribute), ref attribute, value);
        }
    }
}