﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductOptionValue : CustomBaseObject
    {
        public ProductOptionValue(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int modifierAmount;
        private int position;
        private string colourHexCode;
        private bool imageUpload;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public bool ImageUpload
        {
            get => imageUpload;
            set => SetPropertyValue(nameof(ImageUpload), ref imageUpload, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ColorHexCode
        {
            get => colourHexCode;
            set => SetPropertyValue(nameof(ColorHexCode), ref colourHexCode, value);
        }

        public int Position
        {
            get => position;
            set => SetPropertyValue(nameof(Position), ref position, value);
        }

        public int ModifierAmount
        {
            get => modifierAmount;
            set => SetPropertyValue(nameof(ModifierAmount), ref modifierAmount, value);
        }
    }
}