﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductImage : CustomBaseObject
    {
        public ProductImage(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private ProductMore more;
        private string customTag;
        private bool defaultImage;
        private string imageNote;
        private string fileDescription;
        private FileData imageFile;

        //[DevExpress.Xpo.Aggregated]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.PictureEdit,
    DetailViewImageEditorMode = ImageEditorMode.PictureEdit,
    ListViewImageEditorCustomHeight = 40)]
        public FileData ImageFile
        {
            get => imageFile;
            set => SetPropertyValue(nameof(ImageFile), ref imageFile, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string FileDescription
        {
            get => fileDescription;
            set => SetPropertyValue(nameof(FileDescription), ref fileDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ImageNote
        {
            get => imageNote;
            set => SetPropertyValue(nameof(ImageNote), ref imageNote, value);
        }

        public bool DefaultImage
        {
            get => defaultImage;
            set => SetPropertyValue(nameof(DefaultImage), ref defaultImage, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CustomTag
        {
            get => customTag;
            set => SetPropertyValue(nameof(CustomTag), ref customTag, value);
        }

        [Association("ProductMore-Image")]
        public ProductMore More
        {
            get => more;
            set => SetPropertyValue(nameof(More), ref more, value);
        }
    }
}