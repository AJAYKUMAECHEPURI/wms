﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductMatrix : CustomBaseObject
    {
        public ProductMatrix(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        [Association("ProductMatrix-MatrixColumn"), DevExpress.Xpo.Aggregated]
        public XPCollection<MatrixColumn> MatrixColumn
        {
            get
            {
                return GetCollection<MatrixColumn>(nameof(MatrixColumn));
            }
        }

        [Association("ProductMatrix-MatrixRow"), DevExpress.Xpo.Aggregated]
        public XPCollection<MatrixRow> MatrixRow
        {
            get
            {
                return GetCollection<MatrixRow>(nameof(MatrixRow));
            }
        }

        [Association("ProductMatrix-MatrixOption"), DevExpress.Xpo.Aggregated]
        public XPCollection<MatrixOption> MatrixOption
        {
            get
            {
                return GetCollection<MatrixOption>(nameof(MatrixOption));
            }
        }
    }
}