﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.ProductConfig;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductBarcodeSchema : CustomBaseObject
    {
        public ProductBarcodeSchema(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
        }

        private string fixedPosition;
        private DecimalPosition decimalPosition;
        private string expiryFormat;
        private bool treatDelimiterAsASCIINumber;
        private string delimiter;
        private BarcodeMode barcodeMode;
        private string description;
        private string name;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public BarcodeMode BarcodeMode
        {
            get => barcodeMode;
            set => SetPropertyValue(nameof(BarcodeMode), ref barcodeMode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Delimiter
        {
            get => delimiter;
            set => SetPropertyValue(nameof(Delimiter), ref delimiter, value);
        }

        public bool TreatDelimiterAsASCIINumber
        {
            get => treatDelimiterAsASCIINumber;
            set => SetPropertyValue(nameof(TreatDelimiterAsASCIINumber), ref treatDelimiterAsASCIINumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ExpiryFormat
        {
            get => expiryFormat;
            set => SetPropertyValue(nameof(ExpiryFormat), ref expiryFormat, value);
        }

        public DecimalPosition DecimalPosition
        {
            get => decimalPosition;
            set => SetPropertyValue(nameof(DecimalPosition), ref decimalPosition, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string FixedPosition
        {
            get => fixedPosition;
            set => SetPropertyValue(nameof(FixedPosition), ref fixedPosition, value);
        }
    }
}