﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System.ComponentModel.DataAnnotations;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class PropertyGrid : CustomBaseObject
    {
        public PropertyGrid(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string searchOrder;
        private string productProperty;
        private string variantProperty;
        private DataType dataType;
        private string name;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        public DataType DataType
        {
            get => dataType;
            set => SetPropertyValue(nameof(DataType), ref dataType, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantProperty
        {
            get => variantProperty;
            set => SetPropertyValue(nameof(VariantProperty), ref variantProperty, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ProductProperty
        {
            get => productProperty;
            set => SetPropertyValue(nameof(ProductProperty), ref productProperty, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SearchOrder
        {
            get => searchOrder;
            set => SetPropertyValue(nameof(SearchOrder), ref searchOrder, value);
        }
    }
}