﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductAmazonListing : CustomBaseObject
    {
        public ProductAmazonListing(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string bulletPoints;
        private string brand;
        private string description;
        private string title;
        private TemplateProductDate template;
        private string aSIN;
        private AmazonSeller marketplace;

        public AmazonSeller Marketplace
        {
            get => marketplace;
            set => SetPropertyValue(nameof(Marketplace), ref marketplace, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ASIN
        {
            get => aSIN;
            set => SetPropertyValue(nameof(ASIN), ref aSIN, value);
        }

        public TemplateProductDate Template
        {
            get => template;
            set => SetPropertyValue(nameof(Template), ref template, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Title
        {
            get => title;
            set => SetPropertyValue(nameof(Title), ref title, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Brand
        {
            get => brand;
            set => SetPropertyValue(nameof(Brand), ref brand, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string BulletPoints
        {
            get => bulletPoints;
            set => SetPropertyValue(nameof(BulletPoints), ref bulletPoints, value);
        }
    }
}