﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class PropertyOption : CustomBaseObject
    {
        public PropertyOption(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private ProductOptionValue productOptionValue;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public ProductOptionValue ProductOptionValue
        {
            get => productOptionValue;
            set => SetPropertyValue(nameof(ProductOptionValue), ref productOptionValue, value);
        }
    }
}