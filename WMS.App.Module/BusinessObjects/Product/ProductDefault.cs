﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.ProductConfig;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductDefault : CustomBaseObject
    {
        public ProductDefault(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int defaultShelfLife;
        private bool requiresExpiryDate;
        private bool defaultToUniqueSerial;
        private bool requiresBatchNumber;
        private string serialNumberInstruction;
        private bool stockInOriginalPurchaseUnitQuantity;
        private bool skipSerialDetailFromWhenBookingIn;
        private bool defaultBatchToUniqueSerial;
        private bool completeSerialTraceReq;
        private ScanPattern scanPattern;
        private bool requiresOutboundSerialNumber;
        private bool useStockQtyAsOpening;
        private ForceAllocationMethod forceAllocationMethod;
        private bool allowAccessToTransactionOverride;
        private bool autoAllocateBeforeDespatch;
        private UsageMethod usageMethod;
        private ProductType type;
        private string comments;
        private string commodityCode;
        private string abbreviatedDescription;
        private string description;
        private string defaultName;
        private bool active;

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string DefaultName
        {
            get => defaultName;
            set => SetPropertyValue(nameof(DefaultName), ref defaultName, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AbbreviatedDescription
        {
            get => abbreviatedDescription;
            set => SetPropertyValue(nameof(AbbreviatedDescription), ref abbreviatedDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CommodityCode
        {
            get => commodityCode;
            set => SetPropertyValue(nameof(CommodityCode), ref commodityCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Comments
        {
            get => comments;
            set => SetPropertyValue(nameof(Comments), ref comments, value);
        }

        public ProductType Type
        {
            get => type;
            set => SetPropertyValue(nameof(Type), ref type, value);
        }

        public UsageMethod UsageMethod
        {
            get => usageMethod;
            set => SetPropertyValue(nameof(UsageMethod), ref usageMethod, value);
        }

        public bool AutoAllocateBeforeDespatch
        {
            get => autoAllocateBeforeDespatch;
            set => SetPropertyValue(nameof(AutoAllocateBeforeDespatch), ref autoAllocateBeforeDespatch, value);
        }

        public bool AllowAccessToTransactionOverride
        {
            get => allowAccessToTransactionOverride;
            set => SetPropertyValue(nameof(AllowAccessToTransactionOverride), ref allowAccessToTransactionOverride, value);
        }

        public ForceAllocationMethod ForceAllocationMethod
        {
            get => forceAllocationMethod;
            set => SetPropertyValue(nameof(ForceAllocationMethod), ref forceAllocationMethod, value);
        }

        public bool UseStockQtyAsOpening
        {
            get => useStockQtyAsOpening;
            set => SetPropertyValue(nameof(UseStockQtyAsOpening), ref useStockQtyAsOpening, value);
        }

        public bool RequiresOutboundSerialNumber
        {
            get => requiresOutboundSerialNumber;
            set => SetPropertyValue(nameof(RequiresOutboundSerialNumber), ref requiresOutboundSerialNumber, value);
        }

        public ScanPattern ScanPattern
        {
            get => scanPattern;
            set => SetPropertyValue(nameof(ScanPattern), ref scanPattern, value);
        }

        public bool CompleteSerialTraceReq
        {
            get => completeSerialTraceReq;
            set => SetPropertyValue(nameof(CompleteSerialTraceReq), ref completeSerialTraceReq, value);
        }

        public bool DefaultToUniqueSerial
        {
            get => defaultToUniqueSerial;
            set => SetPropertyValue(nameof(DefaultToUniqueSerial), ref defaultToUniqueSerial, value);
        }

        public bool SkipSerialDetailFromWhenBookingIn
        {
            get => skipSerialDetailFromWhenBookingIn;
            set => SetPropertyValue(nameof(SkipSerialDetailFromWhenBookingIn), ref skipSerialDetailFromWhenBookingIn, value);
        }

        public bool StockInOriginalPurchaseUnitQuantity
        {
            get => stockInOriginalPurchaseUnitQuantity;
            set => SetPropertyValue(nameof(StockInOriginalPurchaseUnitQuantity), ref stockInOriginalPurchaseUnitQuantity, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SerialNumberInstruction
        {
            get => serialNumberInstruction;
            set => SetPropertyValue(nameof(SerialNumberInstruction), ref serialNumberInstruction, value);
        }

        public bool RequiresBatchNumber
        {
            get => requiresBatchNumber;
            set => SetPropertyValue(nameof(RequiresBatchNumber), ref requiresBatchNumber, value);
        }

        public bool DefaultBatchToUniqueSerial
        {
            get => defaultBatchToUniqueSerial;
            set => SetPropertyValue(nameof(DefaultBatchToUniqueSerial), ref defaultBatchToUniqueSerial, value);
        }

        public bool RequiresExpiryDate
        {
            get => requiresExpiryDate;
            set => SetPropertyValue(nameof(RequiresExpiryDate), ref requiresExpiryDate, value);
        }

        public int DefaultShelfLife
        {
            get => defaultShelfLife;
            set => SetPropertyValue(nameof(DefaultShelfLife), ref defaultShelfLife, value);
        }
    }
}