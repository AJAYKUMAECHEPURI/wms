﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class AmazonSeller : CustomBaseObject
    {
        public AmazonSeller(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string authToken;
        private ProductSalesMarketPlace productSalesMarketPlace;
        private string sellerDescription;
        private string sellerId;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SellerId
        {
            get => sellerId;
            set => SetPropertyValue(nameof(SellerId), ref sellerId, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SellerDescription
        {
            get => sellerDescription;
            set => SetPropertyValue(nameof(SellerDescription), ref sellerDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AuthToken
        {
            get => authToken;
            set => SetPropertyValue(nameof(AuthToken), ref authToken, value);
        }

        public ProductSalesMarketPlace ProductSalesMarketPlace
        {
            get => productSalesMarketPlace;
            set => SetPropertyValue(nameof(ProductSalesMarketPlace), ref productSalesMarketPlace, value);
        }
    }
}