﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductSalesMarketPlace : CustomBaseObject
    {
        public ProductSalesMarketPlace(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string marketplaceID;
        private string region;
        private string marketplaceCode;
        private string marketplaceURL;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MarketplaceURL
        {
            get => marketplaceURL;
            set => SetPropertyValue(nameof(MarketplaceURL), ref marketplaceURL, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MarketplaceCode
        {
            get => marketplaceCode;
            set => SetPropertyValue(nameof(MarketplaceCode), ref marketplaceCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Region
        {
            get => region;
            set => SetPropertyValue(nameof(Region), ref region, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MarketplaceID
        {
            get => marketplaceID;
            set => SetPropertyValue(nameof(MarketplaceID), ref marketplaceID, value);
        }
    }
}