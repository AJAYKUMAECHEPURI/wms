﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.ProductVariant;
using static WMS.App.Module.BusinessObjects.System.Helpers.ProductConfig;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductSetting : CustomBaseObject
    {
        public ProductSetting(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private ProductOptionValue scanPattern;
        private bool requiresOutboundSerialNumber;
        private string serialNumberInstruction;
        private bool stockInOriginalPurchaseUnitQuantity;
        private bool skipSerialDetailFromWhenBookingIn;
        private bool defaultToUniqueSerial;
        private bool completeSerialTrace;
        private ShowUsageFor showUsageFor;
        private Variant purchaseVariant;
        private bool includeInProductToPurchaseReport;
        private bool useProductStockQtyAsOpeningBalance;
        private ForceAllocationMethod forceAllocationMethod;
        private string allowAccessToTransactionOverride;
        private string autoAllocateBeforeDespatch;
        private UsageMethod usageMethod;
        private ProductType productType;

        public ProductType ProductType
        {
            get => productType;
            set => SetPropertyValue(nameof(ProductType), ref productType, value);
        }

        public UsageMethod UsageMethod
        {
            get => usageMethod;
            set => SetPropertyValue(nameof(UsageMethod), ref usageMethod, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AutoAllocateBeforeDespatch
        {
            get => autoAllocateBeforeDespatch;
            set => SetPropertyValue(nameof(AutoAllocateBeforeDespatch), ref autoAllocateBeforeDespatch, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AllowAccessToTransactionOverride
        {
            get => allowAccessToTransactionOverride;
            set => SetPropertyValue(nameof(AllowAccessToTransactionOverride), ref allowAccessToTransactionOverride, value);
        }

        public ForceAllocationMethod ForceAllocationMethod
        {
            get => forceAllocationMethod;
            set => SetPropertyValue(nameof(ForceAllocationMethod), ref forceAllocationMethod, value);
        }

        public bool UseProductStockQtyAsOpeningBalance
        {
            get => useProductStockQtyAsOpeningBalance;
            set => SetPropertyValue(nameof(UseProductStockQtyAsOpeningBalance), ref useProductStockQtyAsOpeningBalance, value);
        }

        public bool IncludeInProductToPurchaseReport
        {
            get => includeInProductToPurchaseReport;
            set => SetPropertyValue(nameof(IncludeInProductToPurchaseReport), ref includeInProductToPurchaseReport, value);
        }

        public Variant PurchaseVariant
        {
            get => purchaseVariant;
            set => SetPropertyValue(nameof(PurchaseVariant), ref purchaseVariant, value);
        }

        public ShowUsageFor ShowUsageFor
        {
            get => showUsageFor;
            set => SetPropertyValue(nameof(ShowUsageFor), ref showUsageFor, value);
        }

        public bool CompleteSerialTrace
        {
            get => completeSerialTrace;
            set => SetPropertyValue(nameof(CompleteSerialTrace), ref completeSerialTrace, value);
        }

        public bool DefaultToUniqueSerial
        {
            get => defaultToUniqueSerial;
            set => SetPropertyValue(nameof(DefaultToUniqueSerial), ref defaultToUniqueSerial, value);
        }

        public bool SkipSerialDetailFromWhenBookingIn
        {
            get => skipSerialDetailFromWhenBookingIn;
            set => SetPropertyValue(nameof(SkipSerialDetailFromWhenBookingIn), ref skipSerialDetailFromWhenBookingIn, value);
        }

        public bool StockInOriginalPurchaseUnitQuantity
        {
            get => stockInOriginalPurchaseUnitQuantity;
            set => SetPropertyValue(nameof(StockInOriginalPurchaseUnitQuantity), ref stockInOriginalPurchaseUnitQuantity, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SerialNumberInstruction
        {
            get => serialNumberInstruction;
            set => SetPropertyValue(nameof(SerialNumberInstruction), ref serialNumberInstruction, value);
        }

        public bool RequiresOutboundSerialNumber
        {
            get => requiresOutboundSerialNumber;
            set => SetPropertyValue(nameof(RequiresOutboundSerialNumber), ref requiresOutboundSerialNumber, value);
        }

        public ProductOptionValue ProductOptionValue
        {
            get => scanPattern;
            set => SetPropertyValue(nameof(ProductOptionValue), ref scanPattern, value);
        }
    }
}