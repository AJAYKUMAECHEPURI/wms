﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductMore : CustomBaseObject
    {
        public ProductMore(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        [Association("ProductMore-Image"), DevExpress.Xpo.Aggregated]
        public XPCollection<ProductImage> Image
        {
            get
            {
                return GetCollection<ProductImage>(nameof(Image));
            }
        }

        [Association("ProductMore-Document"), DevExpress.Xpo.Aggregated]
        public XPCollection<ProductDocument> Document
        {
            get
            {
                return GetCollection<ProductDocument>(nameof(Document));
            }
        }

        [Association("ProductMore-Message"), DevExpress.Xpo.Aggregated]
        public XPCollection<ProductMessage> Message => GetCollection<ProductMessage>(nameof(Message));
    }
}