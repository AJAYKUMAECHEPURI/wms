﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Product
{
    [DefaultClassOptions]
    [NavigationItem("Product")]
    public class ProductMessage : CustomBaseObject
    {
        public ProductMessage(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private ProductMore more;
        private bool showMessageWhenAddingToPO;
        private bool showMessageWhenAddingToSO;
        private bool showMessageIndividually;
        private DateTime endDate;
        private DateTime startDate;
        private string message;
        private string title;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Title
        {
            get => title;
            set => SetPropertyValue(nameof(Title), ref title, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Message
        {
            get => message;
            set => SetPropertyValue(nameof(Message), ref message, value);
        }

        public DateTime StartDate
        {
            get => startDate;
            set => SetPropertyValue(nameof(StartDate), ref startDate, value);
        }

        public DateTime EndDate
        {
            get => endDate;
            set => SetPropertyValue(nameof(EndDate), ref endDate, value);
        }

        public bool ShowMessageIndividually
        {
            get => showMessageIndividually;
            set => SetPropertyValue(nameof(ShowMessageIndividually), ref showMessageIndividually, value);
        }

        public bool ShowMessageWhenAddingToSO
        {
            get => showMessageWhenAddingToSO;
            set => SetPropertyValue(nameof(ShowMessageWhenAddingToSO), ref showMessageWhenAddingToSO, value);
        }

        public bool ShowMessageWhenAddingToPO
        {
            get => showMessageWhenAddingToPO;
            set => SetPropertyValue(nameof(ShowMessageWhenAddingToPO), ref showMessageWhenAddingToPO, value);
        }

        [Association("ProductMore-Message")]
        public ProductMore More
        {
            get => more;
            set => SetPropertyValue(nameof(More), ref more, value);
        }
    }
}