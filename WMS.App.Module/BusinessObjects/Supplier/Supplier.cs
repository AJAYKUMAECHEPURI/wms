﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class Supplier : BaseObject
    {
        public Supplier(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Detail = new SupplierDetails(Session);
        }

        private SupplierCreditDetail supplierCreditDetail;
        private SupplierSetting setting;
        private SupplierDetails detail;

        [DevExpress.Xpo.Aggregated, VisibleInListView(false)]
        public SupplierDetails Detail
        {
            get => detail;
            set => SetPropertyValue(nameof(Detail), ref detail, value);
        }

        [DevExpress.Xpo.Aggregated, VisibleInListView(false)]
        public SupplierSetting Setting
        {
            get => setting;
            set => SetPropertyValue(nameof(Setting), ref setting, value);
        }

        [DevExpress.Xpo.Aggregated, VisibleInListView(false)]
        public SupplierCreditDetail SupplierCreditDetail
        {
            get => supplierCreditDetail;
            set => SetPropertyValue(nameof(SupplierCreditDetail), ref supplierCreditDetail, value);
        }
    }
}