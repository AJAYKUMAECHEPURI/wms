﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.SupplierConfig;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierMessage : BaseObject
    {
        public SupplierMessage(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private ShowOn showOn;
        private bool showIndividually;
        private string message;
        private string title;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Title
        {
            get => title;
            set => SetPropertyValue(nameof(Title), ref title, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Message
        {
            get => message;
            set => SetPropertyValue(nameof(Message), ref message, value);
        }

        public bool ShowIndividually
        {
            get => showIndividually;
            set => SetPropertyValue(nameof(ShowIndividually), ref showIndividually, value);
        }

        public ShowOn ShowOn
        {
            get => showOn;
            set => SetPropertyValue(nameof(ShowOn), ref showOn, value);
        }
    }
}