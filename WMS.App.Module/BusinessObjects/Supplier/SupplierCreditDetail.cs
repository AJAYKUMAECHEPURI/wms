﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    public class SupplierCreditDetail : BaseObject
    {
        public SupplierCreditDetail(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            //SupplierCreditDetailsGeneral = new SupplierCreditDetail(Session);
            // SupplierCreditDetailsExtendedTerms = new SupplierCreditDetailsExtendedTerms(Session);
        }

        private Product.Product supplierCreditDetailsGeneral;

        [DevExpress.Xpo.Aggregated, VisibleInListView(false)]
        public Product.Product SupplierCreditDetailsGeneral
        {
            get => supplierCreditDetailsGeneral;
            set => SetPropertyValue(nameof(SupplierCreditDetailsGeneral), ref supplierCreditDetailsGeneral, value);
        }

        //[DevExpress.Xpo.Aggregated, VisibleInListView(false)]
        //public SupplierCreditDetailsExtendedTerms SupplierCreditDetailsExtendedTerms
        //{
        //    get => supplierCreditDetailsExtendedTerms;
        //    set => SetPropertyValue(nameof(SupplierCreditDetailsExtendedTerms), ref supplierCreditDetailsExtendedTerms, value);
        //}
    }
}