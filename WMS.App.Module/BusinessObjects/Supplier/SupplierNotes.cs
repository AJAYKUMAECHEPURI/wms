﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierNotes : BaseObject
    {
        public SupplierNotes(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool requiresAction;
        private string goodsSold;
        private string notes;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Notes
        {
            get => notes;
            set => SetPropertyValue(nameof(Notes), ref notes, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Finalized
        {
            get => goodsSold;
            set => SetPropertyValue(nameof(Finalized), ref goodsSold, value);
        }

        public bool RequiresAction
        {
            get => requiresAction;
            set => SetPropertyValue(nameof(RequiresAction), ref requiresAction, value);
        }
    }
}