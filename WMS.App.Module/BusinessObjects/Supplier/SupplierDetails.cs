﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierDetails : BaseObject
    {
        public SupplierDetails(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private SupplierAddress statementAddress;
        private SupplierAddress returnAddress;
        private bool importSupplier;
        private bool doNotUseThisAccount;
        private bool eUSupplier;
        private string vATNumber;
        private SupplierDefault @default;
        private string accountNumberWithSupplier;
        private string alternativeAccountNumber;
        private string accountNumber;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AccountNumber
        {
            get => accountNumber;
            set => SetPropertyValue(nameof(AccountNumber), ref accountNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AlternativeAccountNumber
        {
            get => alternativeAccountNumber;
            set => SetPropertyValue(nameof(AlternativeAccountNumber), ref alternativeAccountNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AccountNumberWithSupplier
        {
            get => accountNumberWithSupplier;
            set => SetPropertyValue(nameof(AccountNumberWithSupplier), ref accountNumberWithSupplier, value);
        }

        public SupplierDefault Default
        {
            get => @default;
            set => SetPropertyValue(nameof(Default), ref @default, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VATNumber
        {
            get => vATNumber;
            set => SetPropertyValue(nameof(VATNumber), ref vATNumber, value);
        }

        public bool EUSupplier
        {
            get => eUSupplier;
            set => SetPropertyValue(nameof(EUSupplier), ref eUSupplier, value);
        }

        public bool DoNotUseThisAccount
        {
            get => doNotUseThisAccount;
            set => SetPropertyValue(nameof(DoNotUseThisAccount), ref doNotUseThisAccount, value);
        }

        public bool ImportSupplier
        {
            get => importSupplier;
            set => SetPropertyValue(nameof(ImportSupplier), ref importSupplier, value);
        }

        public SupplierAddress StatementAddress
        {
            get => statementAddress;
            set => SetPropertyValue(nameof(StatementAddress), ref statementAddress, value);
        }

        public SupplierAddress ReturnAddress
        {
            get => returnAddress;
            set => SetPropertyValue(nameof(ReturnAddress), ref returnAddress, value);
        }
    }
}