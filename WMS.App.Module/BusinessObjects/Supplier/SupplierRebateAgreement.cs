﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.SupplierConfig;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierRebateAgreement : BaseObject
    {
        public SupplierRebateAgreement(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private PaymentMethod paymentMethod;
        private DateTime endDate;
        private DateTime startDate;
        private PaymentFrequency paymentFrequency;
        private CalculationPeriod calculationPeriod;
        private SupplierRebatePeriod rebateAgreement;
        private bool active;

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        public SupplierRebatePeriod RebateAgreement
        {
            get => rebateAgreement;
            set => SetPropertyValue(nameof(RebateAgreement), ref rebateAgreement, value);
        }

        public CalculationPeriod CalculationPeriod

        {
            get => calculationPeriod;
            set => SetPropertyValue(nameof(CalculationPeriod), ref calculationPeriod, value);
        }

        public PaymentFrequency PaymentFrequency
        {
            get => paymentFrequency;
            set => SetPropertyValue(nameof(PaymentFrequency), ref paymentFrequency, value);
        }

        public DateTime StartDate
        {
            get => startDate;
            set => SetPropertyValue(nameof(StartDate), ref startDate, value);
        }

        public DateTime EndDate
        {
            get => endDate;
            set => SetPropertyValue(nameof(EndDate), ref endDate, value);
        }

        public PaymentMethod PaymentMethod
        {
            get => paymentMethod;
            set => SetPropertyValue(nameof(PaymentMethod), ref paymentMethod, value);
        }
    }
}