﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierThirdPartyLogistic : BaseObject
    {
        public SupplierThirdPartyLogistic(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string pOCharge;
        private Customer.Customer customer;
        private bool thirdPartyLogisticsSupplier;

        public bool ThirdPartyLogisticsSupplier
        {
            get => thirdPartyLogisticsSupplier;
            set => SetPropertyValue(nameof(ThirdPartyLogisticsSupplier), ref thirdPartyLogisticsSupplier, value);
        }

        public Customer.Customer Customer
        {
            get => customer;
            set => SetPropertyValue(nameof(Customer), ref customer, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string POCharge
        {
            get => pOCharge;
            set => SetPropertyValue(nameof(POCharge), ref pOCharge, value);
        }
    }
}