﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierDefault : BaseObject
    {
        public SupplierDefault(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool active;
        private System.Country returnCountry;
        private System.Country statementCountry;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public System.Country StatementCountry
        {
            get => statementCountry;
            set => SetPropertyValue(nameof(StatementCountry), ref statementCountry, value);
        }

        public System.Country ReturnCountry
        {
            get => returnCountry;
            set => SetPropertyValue(nameof(ReturnCountry), ref returnCountry, value);
        }

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }
    }
}