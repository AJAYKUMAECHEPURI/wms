﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierAddress : BaseObject
    {
        public SupplierAddress(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string email;
        private string fax;
        private string telephoneNumber;
        private string postcode;
        private System.Country country;
        private string town;
        private string address3;
        private string address2;
        private string address1;
        private string name;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address1
        {
            get => address1;
            set => SetPropertyValue(nameof(Address1), ref address1, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address2
        {
            get => address2;
            set => SetPropertyValue(nameof(Address2), ref address2, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address3
        {
            get => address3;
            set => SetPropertyValue(nameof(Address3), ref address3, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Town
        {
            get => town;
            set => SetPropertyValue(nameof(Town), ref town, value);
        }

        public System.Country Country
        {
            get => country;
            set => SetPropertyValue(nameof(Country), ref country, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Postcode
        {
            get => postcode;
            set => SetPropertyValue(nameof(Postcode), ref postcode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string TelephoneNumber
        {
            get => telephoneNumber;
            set => SetPropertyValue(nameof(TelephoneNumber), ref telephoneNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Fax
        {
            get => fax;
            set => SetPropertyValue(nameof(Fax), ref fax, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Email
        {
            get => email;
            set => SetPropertyValue(nameof(Email), ref email, value);
        }
    }
}