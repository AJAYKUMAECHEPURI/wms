﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.SupplierConfig;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierCreditDetailsGeneral : BaseObject
    {
        public SupplierCreditDetailsGeneral(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private PurchaseInvoiceDetailLevel purchaseInvoiceDetailLevel;
        private AutoHoldOnOverDue autoHoldOnOverDue;
        private string older;
        private string period3;
        private string period2;
        private string period1;
        private string current;
        private string future;
        private string obtainAgedBalance;
        private SettlementFrom from;
        private string settlementDays;
        private string settlementDiscount;
        private SupplierDayFrom dayFrom;
        private string paymentsTerms;
        private bool requiresPosting;
        private bool overCreditTerms;
        private bool onhold;
        private string availableToSpend;
        private string openOrderValue;
        private string unpostedPayment;
        private string unPostedInvoice;
        private string creditLimit;
        private string accountBalance;
        private bool autoObtainAccountBalance;

        public bool AutoObtainAccountBalance
        {
            get => autoObtainAccountBalance;
            set => SetPropertyValue(nameof(AutoObtainAccountBalance), ref autoObtainAccountBalance, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AccountBalance
        {
            get => accountBalance;
            set => SetPropertyValue(nameof(AccountBalance), ref accountBalance, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CreditLimit
        {
            get => creditLimit;
            set => SetPropertyValue(nameof(CreditLimit), ref creditLimit, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string UnPostedInvoice
        {
            get => unPostedInvoice;
            set => SetPropertyValue(nameof(UnPostedInvoice), ref unPostedInvoice, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string UnPostedPayment
        {
            get => unpostedPayment;
            set => SetPropertyValue(nameof(UnPostedPayment), ref unpostedPayment, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string OpenOrderValue
        {
            get => openOrderValue;
            set => SetPropertyValue(nameof(OpenOrderValue), ref openOrderValue, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AvailableToSpend
        {
            get => availableToSpend;
            set => SetPropertyValue(nameof(AvailableToSpend), ref availableToSpend, value);
        }

        public bool OnHold
        {
            get => onhold;
            set => SetPropertyValue(nameof(OnHold), ref onhold, value);
        }

        public bool OverCreditTerms
        {
            get => overCreditTerms;
            set => SetPropertyValue(nameof(OverCreditTerms), ref overCreditTerms, value);
        }

        public bool RequiresPosting
        {
            get => requiresPosting;
            set => SetPropertyValue(nameof(RequiresPosting), ref requiresPosting, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PaymentsTerms
        {
            get => paymentsTerms;
            set => SetPropertyValue(nameof(PaymentsTerms), ref paymentsTerms, value);
        }

        public SupplierDayFrom DayFrom
        {
            get => dayFrom;
            set => SetPropertyValue(nameof(DayFrom), ref dayFrom, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SettlementDiscount
        {
            get => settlementDiscount;
            set => SetPropertyValue(nameof(SettlementDiscount), ref settlementDiscount, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SettlementDays
        {
            get => settlementDays;
            set => SetPropertyValue(nameof(SettlementDays), ref settlementDays, value);
        }

        public SettlementFrom From
        {
            get => from;
            set => SetPropertyValue(nameof(From), ref from, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ObtainAgedBalance
        {
            get => obtainAgedBalance;
            set => SetPropertyValue(nameof(ObtainAgedBalance), ref obtainAgedBalance, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Future
        {
            get => future;
            set => SetPropertyValue(nameof(Future), ref future, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Current
        {
            get => current;
            set => SetPropertyValue(nameof(Current), ref current, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Period1
        {
            get => period1;
            set => SetPropertyValue(nameof(Period1), ref period1, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Period2
        {
            get => period2;
            set => SetPropertyValue(nameof(Period2), ref period2, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Period3
        {
            get => period3;
            set => SetPropertyValue(nameof(Period3), ref period3, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Older
        {
            get => older;
            set => SetPropertyValue(nameof(Older), ref older, value);
        }

        public AutoHoldOnOverDue AutoHoldOnOverDue
        {
            get => autoHoldOnOverDue;
            set => SetPropertyValue(nameof(AutoHoldOnOverDue), ref autoHoldOnOverDue, value);
        }

        public PurchaseInvoiceDetailLevel PurchaseInvoiceDetailLevel
        {
            get => purchaseInvoiceDetailLevel;
            set => SetPropertyValue(nameof(PurchaseInvoiceDetailLevel), ref purchaseInvoiceDetailLevel, value);
        }
    }
}