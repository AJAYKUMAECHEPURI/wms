﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.SupplierConfig;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierCreditDetailsExtendedTerms : BaseObject
    {
        public SupplierCreditDetailsExtendedTerms(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private SettlementFrom daysFrom;
        private int extendedSettlementTerms;
        private SupplierDayFrom from;
        private int extendedFromDays;
        private bool useExtendedPamentTerms;

        public bool UseExtendedPaymentTerms
        {
            get => useExtendedPamentTerms;
            set => SetPropertyValue(nameof(UseExtendedPaymentTerms), ref useExtendedPamentTerms, value);
        }

        public int ExtendedFromDays
        {
            get => extendedFromDays;
            set => SetPropertyValue(nameof(ExtendedFromDays), ref extendedFromDays, value);
        }

        public SupplierDayFrom ExtendedFrom
        {
            get => from;
            set => SetPropertyValue(nameof(ExtendedFrom), ref from, value);
        }

        public int ExtendedSettlementTerms
        {
            get => extendedSettlementTerms;
            set => SetPropertyValue(nameof(ExtendedSettlementTerms), ref extendedSettlementTerms, value);
        }

        public SettlementFrom DaysFrom
        {
            get => daysFrom;
            set => SetPropertyValue(nameof(DaysFrom), ref daysFrom, value);
        }
    }
}