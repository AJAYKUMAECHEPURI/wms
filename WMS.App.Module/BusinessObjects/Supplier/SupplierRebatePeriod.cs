﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierRebatePeriod : BaseObject
    {
        public SupplierRebatePeriod(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool paid;
        private string rebateAmount;
        private string salesNet;
        private bool finalised;
        private DateTime dateTo;
        private DateTime dateFrom;
        private string rebateAgreement;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string RebateAgreement
        {
            get => rebateAgreement;
            set => SetPropertyValue(nameof(RebateAgreement), ref rebateAgreement, value);
        }

        public DateTime DateFrom
        {
            get => dateFrom;
            set => SetPropertyValue(nameof(DateFrom), ref dateFrom, value);
        }

        public DateTime DateTo
        {
            get => dateTo;
            set => SetPropertyValue(nameof(DateTo), ref dateTo, value);
        }

        public bool Finalized
        {
            get => finalised;
            set => SetPropertyValue(nameof(Finalized), ref finalised, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SalesNet
        {
            get => salesNet;
            set => SetPropertyValue(nameof(SalesNet), ref salesNet, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string RebateAmount
        {
            get => rebateAmount;
            set => SetPropertyValue(nameof(RebateAmount), ref rebateAmount, value);
        }

        public bool Paid
        {
            get => paid;
            set => SetPropertyValue(nameof(Paid), ref paid, value);
        }
    }
}