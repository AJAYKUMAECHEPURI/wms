﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierContactDetail : BaseObject
    {
        public SupplierContactDetail(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string email;
        private string fax;
        private string mobile;
        private string extension;
        private string telephone;
        private string position;
        private string name;
        private string title;
        private bool mainContact;
        private bool active;

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        public bool MainContact
        {
            get => mainContact;
            set => SetPropertyValue(nameof(MainContact), ref mainContact, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Title
        {
            get => title;
            set => SetPropertyValue(nameof(Title), ref title, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Position
        {
            get => position;
            set => SetPropertyValue(nameof(Position), ref position, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Telephone
        {
            get => telephone;
            set => SetPropertyValue(nameof(Telephone), ref telephone, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Extension
        {
            get => extension;
            set => SetPropertyValue(nameof(Extension), ref extension, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Mobile
        {
            get => mobile;
            set => SetPropertyValue(nameof(Mobile), ref mobile, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Fax
        {
            get => fax;
            set => SetPropertyValue(nameof(Fax), ref fax, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Email
        {
            get => email;
            set => SetPropertyValue(nameof(Email), ref email, value);
        }
    }
}