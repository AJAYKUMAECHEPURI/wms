﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.SupplierConfig;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierDefaultCreditDetail : BaseObject
    {
        public SupplierDefaultCreditDetail(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool requiresPosting;
        private bool overCreditTerms;
        private bool onhold;
        private PurchaseInvoiceDetailLevel purchaseInvoiceDetailLevel;
        private AutoHoldOnOverDue autoHoldOnOverDue;
        private bool obtainAgedBalance;
        private SettlementFrom settlementFrom;
        private string settlementDays;
        private string settlementDiscount;
        private SupplierDayFrom supplierDayFrom;
        private string paymentsTerms;
        private string creditLimit;
        private bool autoObtainAccountBalance;

        public bool AutoObtainAccountBalance
        {
            get => autoObtainAccountBalance;
            set => SetPropertyValue(nameof(AutoObtainAccountBalance), ref autoObtainAccountBalance, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CreditLimit
        {
            get => creditLimit;
            set => SetPropertyValue(nameof(CreditLimit), ref creditLimit, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PaymentsTerms
        {
            get => paymentsTerms;
            set => SetPropertyValue(nameof(PaymentsTerms), ref paymentsTerms, value);
        }

        public SupplierDayFrom SupplierDayFrom
        {
            get => supplierDayFrom;
            set => SetPropertyValue(nameof(SupplierDayFrom), ref supplierDayFrom, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SettlementDiscount
        {
            get => settlementDiscount;
            set => SetPropertyValue(nameof(SettlementDiscount), ref settlementDiscount, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SettlementDays
        {
            get => settlementDays;
            set => SetPropertyValue(nameof(SettlementDays), ref settlementDays, value);
        }

        public SettlementFrom SettlementFrom
        {
            get => settlementFrom;
            set => SetPropertyValue(nameof(SettlementFrom), ref settlementFrom, value);
        }

        public bool ObtainAgedBalance
        {
            get => obtainAgedBalance;
            set => SetPropertyValue(nameof(ObtainAgedBalance), ref obtainAgedBalance, value);
        }

        public AutoHoldOnOverDue AutoHoldOnOverDue
        {
            get => autoHoldOnOverDue;
            set => SetPropertyValue(nameof(AutoHoldOnOverDue), ref autoHoldOnOverDue, value);
        }

        public PurchaseInvoiceDetailLevel PurchaseInvoiceDetailLevel
        {
            get => purchaseInvoiceDetailLevel;
            set => SetPropertyValue(nameof(PurchaseInvoiceDetailLevel), ref purchaseInvoiceDetailLevel, value);
        }

        public bool OnHold
        {
            get => onhold;
            set => SetPropertyValue(nameof(OnHold), ref onhold, value);
        }

        public bool OverCreditTerms
        {
            get => overCreditTerms;
            set => SetPropertyValue(nameof(OverCreditTerms), ref overCreditTerms, value);
        }

        public bool RequiresPosting
        {
            get => requiresPosting;
            set => SetPropertyValue(nameof(RequiresPosting), ref requiresPosting, value);
        }
    }
}