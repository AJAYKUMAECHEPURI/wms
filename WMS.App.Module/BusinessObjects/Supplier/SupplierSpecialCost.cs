﻿using DevExpress.CodeParser;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierSpecialCost : BaseObject
    {
        public SupplierSpecialCost(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string quantityTo;
        private string quantityFrom;
        private string cost;
        private string discountPercentage;
        private Variable variantCode;

        public Variable VariantCode
        {
            get => variantCode;
            set => SetPropertyValue(nameof(VariantCode), ref variantCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string DiscountPercentage
        {
            get => discountPercentage;
            set => SetPropertyValue(nameof(DiscountPercentage), ref discountPercentage, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Cost
        {
            get => cost;
            set => SetPropertyValue(nameof(Cost), ref cost, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string QuantityFrom
        {
            get => quantityFrom;
            set => SetPropertyValue(nameof(QuantityFrom), ref quantityFrom, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string QuantityTo
        {
            get => quantityTo;
            set => SetPropertyValue(nameof(QuantityTo), ref quantityTo, value);
        }
    }
}