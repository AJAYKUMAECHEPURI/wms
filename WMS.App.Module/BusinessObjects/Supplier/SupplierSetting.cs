﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;
using WMS.App.Module.BusinessObjects.System.Customer;
using WMS.App.Module.BusinessObjects.System.Supplier;
using static WMS.App.Module.BusinessObjects.System.Helpers.SupplierConfig;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierSetting : BaseObject
    {
        public SupplierSetting(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private Supplier alternativeInvoiceSupplier;
        private bool rebateSupplier;
        private string bIC;
        private string iBAN;
        private SupplierPaymentGroup paymentGroup;
        private string sortCode;
        private string accountNumber;
        private string bankAccountName;
        private string bankName;
        private int fMDVerification;
        private bool autoGeneratePO;
        private bool disableAutoOrders;
        private bool buildupsForMinimumOrder;
        private SupplierPOType purchaseReportPOType;
        private bool excludeFromDutie;
        private string freeShippingValue;
        private string minOrderValue;
        private bool qualityApproved;
        private bool costCenterOverRideVariant;
        private bool departmentCodeOverRideVariant;
        private bool nominalCodeOverRideVariant;
        private bool taxOverRideVariant;
        private bool multiCurrency;
        private SupplierSpecialCost specialCostSetByDate;
        private SupplierType supplierType;
        private QuantityStatus quantityStatus;
        private StockLocationDetail defaultStockLocation;
        private CostCenter costCenter;
        private DepartmentCode departmentCode;
        private NominalCode nominalCode;
        private TaxRate tax;
        private System.Currency currency;

        public System.Currency Currency
        {
            get => currency;
            set => SetPropertyValue(nameof(Currency), ref currency, value);
        }

        public TaxRate Tax
        {
            get => tax;
            set => SetPropertyValue(nameof(Tax), ref tax, value);
        }

        public NominalCode NominalCode
        {
            get => nominalCode;
            set => SetPropertyValue(nameof(NominalCode), ref nominalCode, value);
        }

        public DepartmentCode DepartmentCode
        {
            get => departmentCode;
            set => SetPropertyValue(nameof(DepartmentCode), ref departmentCode, value);
        }

        public CostCenter CostCenter
        {
            get => costCenter;
            set => SetPropertyValue(nameof(CostCenter), ref costCenter, value);
        }

        public StockLocationDetail DefaultStockLocation
        {
            get => defaultStockLocation;
            set => SetPropertyValue(nameof(DefaultStockLocation), ref defaultStockLocation, value);
        }

        public QuantityStatus QuantityStatus
        {
            get => quantityStatus;
            set => SetPropertyValue(nameof(QuantityStatus), ref quantityStatus, value);
        }

        public SupplierType SupplierType
        {
            get => supplierType;
            set => SetPropertyValue(nameof(SupplierType), ref supplierType, value);
        }

        public SupplierSpecialCost SpecialCostSetByDate
        {
            get => specialCostSetByDate;
            set => SetPropertyValue(nameof(SpecialCostSetByDate), ref specialCostSetByDate, value);
        }

        public bool MultiCurrency
        {
            get => multiCurrency;
            set => SetPropertyValue(nameof(MultiCurrency), ref multiCurrency, value);
        }

        public bool TaxOverRideVariant
        {
            get => taxOverRideVariant;
            set => SetPropertyValue(nameof(TaxOverRideVariant), ref taxOverRideVariant, value);
        }

        public bool NominalCodeOverRideVariant
        {
            get => nominalCodeOverRideVariant;
            set => SetPropertyValue(nameof(NominalCodeOverRideVariant), ref nominalCodeOverRideVariant, value);
        }

        public bool DepartmentCodeOverRideVariant
        {
            get => departmentCodeOverRideVariant;
            set => SetPropertyValue(nameof(DepartmentCodeOverRideVariant), ref departmentCodeOverRideVariant, value);
        }

        public bool CostCenterOverRideVariant
        {
            get => costCenterOverRideVariant;
            set => SetPropertyValue(nameof(CostCenterOverRideVariant), ref costCenterOverRideVariant, value);
        }

        public bool QualityApproved
        {
            get => qualityApproved;
            set => SetPropertyValue(nameof(QualityApproved), ref qualityApproved, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MinOrderValue
        {
            get => minOrderValue;
            set => SetPropertyValue(nameof(MinOrderValue), ref minOrderValue, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string FreeShippingValue
        {
            get => freeShippingValue;
            set => SetPropertyValue(nameof(FreeShippingValue), ref freeShippingValue, value);
        }

        public bool ExcludeFromDuties
        {
            get => excludeFromDutie;
            set => SetPropertyValue(nameof(ExcludeFromDuties), ref excludeFromDutie, value);
        }

        public SupplierPOType PurchaseReportPOType
        {
            get => purchaseReportPOType;
            set => SetPropertyValue(nameof(PurchaseReportPOType), ref purchaseReportPOType, value);
        }

        public bool BuildupsForMinimumOrder
        {
            get => buildupsForMinimumOrder;
            set => SetPropertyValue(nameof(BuildupsForMinimumOrder), ref buildupsForMinimumOrder, value);
        }

        public bool DisableAutoOrders
        {
            get => disableAutoOrders;
            set => SetPropertyValue(nameof(DisableAutoOrders), ref disableAutoOrders, value);
        }

        public bool AutoGeneratePO
        {
            get => autoGeneratePO;
            set => SetPropertyValue(nameof(AutoGeneratePO), ref autoGeneratePO, value);
        }

        public Supplier AlternativeInvoiceSupplier
        {
            get => alternativeInvoiceSupplier;
            set => SetPropertyValue(nameof(AlternativeInvoiceSupplier), ref alternativeInvoiceSupplier, value);
        }

        public int FMDVerification
        {
            get => fMDVerification;
            set => SetPropertyValue(nameof(FMDVerification), ref fMDVerification, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string BankName
        {
            get => bankName;
            set => SetPropertyValue(nameof(BankName), ref bankName, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string BankAccountName
        {
            get => bankAccountName;
            set => SetPropertyValue(nameof(BankAccountName), ref bankAccountName, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AccountNumber
        {
            get => accountNumber;
            set => SetPropertyValue(nameof(AccountNumber), ref accountNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SortCode
        {
            get => sortCode;
            set => SetPropertyValue(nameof(SortCode), ref sortCode, value);
        }

        public SupplierPaymentGroup PaymentGroup
        {
            get => paymentGroup;
            set => SetPropertyValue(nameof(PaymentGroup), ref paymentGroup, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string IBAN
        {
            get => iBAN;
            set => SetPropertyValue(nameof(IBAN), ref iBAN, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string BIC
        {
            get => bIC;
            set => SetPropertyValue(nameof(BIC), ref bIC, value);
        }

        public bool RebateSupplier
        {
            get => rebateSupplier;
            set => SetPropertyValue(nameof(RebateSupplier), ref rebateSupplier, value);
        }
    }
}