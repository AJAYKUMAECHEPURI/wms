﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierAnalysis : BaseObject
    {
        public SupplierAnalysis(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private DateTime firstOrderDate;

        public DateTime FirstOrderDate
        {
            get => firstOrderDate;
            set => SetPropertyValue(nameof(FirstOrderDate), ref firstOrderDate, value);
        }
    }
}