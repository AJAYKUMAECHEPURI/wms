﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;
using WMS.App.Module.BusinessObjects.System.Customer;
using WMS.App.Module.BusinessObjects.System.Supplier;
using static WMS.App.Module.BusinessObjects.System.Helpers.SupplierConfig;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierDefaultSetting : BaseObject
    {
        public SupplierDefaultSetting(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string freeShippingValue;
        private string minOrderValue;
        private bool autoGeneratePO;
        private bool disableAutoOrders;
        private bool buildupsForMinimumOrder;
        private bool qualityApproved;
        private SupplierPOType purchaseReportPOType;
        private bool costCenterOverRideVariant;
        private bool departmentCodeOverRideVariant;
        private bool nominalCodeOverRideVariant;
        private bool taxOverRideVariant;
        private SupplierType supplierType;
        private QuantityStatus qualityStatus;
        private StockLocationDetail defaultStockLocation;
        private CostCenter costCenter;
        private DepartmentCode departmentCode;
        private NominalCode nominalCode;
        private TaxRate taxRate;
        private Currency currency;

        public Currency Currency
        {
            get => currency;
            set => SetPropertyValue(nameof(Currency), ref currency, value);
        }

        public TaxRate TaxRate
        {
            get => taxRate;
            set => SetPropertyValue(nameof(TaxRate), ref taxRate, value);
        }

        public NominalCode NominalCode
        {
            get => nominalCode;
            set => SetPropertyValue(nameof(NominalCode), ref nominalCode, value);
        }

        public DepartmentCode DepartmentCode
        {
            get => departmentCode;
            set => SetPropertyValue(nameof(DepartmentCode), ref departmentCode, value);
        }

        public CostCenter CostCenter
        {
            get => costCenter;
            set => SetPropertyValue(nameof(CostCenter), ref costCenter, value);
        }

        public StockLocationDetail DefaultStockLocation
        {
            get => defaultStockLocation;
            set => SetPropertyValue(nameof(DefaultStockLocation), ref defaultStockLocation, value);
        }

        public QuantityStatus QualityStatus
        {
            get => qualityStatus;
            set => SetPropertyValue(nameof(QualityStatus), ref qualityStatus, value);
        }

        public SupplierType SupplierType
        {
            get => supplierType;
            set => SetPropertyValue(nameof(SupplierType), ref supplierType, value);
        }

        public bool TaxOverRideVariant
        {
            get => taxOverRideVariant;
            set => SetPropertyValue(nameof(TaxOverRideVariant), ref taxOverRideVariant, value);
        }

        public bool NominalCodeOverRideVariant
        {
            get => nominalCodeOverRideVariant;
            set => SetPropertyValue(nameof(NominalCodeOverRideVariant), ref nominalCodeOverRideVariant, value);
        }

        public bool DepartmentCodeOverRideVariant
        {
            get => departmentCodeOverRideVariant;
            set => SetPropertyValue(nameof(DepartmentCodeOverRideVariant), ref departmentCodeOverRideVariant, value);
        }

        public bool CostCenterOverRideVariant
        {
            get => costCenterOverRideVariant;
            set => SetPropertyValue(nameof(CostCenterOverRideVariant), ref costCenterOverRideVariant, value);
        }

        public SupplierPOType PurchaseReportPOType
        {
            get => purchaseReportPOType;
            set => SetPropertyValue(nameof(PurchaseReportPOType), ref purchaseReportPOType, value);
        }

        public bool QualityApproved
        {
            get => qualityApproved;
            set => SetPropertyValue(nameof(QualityApproved), ref qualityApproved, value);
        }

        public bool BuildupsForMinimumOrder
        {
            get => buildupsForMinimumOrder;
            set => SetPropertyValue(nameof(BuildupsForMinimumOrder), ref buildupsForMinimumOrder, value);
        }

        public bool DisableAutoOrders
        {
            get => disableAutoOrders;
            set => SetPropertyValue(nameof(DisableAutoOrders), ref disableAutoOrders, value);
        }

        public bool AutoGeneratePO
        {
            get => autoGeneratePO;
            set => SetPropertyValue(nameof(AutoGeneratePO), ref autoGeneratePO, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MinOrderValue
        {
            get => minOrderValue;
            set => SetPropertyValue(nameof(MinOrderValue), ref minOrderValue, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string FreeShippingValue
        {
            get => freeShippingValue;
            set => SetPropertyValue(nameof(FreeShippingValue), ref freeShippingValue, value);
        }
    }
}