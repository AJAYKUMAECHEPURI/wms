﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Supplier
{
    [DefaultClassOptions]
    [NavigationItem("Supplier")]
    public class SupplierContactAccount : BaseObject
    {
        public SupplierContactAccount(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool remittanceAdviceContact;

        public bool RemittanceAdviceContact
        {
            get => remittanceAdviceContact;
            set => SetPropertyValue(nameof(RemittanceAdviceContact), ref remittanceAdviceContact, value);
        }
    }
}