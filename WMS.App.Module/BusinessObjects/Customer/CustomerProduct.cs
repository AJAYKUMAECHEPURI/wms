﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.ProductVariant;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerProduct : CustomBaseObject
    {
        public CustomerProduct(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private Variant variantCode;
        private string commets;
        private string customerDescription;
        private string variantDescription;
        private string customerPartNumber;
        private Customer customer;

        [Association("Customer-Product")]
        public Customer Customer
        {
            get => customer;
            set => SetPropertyValue(nameof(Customer), ref customer, value);
        }

        public Variant VariantCode
        {
            get => variantCode;
            set => SetPropertyValue(nameof(VariantCode), ref variantCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CustomerPartNumber
        {
            get => customerPartNumber;
            set => SetPropertyValue(nameof(CustomerPartNumber), ref customerPartNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantDescription
        {
            get => variantDescription;
            set => SetPropertyValue(nameof(VariantDescription), ref variantDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CustomerDescription
        {
            get => customerDescription;
            set => SetPropertyValue(nameof(CustomerDescription), ref customerDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Comments
        {
            get => commets;
            set => SetPropertyValue(nameof(Comments), ref commets, value);
        }
    }
}