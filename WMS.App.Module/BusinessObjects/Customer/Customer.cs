﻿using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem("Customer")]
    [Appearance("BuyingListRule", TargetItems = "CustomerSettings.Sales.BuyingList", Criteria = "CustomerSettings.Sales.UseBuyingList = false and IsNewObject(this)", Enabled = false)]
    public class Customer : CustomBaseObject
    {
        public Customer(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.CustomerSettings = new CustomerSettings(Session);
            this.DefaultShippingAddress = new CustomerAddress(Session)
            {
                ShippingAddress = true,
                DefaultAddress = true,
                Customer = this
            };
            this.DefaultInvoiceAddress = defaultShippingAddress;
            this.CustomerAnalysis = new CustomerAnalysis(Session);
            this.CreditDetails = new CustomerCreditDetails(Session);
            this.Profile = new CustomerProfile(Session);
        }

        private CustomerProfile profile;
        private string vATNumber;
        private string accountNumber;
        private string eORINumber;
        private CustomerAnalysis customerAnalysis;
        private CustomerCreditDetails creditDetails;
        private CustomerSettings customerSettings;
        private CustomerAddress defaultInvoiceAddress;
        private CustomerAddress defaultShippingAddress;

        [DevExpress.Xpo.Aggregated]
        [VisibleInListView(false)]
        public CustomerSettings CustomerSettings
        {
            get => customerSettings;
            set
            {
                if (customerSettings == value)
                {
                    return;
                }

                SetPropertyValue(nameof(CustomerSettings), ref customerSettings, value);
            }
        }

        [Association("Customer-CustomerAddress"), DevExpress.Xpo.Aggregated]
        public XPCollection<CustomerAddress> CustomerAddress
        {
            get
            {
                return GetCollection<CustomerAddress>(nameof(CustomerAddress));
            }
        }

        [DevExpress.Xpo.Aggregated]
        [VisibleInListView(false)]
        public CustomerCreditDetails CreditDetails
        {
            get => creditDetails;
            set => SetPropertyValue(nameof(CreditDetails), ref creditDetails, value);
        }

        [Association("Customer-Product"), DevExpress.Xpo.Aggregated]
        public XPCollection<CustomerProduct> Products
        {
            get
            {
                return GetCollection<CustomerProduct>(nameof(Products));
            }
        }

        [Association("Customer-Contact"), DevExpress.Xpo.Aggregated]
        public XPCollection<CustomerContact> Contacts
        {
            get { return GetCollection<CustomerContact>(nameof(Contacts)); }
        }

        [DevExpress.Xpo.Aggregated]
        [VisibleInListView(false)]
        public CustomerAnalysis CustomerAnalysis
        {
            get => customerAnalysis;
            set => SetPropertyValue(nameof(CustomerAnalysis), ref customerAnalysis, value);
        }

        [Association("Customer-SpecialPrice"), DevExpress.Xpo.Aggregated]
        public XPCollection<CustomerSpecialPrice> SpecialPrices
        {
            get
            {
                return GetCollection<CustomerSpecialPrice>(nameof(SpecialPrices));
            }
        }

        [Association("Customer-Forcast"), DevExpress.Xpo.Aggregated]
        public XPCollection<CustomerForecast> Forecast
        {
            get
            {
                return GetCollection<CustomerForecast>(nameof(Forecast));
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize), RuleRequiredField("Customer.AccountNumber", DefaultContexts.Save, "Account Number Should not be Empty")]
        public string AccountNumber
        {
            get => accountNumber;
            set => SetPropertyValue(nameof(AccountNumber), ref accountNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [XafDisplayName("VAT Number"), ToolTip("VAT Number")]
        public string VATNumber
        {
            get => vATNumber;
            set => SetPropertyValue(nameof(VATNumber), ref vATNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [XafDisplayName("EORI Number"), ToolTip("Economic Operators Registration and Identification Number")]
        public string EORINumber
        {
            get => eORINumber;
            set => SetPropertyValue(nameof(EORINumber), ref eORINumber, value);
        }

        public CustomerAddress DefaultShippingAddress
        {
            get => defaultShippingAddress;
            set => SetPropertyValue(nameof(DefaultShippingAddress), ref defaultShippingAddress, value);
        }

        public CustomerAddress DefaultInvoiceAddress
        {
            get => defaultInvoiceAddress;
            set => SetPropertyValue(nameof(DefaultInvoiceAddress), ref defaultInvoiceAddress, value);
        }

        [DevExpress.Xpo.Aggregated]
        [VisibleInListView(false)]
        public CustomerProfile Profile
        {
            get => profile;
            set => SetPropertyValue(nameof(Profile), ref profile, value);
        }
    }
}