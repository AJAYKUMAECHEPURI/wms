﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerSettingInvoicing : CustomBaseObject
    {
        public CustomerSettingInvoicing(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string goCardlessMandateID;
        private bool paymentPortalAutoGenerateInvoiceURL;
        private string paymentPortalPassword;
        private string paymentPortal;
        private string gCCLastCCLGenerated;
        private int gCCLNoRegenScheduledCCL;
        private bool generateMobileSalesInvoicesInInvoicing;
        private bool generatePOSInvoicesInInvoicing;
        private bool allowInvoiceBeforeDespatch;
        private bool printDiscountColumnsOnInvoices;
        private bool acceptInvoicesForPartShippedOrders;
        private bool doNotGenerateInvoices;

        public bool DoNotGenerateInvoices
        {
            get => doNotGenerateInvoices;
            set => SetPropertyValue(nameof(DoNotGenerateInvoices), ref doNotGenerateInvoices, value);
        }

        public bool AcceptInvoicesForPartShippedOrders
        {
            get => acceptInvoicesForPartShippedOrders;
            set => SetPropertyValue(nameof(AcceptInvoicesForPartShippedOrders), ref acceptInvoicesForPartShippedOrders, value);
        }

        public bool PrintDiscountColumnsOnInvoices
        {
            get => printDiscountColumnsOnInvoices;
            set => SetPropertyValue(nameof(PrintDiscountColumnsOnInvoices), ref printDiscountColumnsOnInvoices, value);
        }

        public bool AllowInvoiceBeforeDespatch
        {
            get => allowInvoiceBeforeDespatch;
            set => SetPropertyValue(nameof(AllowInvoiceBeforeDespatch), ref allowInvoiceBeforeDespatch, value);
        }

        public bool GeneratePOSInvoicesInInvoicing
        {
            get => generatePOSInvoicesInInvoicing;
            set => SetPropertyValue(nameof(GeneratePOSInvoicesInInvoicing), ref generatePOSInvoicesInInvoicing, value);
        }

        public bool GenerateMobileSalesInvoicesInInvoicing
        {
            get => generateMobileSalesInvoicesInInvoicing;
            set => SetPropertyValue(nameof(GenerateMobileSalesInvoicesInInvoicing), ref generateMobileSalesInvoicesInInvoicing, value);
        }

        public int GCCLNoRegenScheduledCCL
        {
            get => gCCLNoRegenScheduledCCL;
            set => SetPropertyValue(nameof(GCCLNoRegenScheduledCCL), ref gCCLNoRegenScheduledCCL, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string GCCLastCCLGenerated
        {
            get => gCCLastCCLGenerated;
            set => SetPropertyValue(nameof(GCCLastCCLGenerated), ref gCCLastCCLGenerated, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PaymentPortal
        {
            get => paymentPortal;
            set => SetPropertyValue(nameof(PaymentPortal), ref paymentPortal, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PaymentPortalPassword
        {
            get => paymentPortalPassword;
            set => SetPropertyValue(nameof(PaymentPortalPassword), ref paymentPortalPassword, value);
        }

        public bool PaymentPortalAutoGenerateInvoiceURL
        {
            get => paymentPortalAutoGenerateInvoiceURL;
            set => SetPropertyValue(nameof(PaymentPortalAutoGenerateInvoiceURL), ref paymentPortalAutoGenerateInvoiceURL, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string GoCardlessMandateID
        {
            get => goCardlessMandateID;
            set => SetPropertyValue(nameof(GoCardlessMandateID), ref goCardlessMandateID, value);
        }
    }
}