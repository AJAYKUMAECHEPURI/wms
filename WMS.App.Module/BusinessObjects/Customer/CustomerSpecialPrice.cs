﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.ProductVariant;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerSpecialPrice : CustomBaseObject
    {
        public CustomerSpecialPrice(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int qtyTo;
        private int qtyFrom;
        private Decimal dicountPrice;
        private Decimal discountPercentage;
        private Decimal newRSP;
        private string variantDescription;
        private Variant variantCode;
        private Customer customer;

        [Association("Customer-SpecialPrice")]
        public Customer Customer
        {
            get => customer;
            set => SetPropertyValue(nameof(Customer), ref customer, value);
        }

        public Variant VariantCode
        {
            get => variantCode;
            set => SetPropertyValue(nameof(VariantCode), ref variantCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantDescription
        {
            get => variantDescription;
            set => SetPropertyValue(nameof(VariantDescription), ref variantDescription, value);
        }

        public Decimal NewRSP
        {
            get => newRSP;
            set => SetPropertyValue(nameof(NewRSP), ref newRSP, value);
        }

        public Decimal DiscountPercentage
        {
            get => discountPercentage;
            set => SetPropertyValue(nameof(DiscountPercentage), ref discountPercentage, value);
        }

        public Decimal DiscountPrice
        {
            get => dicountPrice;
            set => SetPropertyValue(nameof(DiscountPrice), ref dicountPrice, value);
        }

        public int QtyFrom
        {
            get => qtyFrom;
            set => SetPropertyValue(nameof(QtyFrom), ref qtyFrom, value);
        }

        public int QtyTo
        {
            get => qtyTo;
            set => SetPropertyValue(nameof(QtyTo), ref qtyTo, value);
        }
    }
}