﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerAnalysis : CustomBaseObject
    {
        public CustomerAnalysis(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string specialNote;
        private DateTime lastContactDate;
        private SalesArea salesArea;

        public SalesArea SalesArea
        {
            get => salesArea;
            set => SetPropertyValue(nameof(SalesArea), ref salesArea, value);
        }

        public DateTime LastContactDate
        {
            get => lastContactDate;
            set => SetPropertyValue(nameof(LastContactDate), ref lastContactDate, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SpecialNote
        {
            get => specialNote;
            set => SetPropertyValue(nameof(SpecialNote), ref specialNote, value);
        }
    }
}