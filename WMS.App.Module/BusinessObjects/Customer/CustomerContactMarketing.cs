﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerContactMarketing : CustomBaseObject
    {
        public CustomerContactMarketing(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private CustomerContact contact;

        [Association("CustomerContact-Marketing")]
        public CustomerContact Contact
        {
            get => contact;
            set => SetPropertyValue(nameof(Contact), ref contact, value);
        }

        [Association("Marketing-SegmentGroup")]
        public XPCollection<SegmentGroup> SegmentGroup
        {
            get
            {
                return GetCollection<SegmentGroup>(nameof(SegmentGroup));
            }
        }

        [Association("Marketing-Segment")]
        public XPCollection<StaticSegment> Segment
        {
            get { return GetCollection<StaticSegment>(nameof(Segment)); }
        }
    }
}