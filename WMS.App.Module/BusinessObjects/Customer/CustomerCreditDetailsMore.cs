﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerCreditDetailsMore : CustomBaseObject
    {
        public CustomerCreditDetailsMore(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool excludeDisputeInvoices;
        private int period3;
        private int period2;
        private int period1;
        private int current;
        private int future;

        private int cardPAN;

        private bool obtainAgedBalance;

        private bool alwaysObtainAccountBalance;

        private bool placeOnHoldWhenTransactionAreOverDue;
        private int onHoldGracePeriod;
        private bool donotRecalCreditDetails;

        public int Future
        {
            get => future;
            set => SetPropertyValue(nameof(Future), ref future, value);
        }

        public int Current
        {
            get => current;
            set => SetPropertyValue(nameof(Current), ref current, value);
        }

        public int Period1
        {
            get => period1;
            set => SetPropertyValue(nameof(Period1), ref period1, value);
        }

        public int Period2
        {
            get => period2;
            set => SetPropertyValue(nameof(Period2), ref period2, value);
        }

        public int Period3
        {
            get => period3;
            set => SetPropertyValue(nameof(Period3), ref period3, value);
        }

        public bool AlwaysObtainAccountBalance
        {
            get => alwaysObtainAccountBalance;
            set => SetPropertyValue(nameof(AlwaysObtainAccountBalance), ref alwaysObtainAccountBalance, value);
        }

        public bool ObtainAgedBalance
        {
            get => obtainAgedBalance;
            set => SetPropertyValue(nameof(ObtainAgedBalance), ref obtainAgedBalance, value);
        }

        public bool PlaceOnHoldWhenTransactionAreOverDue
        {
            get => placeOnHoldWhenTransactionAreOverDue;
            set => SetPropertyValue(nameof(PlaceOnHoldWhenTransactionAreOverDue), ref placeOnHoldWhenTransactionAreOverDue, value);
        }

        public int OnHoldGracePeriod
        {
            get => onHoldGracePeriod;
            set => SetPropertyValue(nameof(OnHoldGracePeriod), ref onHoldGracePeriod, value);
        }

        public bool ExcludeDisputeInvoices
        {
            get => excludeDisputeInvoices;
            set => SetPropertyValue(nameof(ExcludeDisputeInvoices), ref excludeDisputeInvoices, value);
        }

        public bool DonotRecalCreditDetails
        {
            get => donotRecalCreditDetails;
            set => SetPropertyValue(nameof(DonotRecalCreditDetails), ref donotRecalCreditDetails, value);
        }

        public int CardPAN
        {
            get => cardPAN;
            set => SetPropertyValue(nameof(CardPAN), ref cardPAN, value);
        }
    }
}