﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerCreditDetailsStoredCards : CustomBaseObject
    {
        public CustomerCreditDetailsStoredCards(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string paymentMethod;

        private DateTime dateAdded;

        private string addedBy;

        private CustomerCreditDetails creditDetails;

        private DateTime expiryDate;

        [Association("CustomerCreditDetails-StoredCards")]
        public CustomerCreditDetails CreditDetails
        {
            get => creditDetails;
            set => SetPropertyValue(nameof(CreditDetails), ref creditDetails, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PaymentMethod
        {
            get => paymentMethod;
            set => SetPropertyValue(nameof(PaymentMethod), ref paymentMethod, value);
        }

        public DateTime ExpiryDate
        {
            get => expiryDate;
            set => SetPropertyValue(nameof(ExpiryDate), ref expiryDate, value);
        }

        public DateTime DateAdded
        {
            get => dateAdded;
            set => SetPropertyValue(nameof(DateAdded), ref dateAdded, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AddedBy
        {
            get => addedBy;
            set => SetPropertyValue(nameof(AddedBy), ref addedBy, value);
        }
    }
}