﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerCreditDetailsGeneral : CustomBaseObject
    {
        public CustomerCreditDetailsGeneral(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int older;
        private ChaseCycle chaseCycle;
        private int paymentTermsSettlementGracePeriod;
        private int paymentTermsSettlementDays;
        private decimal paymentTermsSettlementDiscount;
        private int paymentTerms;
        private DateTime preventHoldOrOverCredit;
        private int offHoldBuffer;
        private bool autoPlaceOrdersOnAndOffHold;
        private bool autoTakeOrdersOffHoldWhenPosting;
        private bool autoPlaceOrdersOnHoldWhenPosting;
        private bool checkCreditLimitOnEveryOrder;
        private bool posting;
        private bool accountStatusOverCreditTerms;
        private bool accountStatusOnhold;
        private bool accountStatusManualOnhold;
        private int orderOnHold;
        private int availableToSpend;
        private int openOrderValue;
        private int unpostedReceipts;
        private string amendedBy;
        private DateTime lastAmendedOn;
        private int accountBalance;
        private int creditLimit;
        private bool calculateCreditUsingPostToAccount;
        private bool accountCustomer;

        public bool AccountCustomer
        {
            get => accountCustomer;
            set => SetPropertyValue(nameof(AccountCustomer), ref accountCustomer, value);
        }

        public bool CalculateCreditUsingPostToAccount
        {
            get => calculateCreditUsingPostToAccount;
            set => SetPropertyValue(nameof(CalculateCreditUsingPostToAccount), ref calculateCreditUsingPostToAccount, value);
        }

        public int CreditLimit
        {
            get => creditLimit;
            set => SetPropertyValue(nameof(CreditLimit), ref creditLimit, value);
        }

        public int AccountBalance
        {
            get => accountBalance;
            set => SetPropertyValue(nameof(AccountBalance), ref accountBalance, value);
        }

        public DateTime LastAmendedOn
        {
            get => lastAmendedOn;
            set => SetPropertyValue(nameof(LastAmendedOn), ref lastAmendedOn, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AmendedBy
        {
            get => amendedBy;
            set => SetPropertyValue(nameof(AmendedBy), ref amendedBy, value);
        }

        public int UnPostedReceipts
        {
            get => unpostedReceipts;
            set => SetPropertyValue(nameof(UnPostedReceipts), ref unpostedReceipts, value);
        }

        public int OpenOrderValue
        {
            get => openOrderValue;
            set => SetPropertyValue(nameof(OpenOrderValue), ref openOrderValue, value);
        }

        public int AvailableToSpend
        {
            get => availableToSpend;
            set => SetPropertyValue(nameof(AvailableToSpend), ref availableToSpend, value);
        }

        public int OrderOnHold
        {
            get => orderOnHold;
            set => SetPropertyValue(nameof(OrderOnHold), ref orderOnHold, value);
        }

        public bool AccountStatusManualOnHold
        {
            get => accountStatusManualOnhold;
            set => SetPropertyValue(nameof(AccountStatusManualOnHold), ref accountStatusManualOnhold, value);
        }

        public bool AccountStatusOnHold
        {
            get => accountStatusOnhold;
            set => SetPropertyValue(nameof(AccountStatusOnHold), ref accountStatusOnhold, value);
        }

        public bool AccountStatusOverCreditTerms
        {
            get => accountStatusOverCreditTerms;
            set => SetPropertyValue(nameof(AccountStatusOverCreditTerms), ref accountStatusOverCreditTerms, value);
        }

        public bool Posting
        {
            get => posting;
            set => SetPropertyValue(nameof(Posting), ref posting, value);
        }

        public bool CheckCreditLimitOnEveryOrder
        {
            get => checkCreditLimitOnEveryOrder;
            set => SetPropertyValue(nameof(CheckCreditLimitOnEveryOrder), ref checkCreditLimitOnEveryOrder, value);
        }

        public bool AutoPlaceOrdersOnHoldWhenPosting
        {
            get => autoPlaceOrdersOnHoldWhenPosting;
            set => SetPropertyValue(nameof(AutoPlaceOrdersOnHoldWhenPosting), ref autoPlaceOrdersOnHoldWhenPosting, value);
        }

        public bool AutoTakeOrdersOffHoldWhenPosting
        {
            get => autoTakeOrdersOffHoldWhenPosting;
            set => SetPropertyValue(nameof(AutoTakeOrdersOffHoldWhenPosting), ref autoTakeOrdersOffHoldWhenPosting, value);
        }

        public bool AutoPlaceOrdersOnAndOffHold
        {
            get => autoPlaceOrdersOnAndOffHold;
            set => SetPropertyValue(nameof(AutoPlaceOrdersOnAndOffHold), ref autoPlaceOrdersOnAndOffHold, value);
        }

        public int OffHoldBuffer
        {
            get => offHoldBuffer;
            set => SetPropertyValue(nameof(OffHoldBuffer), ref offHoldBuffer, value);
        }

        public DateTime PreventHoldOrOverCredit
        {
            get => preventHoldOrOverCredit;
            set => SetPropertyValue(nameof(PreventHoldOrOverCredit), ref preventHoldOrOverCredit, value);
        }

        public int PaymentTerms
        {
            get => paymentTerms;
            set => SetPropertyValue(nameof(PaymentTerms), ref paymentTerms, value);
        }

        public decimal PaymentTermsSettlementDiscount
        {
            get => paymentTermsSettlementDiscount;
            set => SetPropertyValue(nameof(PaymentTermsSettlementDiscount), ref paymentTermsSettlementDiscount, value);
        }

        public int PaymentTermsSettlementDays
        {
            get => paymentTermsSettlementDays;
            set => SetPropertyValue(nameof(PaymentTermsSettlementDays), ref paymentTermsSettlementDays, value);
        }

        public int PaymentTermsSettlementGracePeriod
        {
            get => paymentTermsSettlementGracePeriod;
            set => SetPropertyValue(nameof(PaymentTermsSettlementGracePeriod), ref paymentTermsSettlementGracePeriod, value);
        }

        public ChaseCycle ChaseCycle
        {
            get => chaseCycle;
            set => SetPropertyValue(nameof(ChaseCycle), ref chaseCycle, value);
        }

        public int Older
        {
            get => older;
            set => SetPropertyValue(nameof(Older), ref older, value);
        }
    }
}