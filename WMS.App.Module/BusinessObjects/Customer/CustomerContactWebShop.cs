﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerContactWebShop : CustomBaseObject
    {
        public CustomerContactWebShop(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool editDeliveryAddress;
        private bool primaryAccount;
        private bool status;
        private string webshop;
        private CustomerContact customerContact;

        [Association("CusomerContact-ContactWebshop")]
        public CustomerContact CustomerContact
        {
            get => customerContact;
            set => SetPropertyValue(nameof(CustomerContact), ref customerContact, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string WebShop
        {
            get => webshop;
            set => SetPropertyValue(nameof(WebShop), ref webshop, value);
        }

        public bool Status
        {
            get => status;
            set => SetPropertyValue(nameof(Status), ref status, value);
        }

        public bool PrimaryAccount
        {
            get => primaryAccount;
            set => SetPropertyValue(nameof(PrimaryAccount), ref primaryAccount, value);
        }

        public bool EditDeliveryAddress
        {
            get => editDeliveryAddress;
            set => SetPropertyValue(nameof(EditDeliveryAddress), ref editDeliveryAddress, value);
        }
    }
}