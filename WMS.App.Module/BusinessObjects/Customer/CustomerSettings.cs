﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerSettings : CustomBaseObject
    {
        public CustomerSettings(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.Sales = new CustomerSettingSales(Session);
            this.Despatch = new CustomerSettingDespatch(Session);
            this.Invoicing = new CustomerSettingInvoicing(Session);
            this.LoyaltyScheme = new CustomerSettingLoyaltyScheme(Session);
            this.Account = new CustomerSettingAccount(Session);
        }

        private CustomerSettingAccount account;
        private CustomerSettingLoyaltyScheme loyaltyScheme;
        private CustomerSettingInvoicing invoicing;
        private CustomerSettingDespatch despatch;
        private CustomerSettingSales sales;

        [DevExpress.Xpo.Aggregated]
        public CustomerSettingSales Sales
        {
            get => sales;
            set => SetPropertyValue(nameof(Sales), ref sales, value);
        }

        [DevExpress.Xpo.Aggregated]
        public CustomerSettingDespatch Despatch
        {
            get => despatch;
            set => SetPropertyValue(nameof(Despatch), ref despatch, value);
        }

        [DevExpress.Xpo.Aggregated]
        public CustomerSettingInvoicing Invoicing
        {
            get => invoicing;
            set => SetPropertyValue(nameof(Invoicing), ref invoicing, value);
        }

        [DevExpress.Xpo.Aggregated]
        public CustomerSettingLoyaltyScheme LoyaltyScheme
        {
            get => loyaltyScheme;
            set => SetPropertyValue(nameof(LoyaltyScheme), ref loyaltyScheme, value);
        }

        [Aggregated]
        public CustomerSettingAccount Account
        {
            get => account;
            set => SetPropertyValue(nameof(Account), ref account, value);
        }
    }
}