﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerSettingLoyaltyScheme : CustomBaseObject
    {
        public CustomerSettingLoyaltyScheme(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int totalPointsEarned;
        private int currentPointBalance;
        private DateTime validFrom;
        private LoyaltyScheme loyaltyScheme;

        public LoyaltyScheme LoyaltyScheme
        {
            get => loyaltyScheme;
            set => SetPropertyValue(nameof(LoyaltyScheme), ref loyaltyScheme, value);
        }

        public DateTime ValidFrom
        {
            get => validFrom;
            set => SetPropertyValue(nameof(ValidFrom), ref validFrom, value);
        }

        public int CurrentPointBalance
        {
            get => currentPointBalance;
            set => SetPropertyValue(nameof(CurrentPointBalance), ref currentPointBalance, value);
        }

        public int TotalPointsEarned
        {
            get => totalPointsEarned;
            set => SetPropertyValue(nameof(TotalPointsEarned), ref totalPointsEarned, value);
        }
    }
}