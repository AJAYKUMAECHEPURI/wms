﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Country = WMS.App.Module.BusinessObjects.System.Country;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerContactAddress : CustomBaseObject
    {
        public CustomerContactAddress(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int postcode;
        private Country country;
        private string town;
        private string address3;
        private string address2;
        private string address1;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address1
        {
            get => address1;
            set => SetPropertyValue(nameof(Address1), ref address1, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address2
        {
            get => address2;
            set => SetPropertyValue(nameof(Address2), ref address2, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address3
        {
            get => address3;
            set => SetPropertyValue(nameof(Address3), ref address3, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Town
        {
            get => town;
            set => SetPropertyValue(nameof(Town), ref town, value);
        }

        public Country Segment
        {
            get => country;
            set => SetPropertyValue(nameof(Segment), ref country, value);
        }

        public int Postcode
        {
            get => postcode;
            set => SetPropertyValue(nameof(Postcode), ref postcode, value);
        }
    }
}