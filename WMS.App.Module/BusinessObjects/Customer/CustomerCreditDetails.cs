﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerCreditDetails : CustomBaseObject
    {
        public CustomerCreditDetails(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.more = new CustomerCreditDetailsMore(Session);
            this.general = new CustomerCreditDetailsGeneral(Session);
        }

        private CustomerCreditDetailsGeneral general;
        private CustomerCreditDetailsMore more;

        [DevExpress.Xpo.Aggregated]
        public CustomerCreditDetailsMore More
        {
            get => more;
            set => SetPropertyValue(nameof(More), ref more, value);
        }

        [DevExpress.Xpo.Aggregated]
        public CustomerCreditDetailsGeneral General
        {
            get => general;
            set => SetPropertyValue(nameof(General), ref general, value);
        }

        [Association("CustomerCreditDetails-StoredCards"), DevExpress.Xpo.Aggregated]
        public XPCollection<CustomerCreditDetailsStoredCards> StoredCards
        {
            get
            {
                return GetCollection<CustomerCreditDetailsStoredCards>(nameof(StoredCards));
            }
        }
    }
}