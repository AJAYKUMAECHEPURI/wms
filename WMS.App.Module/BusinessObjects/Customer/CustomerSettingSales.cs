﻿using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;
using WMS.App.Module.BusinessObjects.System.Customer;
using static WMS.App.Module.BusinessObjects.System.Helpers.SystemConfig;
using Currency = WMS.App.Module.BusinessObjects.System.Currency;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerSettingSales : CustomBaseObject
    {
        public CustomerSettingSales(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private AutoApplyDiscountCode autoApplyDiscountCode;
        private SalesPriceIncTaxOverride showSalesPriceIncTaxOverride;
        private bool cRMDefaultToIncludeInCRMRecordValue;
        private bool returnReturnReferenceMustBeUnique;
        private bool mustProvideRefNumOnReturn;
        private bool allowDefaultVariantMultiplierOverride;
        private bool ignoreFreeDelivMethods;
        private bool forceAddrSelectNewOrders;
        private bool rebateCustomer;
        private bool onlyBuyItemsFromPriceLists;
        private bool doNotShowInAndroidApp;
        private bool suppressUnevenPackSizeMessage;
        private bool quickOrderCustomer;
        private bool doNotAllowEPOSOrdersToBePlaced;
        private bool overrideDeliveryTaxRate;
        private bool canPlaceOrdersViaMobileSales;
        private bool acceptsOverrideOfDisallowedVariant;
        private bool createBackToBackOrders;
        private bool doNotAllowPOSOrdersToBePlaced;
        private bool provideOrderRefNumberWhenOrdering;
        private bool orderRefNumberMustBeUnique;
        private bool storeProductsPreviouslySold;
        private bool createSpecialPricesOnOrderSave;
        private bool grossPrice;
        private Decimal fixedDeliveryRate;
        private bool useFixedDeliveryRate;
        private Decimal deliveryThreshold;
        private bool applyFreeDeliveryThreshold;
        private BuyingList buyingList;
        private bool useBuyingList;
        private bool useMultiSaverDiscountGroup;
        private Decimal discount;
        private CustomerDiscountStructure discountStructure;
        private bool useDiscountStructure;
        private PriceList priceList;
        private bool usePriceList;
        private OrderType orderType;
        private StockLocationDetail defaultStockLoaction;
        private bool taxRateOverideVariant;
        private TaxRate taxRate;
        private Currency currency;

        public Currency Currency
        {
            get => currency;
            set => SetPropertyValue(nameof(Currency), ref currency, value);
        }

        public TaxRate TaxRate
        {
            get => taxRate;
            set => SetPropertyValue(nameof(TaxRate), ref taxRate, value);
        }

        public bool TaxRateOverrideVariant
        {
            get => taxRateOverideVariant;
            set => SetPropertyValue(nameof(TaxRateOverrideVariant), ref taxRateOverideVariant, value);
        }

        public StockLocationDetail DefaultStockLocation
        {
            get => defaultStockLoaction;
            set => SetPropertyValue(nameof(DefaultStockLocation), ref defaultStockLoaction, value);
        }

        public OrderType OrderType
        {
            get => orderType;
            set => SetPropertyValue(nameof(OrderType), ref orderType, value);
        }

        public bool UsePriceList
        {
            get => usePriceList;
            set => SetPropertyValue(nameof(UsePriceList), ref usePriceList, value);
        }

        [Appearance("PriceList", Visibility = ViewItemVisibility.Hide, Criteria = "UsePriceList=true", Context = "PriceList", TargetItems = "DiscountStructure", Enabled = false, AppearanceItemType = "ViewItem")]
        public PriceList PriceList
        {
            get => priceList;
            set => SetPropertyValue(nameof(PriceList), ref priceList, value);
        }

        public bool UseDiscountStructure
        {
            get => useDiscountStructure;
            set => SetPropertyValue(nameof(UseDiscountStructure), ref useDiscountStructure, value);
        }

        [Appearance("DiscountStructure", Visibility = ViewItemVisibility.Hide, Criteria = "UseDiscountStructure=true", Context = "DetailView", TargetItems = "DiscountStructure", Enabled = false, AppearanceItemType = "ViewItem")]
        public CustomerDiscountStructure DiscountStructure
        {
            get => discountStructure;
            set => SetPropertyValue(nameof(DiscountStructure), ref discountStructure, value);
        }

        public Decimal Discount
        {
            get => discount;
            set => SetPropertyValue(nameof(Discount), ref discount, value);
        }

        public bool UseMultiSaverDiscountGroup
        {
            get => useMultiSaverDiscountGroup;
            set => SetPropertyValue(nameof(UseMultiSaverDiscountGroup), ref useMultiSaverDiscountGroup, value);
        }

        [ImmediatePostData]
        public bool UseBuyingList
        {
            get => useBuyingList;
            set => SetPropertyValue(nameof(UseBuyingList), ref useBuyingList, value);
        }

        [Appearance("BuyingList", Visibility = ViewItemVisibility.Hide, Criteria = "UseBuyingList=true", Context = "DetailView", TargetItems = "BuyingList", Enabled = false, AppearanceItemType = "ViewItem")]
        public BuyingList BuyingList
        {
            get => buyingList;
            set => SetPropertyValue(nameof(BuyingList), ref buyingList, value);
        }

        public bool ApplyFreeDeliveryThreshold
        {
            get => applyFreeDeliveryThreshold;
            set => SetPropertyValue(nameof(ApplyFreeDeliveryThreshold), ref applyFreeDeliveryThreshold, value);
        }

        [Appearance("DeliveryThreshold", Visibility = ViewItemVisibility.Hide, Criteria = "ApplyFreeDeliveryThreshold=true", Context = "DetailView", TargetItems = "DeliveryThreshold", Enabled = false, AppearanceItemType = "ViewItem")]
        public Decimal DeliveryThreshold
        {
            get => deliveryThreshold;
            set => SetPropertyValue(nameof(DeliveryThreshold), ref deliveryThreshold, value);
        }

        [XafDisplayName("Use fixed delivery price")]
        public bool UseFixedDeliveryRate
        {
            get => useFixedDeliveryRate;
            set => SetPropertyValue(nameof(UseFixedDeliveryRate), ref useFixedDeliveryRate, value);
        }

        [Appearance("FixedDeliveryRate", Visibility = ViewItemVisibility.Hide, Criteria = "UseFixedDeliveryRate=true", Context = "DetailView", TargetItems = "FixedDeliveryRate", Enabled = false, AppearanceItemType = "ViewItem")]
        public Decimal FixedDeliveryRate
        {
            get => fixedDeliveryRate;
            set => SetPropertyValue(nameof(FixedDeliveryRate), ref fixedDeliveryRate, value);
        }

        public bool GrossPrice
        {
            get => grossPrice;
            set => SetPropertyValue(nameof(GrossPrice), ref grossPrice, value);
        }

        [XafDisplayName("Create special prices on order save")]
        public bool CreateSpecialPricesOnOrderSave
        {
            get => createSpecialPricesOnOrderSave;
            set => SetPropertyValue(nameof(CreateSpecialPricesOnOrderSave), ref createSpecialPricesOnOrderSave, value);
        }

        [XafDisplayName("Store products previously sold")]
        public bool StoreProductsPreviouslySold
        {
            get => storeProductsPreviouslySold;
            set => SetPropertyValue(nameof(StoreProductsPreviouslySold), ref storeProductsPreviouslySold, value);
        }

        [XafDisplayName("Order ref number must be unique")]
        public bool OrderRefNumberMustBeUnique
        {
            get => orderRefNumberMustBeUnique;
            set => SetPropertyValue(nameof(OrderRefNumberMustBeUnique), ref orderRefNumberMustBeUnique, value);
        }

        [XafDisplayName("Must provide order ref number when ordering")]
        public bool ProvideOrderRefNumberWhenOrdering
        {
            get => provideOrderRefNumberWhenOrdering;
            set => SetPropertyValue(nameof(ProvideOrderRefNumberWhenOrdering), ref provideOrderRefNumberWhenOrdering, value);
        }

        [XafDisplayName("Do not allow POS orders to be placed")]
        public bool DoNotAllowPOSOrdersToBePlaced
        {
            get => doNotAllowPOSOrdersToBePlaced;
            set => SetPropertyValue(nameof(DoNotAllowPOSOrdersToBePlaced), ref doNotAllowPOSOrdersToBePlaced, value);
        }

        [XafDisplayName("Create back to back orders")]
        public bool CreateBackToBackOrders
        {
            get => createBackToBackOrders;
            set => SetPropertyValue(nameof(CreateBackToBackOrders), ref createBackToBackOrders, value);
        }

        [XafDisplayName("Customer accepts override of disallowed variant")]
        public bool AcceptsOverrideOfDisallowedVariant
        {
            get => acceptsOverrideOfDisallowedVariant;
            set => SetPropertyValue(nameof(AcceptsOverrideOfDisallowedVariant), ref acceptsOverrideOfDisallowedVariant, value);
        }

        [XafDisplayName("Customer can place orders via Mobile Sales")]
        public bool CanPlaceOrdersViaMobileSales
        {
            get => canPlaceOrdersViaMobileSales;
            set => SetPropertyValue(nameof(CanPlaceOrdersViaMobileSales), ref canPlaceOrdersViaMobileSales, value);
        }

        [XafDisplayName("Override delivery tax rate")]
        public bool OverrideDeliveryTaxRate
        {
            get => overrideDeliveryTaxRate;
            set => SetPropertyValue(nameof(OverrideDeliveryTaxRate), ref overrideDeliveryTaxRate, value);
        }

        [XafDisplayName("Do not allow EPOS orders to be placed")]
        public bool DoNotAllowEPOSOrdersToBePlaced
        {
            get => doNotAllowEPOSOrdersToBePlaced;
            set => SetPropertyValue(nameof(DoNotAllowEPOSOrdersToBePlaced), ref doNotAllowEPOSOrdersToBePlaced, value);
        }

        public bool SuppressUnevenPackSizeMessage
        {
            get => suppressUnevenPackSizeMessage;
            set => SetPropertyValue(nameof(SuppressUnevenPackSizeMessage), ref suppressUnevenPackSizeMessage, value);
        }

        [XafDisplayName("Do not show in Android app")]
        public bool DoNotShowInAndroidApp
        {
            get => doNotShowInAndroidApp;
            set => SetPropertyValue(nameof(DoNotShowInAndroidApp), ref doNotShowInAndroidApp, value);
        }

        [XafDisplayName("Only buy items from price lists")]
        public bool OnlyBuyItemsFromPriceLists
        {
            get => onlyBuyItemsFromPriceLists;
            set => SetPropertyValue(nameof(OnlyBuyItemsFromPriceLists), ref onlyBuyItemsFromPriceLists, value);
        }

        [XafDisplayName("Rebate customer")]
        public bool RebateCustomer
        {
            get => rebateCustomer;
            set => SetPropertyValue(nameof(RebateCustomer), ref rebateCustomer, value);
        }

        [XafDisplayName("Force delivery address selection for new sales orders")]
        public bool ForceAddrSelectNewOrders
        {
            get => forceAddrSelectNewOrders;
            set => SetPropertyValue(nameof(ForceAddrSelectNewOrders), ref forceAddrSelectNewOrders, value);
        }

        [XafDisplayName("Ignore exclude from free delivery threshold delivery methods")]
        public bool IgnoreFreeDelivMethods
        {
            get => ignoreFreeDelivMethods;
            set => SetPropertyValue(nameof(IgnoreFreeDelivMethods), ref ignoreFreeDelivMethods, value);
        }

        [XafDisplayName("Allow default variant multiplier override")]
        public bool AllowDefaultVariantMultiplierOverride
        {
            get => allowDefaultVariantMultiplierOverride;
            set => SetPropertyValue(nameof(AllowDefaultVariantMultiplierOverride), ref allowDefaultVariantMultiplierOverride, value);
        }

        [XafDisplayName("Must provide reference number when returning")]
        public bool MustProvideRefNumOnReturn
        {
            get => mustProvideRefNumOnReturn;
            set => SetPropertyValue(nameof(MustProvideRefNumOnReturn), ref mustProvideRefNumOnReturn, value);
        }

        [XafDisplayName("Return reference must be unique")]
        public bool ReturnReturnReferenceMustBeUnique
        {
            get => returnReturnReferenceMustBeUnique;
            set => SetPropertyValue(nameof(ReturnReturnReferenceMustBeUnique), ref returnReturnReferenceMustBeUnique, value);
        }

        [XafDisplayName("Default CRM Record for new orders")]
        public bool CRMDefaultToIncludeInCRMRecordValue
        {
            get => cRMDefaultToIncludeInCRMRecordValue;
            set => SetPropertyValue(nameof(CRMDefaultToIncludeInCRMRecordValue), ref cRMDefaultToIncludeInCRMRecordValue, value);
        }

        public bool QuickOrderCustomer
        {
            get => quickOrderCustomer;
            set => SetPropertyValue(nameof(QuickOrderCustomer), ref quickOrderCustomer, value);
        }

        public SalesPriceIncTaxOverride ShowSalesPriceIncTaxOverride
        {
            get => showSalesPriceIncTaxOverride;
            set => SetPropertyValue(nameof(ShowSalesPriceIncTaxOverride), ref showSalesPriceIncTaxOverride, value);
        }

        public AutoApplyDiscountCode AutoApplyDiscountCode
        {
            get => autoApplyDiscountCode;
            set => SetPropertyValue(nameof(AutoApplyDiscountCode), ref autoApplyDiscountCode, value);
        }
    }
}