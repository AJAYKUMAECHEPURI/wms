﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;
using static WMS.App.Module.BusinessObjects.System.Helpers.SystemConfig;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerSettingDespatch : CustomBaseObject
    {
        public CustomerSettingDespatch(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int shelfLife;
        private string fexExAccountNumber;
        private FMDDespatchAction fMDDespatchAction;
        private DeliveryMethod defaultDeliveryAddressDeliveryMethod;
        private DefaultDeliveryAddressDeliveryGroup defaultDeliveryAddressDeliveryGroup;
        private bool amazonOutputSSCCLabels;
        private bool printPricesOnDeliveryNote;
        private bool allowMultipleOrdersToBePackedTogether;
        private bool acceptPartShipments;
        private bool selectDeliveryMethodOnDespatchOfGoods;
        private bool doNotPrintDeliveryNotesInDespatch;
        private bool printAllOrderLinesOnDeliveryNote;

        public bool PrintAllOrderLinesOnDeliveryNote
        {
            get => printAllOrderLinesOnDeliveryNote;
            set => SetPropertyValue(nameof(PrintAllOrderLinesOnDeliveryNote), ref printAllOrderLinesOnDeliveryNote, value);
        }

        public bool DoNotPrintDeliveryNotesInDespatch
        {
            get => doNotPrintDeliveryNotesInDespatch;
            set => SetPropertyValue(nameof(DoNotPrintDeliveryNotesInDespatch), ref doNotPrintDeliveryNotesInDespatch, value);
        }

        public bool SelectDeliveryMethodOnDespatchOfGoods
        {
            get => selectDeliveryMethodOnDespatchOfGoods;
            set => SetPropertyValue(nameof(SelectDeliveryMethodOnDespatchOfGoods), ref selectDeliveryMethodOnDespatchOfGoods, value);
        }

        public bool AcceptPartShipments
        {
            get => acceptPartShipments;
            set => SetPropertyValue(nameof(AcceptPartShipments), ref acceptPartShipments, value);
        }

        public bool AllowMultipleOrdersToBePackedTogether
        {
            get => allowMultipleOrdersToBePackedTogether;
            set => SetPropertyValue(nameof(AllowMultipleOrdersToBePackedTogether), ref allowMultipleOrdersToBePackedTogether, value);
        }

        public bool PrintPricesOnDeliveryNote
        {
            get => printPricesOnDeliveryNote;
            set => SetPropertyValue(nameof(PrintPricesOnDeliveryNote), ref printPricesOnDeliveryNote, value);
        }

        public DefaultDeliveryAddressDeliveryGroup DefaultDeliveryAddressDeliveryGroup
        {
            get => defaultDeliveryAddressDeliveryGroup;
            set => SetPropertyValue(nameof(DefaultDeliveryAddressDeliveryGroup), ref defaultDeliveryAddressDeliveryGroup, value);
        }

        public DeliveryMethod DefaultDeliveryAddressDeliveryMethod
        {
            get => defaultDeliveryAddressDeliveryMethod;
            set => SetPropertyValue(nameof(DefaultDeliveryAddressDeliveryMethod), ref defaultDeliveryAddressDeliveryMethod, value);
        }

        [XafDisplayName("Minimum acceptable shelf life (days)")]
        public int ShelfLife
        {
            get => shelfLife;
            set => SetPropertyValue(nameof(ShelfLife), ref shelfLife, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string FexExAccountNumber
        {
            get => fexExAccountNumber;
            set => SetPropertyValue(nameof(FexExAccountNumber), ref fexExAccountNumber, value);
        }

        public FMDDespatchAction FMDDespatchAction
        {
            get => fMDDespatchAction;
            set => SetPropertyValue(nameof(FMDDespatchAction), ref fMDDespatchAction, value);
        }

        public bool AmazonOutputSSCCLabels
        {
            get => amazonOutputSSCCLabels;
            set => SetPropertyValue(nameof(AmazonOutputSSCCLabels), ref amazonOutputSSCCLabels, value);
        }
    }
}