﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;
using WMS.App.Module.BusinessObjects.System.Customer;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerContact : CustomBaseObject
    {
        public CustomerContact(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Address = new CustomerContactAddress(Session);
        }

        private CustomerContactAddress address;
        private bool customerStatementContact;
        private bool creditControlContact;
        private NotificationTriggerType notificationTriggerType;
        private int ePOSNumber;
        private string gDPRAmendedBy;
        private DateTime gDPRLastAmendedOn;
        private GDPRLawfulInfo gDPRLawfulBasisToStoreAndProcess;
        private string email;
        private string fax;
        private int mobile;
        private int extension;
        private int telephone;
        private ContactJobRole jobRole;
        private string position;
        private string lastName;
        private string firstName;
        private string name;
        private string title;
        private bool mainContact;
        private bool active;
        private Customer customer;

        [Association("Customer-Contact")]
        public Customer Customer
        {
            get => customer;
            set => SetPropertyValue(nameof(Customer), ref customer, value);
        }

        [Association("CustomerContact-Marketing"), DevExpress.Xpo.Aggregated]
        public XPCollection<CustomerContactMarketing> ContactMarketings
        {
            get
            {
                return GetCollection<CustomerContactMarketing>(nameof(ContactMarketings));
            }
        }

        [DevExpress.Xpo.Aggregated]
        public CustomerContactAddress Address
        {
            get => address;
            set => SetPropertyValue(nameof(Address), ref address, value);
        }

        [Association("CusomerContact-ContactWebshop"), DevExpress.Xpo.Aggregated]
        public XPCollection<CustomerContactWebShop> ContactWebShops
        {
            get
            {
                return GetCollection<CustomerContactWebShop>(nameof(ContactWebShops));
            }
        }

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        public bool MainContact
        {
            get => mainContact;
            set => SetPropertyValue(nameof(MainContact), ref mainContact, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Title
        {
            get => title;
            set => SetPropertyValue(nameof(Title), ref title, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string FirstName
        {
            get => firstName;
            set => SetPropertyValue(nameof(FirstName), ref firstName, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string LastName
        {
            get => lastName;
            set => SetPropertyValue(nameof(LastName), ref lastName, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Position
        {
            get => position;
            set => SetPropertyValue(nameof(Position), ref position, value);
        }

        //[DataSourceCriteria("Type = 'ContactJobRole' AND Status = True")]
        private ContactJobRole JobRole
        {
            get => jobRole;
            set => SetPropertyValue(nameof(JobRole), ref jobRole, value);
        }

        public int Telephone
        {
            get => telephone;
            set => SetPropertyValue(nameof(Telephone), ref telephone, value);
        }

        public int Extension
        {
            get => extension;
            set => SetPropertyValue(nameof(Extension), ref extension, value);
        }

        public int Mobile
        {
            get => mobile;
            set => SetPropertyValue(nameof(Mobile), ref mobile, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Fax
        {
            get => fax;
            set => SetPropertyValue(nameof(Fax), ref fax, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Email
        {
            get => email;
            set => SetPropertyValue(nameof(Email), ref email, value);
        }

        [VisibleInListView(false)]
        [XafDisplayName("Lawful basis to store and process")]
        public GDPRLawfulInfo GDPRLawfulBasisToStoreAndProcess
        {
            get => gDPRLawfulBasisToStoreAndProcess;
            set => SetPropertyValue(nameof(GDPRLawfulBasisToStoreAndProcess), ref gDPRLawfulBasisToStoreAndProcess, value);
        }

        [VisibleInListView(false)]
        [XafDisplayName("Last amended on")]
        public DateTime GDPRLastAmendedOn
        {
            get => gDPRLastAmendedOn;
            set => SetPropertyValue(nameof(GDPRLastAmendedOn), ref gDPRLastAmendedOn, value);
        }

        [VisibleInListView(false)]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [XafDisplayName("Last amended by")]
        public string GDPRAmendedBy
        {
            get => gDPRAmendedBy;
            set => SetPropertyValue(nameof(GDPRAmendedBy), ref gDPRAmendedBy, value);
        }

        [VisibleInListView(false)]
        public int EPOSNumber
        {
            get => ePOSNumber;
            set => SetPropertyValue(nameof(EPOSNumber), ref ePOSNumber, value);
        }

        [VisibleInListView(false)]
        public NotificationTriggerType NotificationTriggerType
        {
            get => notificationTriggerType;
            set => SetPropertyValue(nameof(NotificationTriggerType), ref notificationTriggerType, value);
        }

        [VisibleInListView(false)]
        public bool CreditControlContact
        {
            get => creditControlContact;
            set => SetPropertyValue(nameof(CreditControlContact), ref creditControlContact, value);
        }

        [VisibleInListView(false)]
        public bool CustomerStatementContact
        {
            get => customerStatementContact;
            set => SetPropertyValue(nameof(CustomerStatementContact), ref customerStatementContact, value);
        }
    }
}