﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerSettingAccount : CustomBaseObject
    {
        public CustomerSettingAccount(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int sortCode;
        private int accountNumber;
        private string accountName;
        private bool hasExistingDirectDebit;
        private bool directDebitCustomer;

        public bool DirectDebitCustomer
        {
            get => directDebitCustomer;
            set => SetPropertyValue(nameof(DirectDebitCustomer), ref directDebitCustomer, value);
        }

        public bool HasExistingDirectDebit
        {
            get => hasExistingDirectDebit;
            set => SetPropertyValue(nameof(HasExistingDirectDebit), ref hasExistingDirectDebit, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AccountName
        {
            get => accountName;
            set => SetPropertyValue(nameof(AccountName), ref accountName, value);
        }

        public int AccountNumber
        {
            get => accountNumber;
            set => SetPropertyValue(nameof(AccountNumber), ref accountNumber, value);
        }

        public int SortCode
        {
            get => sortCode;
            set => SetPropertyValue(nameof(SortCode), ref sortCode, value);
        }
    }
}