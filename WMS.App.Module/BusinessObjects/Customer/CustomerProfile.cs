﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Customer;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerProfile : CustomBaseObject
    {
        public CustomerProfile(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private DateTime orderedBy;
        private DateTime orderedOn;
        private String amendedBy;
        private DateTime amendedOn;
        private String inputBy;
        private DateTime inputOn;
        private Boolean automaticallyGetCreditReport;
        private String comments;
        private Decimal growthPotential;
        private Boolean calculateRFMActivityScore;
        private Boolean calculateRFMScore;
        private CustomerSource source;
        private CustomerStatus status;
        private CustomerType type;

        //[DataSourceCriteria("Type = 'CustomerType' AND Status = True")]
        public CustomerType Type
        {
            get => type;
            set => SetPropertyValue(nameof(Type), ref type, value);
        }

        //[DataSourceCriteria("Type = 'CustomerStatus' AND Status = True")]
        public CustomerStatus Status
        {
            get => status;
            set => SetPropertyValue(nameof(Status), ref status, value);
        }

        //[DataSourceCriteria("Type = 'CustomerSource' AND Status = True")]
        public CustomerSource Source
        {
            get => source;
            set => SetPropertyValue(nameof(Source), ref source, value);
        }

        public Boolean CalculateRFMScore
        {
            get => calculateRFMScore;
            set => SetPropertyValue(nameof(CalculateRFMScore), ref calculateRFMScore, value);
        }

        public Boolean CalculateRFMActivityScore
        {
            get => calculateRFMActivityScore;
            set => SetPropertyValue(nameof(CalculateRFMActivityScore), ref calculateRFMActivityScore, value);
        }

        public Decimal GrowthPotential
        {
            get => growthPotential;
            set => SetPropertyValue(nameof(GrowthPotential), ref growthPotential, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public String Comments
        {
            get => comments;
            set => SetPropertyValue(nameof(Comments), ref comments, value);
        }

        public Boolean AutomaticallyGetCreditReport
        {
            get => automaticallyGetCreditReport;
            set => SetPropertyValue(nameof(AutomaticallyGetCreditReport), ref automaticallyGetCreditReport, value);
        }

        public DateTime InputOn
        {
            get => inputOn;
            set => SetPropertyValue(nameof(InputOn), ref inputOn, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public String InputBy
        {
            get => inputBy;
            set => SetPropertyValue(nameof(InputBy), ref inputBy, value);
        }

        public DateTime AmendedOn
        {
            get => amendedOn;
            set => SetPropertyValue(nameof(AmendedOn), ref amendedOn, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public String AmendedBy
        {
            get => amendedBy;
            set => SetPropertyValue(nameof(AmendedBy), ref amendedBy, value);
        }

        public DateTime OrderedOn
        {
            get => orderedOn;
            set => SetPropertyValue(nameof(OrderedOn), ref orderedOn, value);
        }

        public DateTime OrderedBy
        {
            get => orderedBy;
            set => SetPropertyValue(nameof(OrderedBy), ref orderedBy, value);
        }
    }
}