﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;
using WMS.App.Module.BusinessObjects.System.Customer;
using Country = WMS.App.Module.BusinessObjects.System.Country;

namespace WMS.App.Module.BusinessObjects.Customer
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomerAddress : CustomBaseObject
    {
        public CustomerAddress(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private MultiSaverDiscount fMDDespatchAction;
        private Country country;
        private TaxRate taxRate;
        private DeliveryGroup deliveryGroup;
        private DeliveryRoute deliveryRoute;
        private DeliveryMethod deliveryMethod;
        private Customer customer;
        private bool invoiceAddress;
        private bool shippingAddress;
        private bool overrideStockLocation;
        private bool overrideDelivery;
        private bool overrideVariant;
        private bool usingBuyingList;
        private string specialInstructions;
        private string pIDNumber;
        private string aNANumber;
        private bool defaultAddress;
        private bool active;
        private string website;
        private string email;
        private string fax;
        private string telephone;
        private string postcode;
        private string town;
        private string address3;
        private string address2;
        private string address1;
        private string name;
        private BuyingList buyingList;

        [Association("Customer-CustomerAddress")]
        public Customer Customer
        {
            get => customer;
            set => SetPropertyValue(nameof(Customer), ref customer, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address1
        {
            get => address1;
            set => SetPropertyValue(nameof(Address1), ref address1, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address2
        {
            get => address2;
            set => SetPropertyValue(nameof(Address2), ref address2, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address3
        {
            get => address3;
            set => SetPropertyValue(nameof(Address3), ref address3, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Town
        {
            get => town;
            set => SetPropertyValue(nameof(Town), ref town, value);
        }

        public Country Country
        {
            get => country;
            set => SetPropertyValue(nameof(Country), ref country, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Postcode
        {
            get => postcode;
            set => SetPropertyValue(nameof(Postcode), ref postcode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Telephone
        {
            get => telephone;
            set => SetPropertyValue(nameof(Telephone), ref telephone, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Fax
        {
            get => fax;
            set => SetPropertyValue(nameof(Fax), ref fax, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Email
        {
            get => email;
            set => SetPropertyValue(nameof(Email), ref email, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Website
        {
            get => website;
            set => SetPropertyValue(nameof(Website), ref website, value);
        }

        [XafDisplayName("Is Active")]
        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        [XafDisplayName("Is Default Address")]
        public bool DefaultAddress
        {
            get => defaultAddress;
            set => SetPropertyValue(nameof(DefaultAddress), ref defaultAddress, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [XafDisplayName("AVA Number")]
        public string ANANumber
        {
            get => aNANumber;
            set => SetPropertyValue(nameof(ANANumber), ref aNANumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [XafDisplayName("PID Number")]
        public string PIDNumber
        {
            get => pIDNumber;
            set => SetPropertyValue(nameof(PIDNumber), ref pIDNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [XafDisplayName("Special Instructions")]
        [VisibleInListView(false)]
        public string SpecialInstructions
        {
            get => specialInstructions;
            set => SetPropertyValue(nameof(SpecialInstructions), ref specialInstructions, value);
        }

        [XafDisplayName("Use Buying List")]
        [VisibleInListView(false)]
        public bool UsingBuyingList
        {
            get => usingBuyingList;
            set => SetPropertyValue(nameof(UsingBuyingList), ref usingBuyingList, value);
        }

        //[Appearance("BuyingList", Visibility = ViewItemVisibility.Hide, Criteria = "UseBuyingList=true", Context = "DetailView", TargetItems = "BuyingList", Enabled = false, AppearanceItemType = "ViewItem")]        //[Appearance("BuyingList", Visibility = ViewItemVisibility.Hide, Criteria = "UseBuyingList=true", Context = "DetailView", TargetItems = "BuyingList", Enabled = false, AppearanceItemType = "ViewItem")]
        public BuyingList BuyingList
        {
            get => buyingList;
            set => SetPropertyValue(nameof(BuyingList), ref buyingList, value);
        }

        [XafDisplayName("Override Varient")]
        [VisibleInListView(false)]
        public bool OverrideVariant
        {
            get => overrideVariant;
            set => SetPropertyValue(nameof(OverrideVariant), ref overrideVariant, value);
        }

        [XafDisplayName("Override Delivery")]
        [VisibleInListView(false)]
        public bool OverrideDelivery
        {
            get => overrideDelivery;
            set => SetPropertyValue(nameof(OverrideDelivery), ref overrideDelivery, value);
        }

        [XafDisplayName("Override Stock Location")]
        [VisibleInListView(false)]
        public bool OverrideStockLocation
        {
            get => overrideStockLocation;
            set => SetPropertyValue(nameof(OverrideStockLocation), ref overrideStockLocation, value);
        }

        public bool ShippingAddress
        {
            get => shippingAddress;
            set => SetPropertyValue(nameof(ShippingAddress), ref shippingAddress, value);
        }

        public bool InvoiceAddress
        {
            get => invoiceAddress;
            set => SetPropertyValue(nameof(InvoiceAddress), ref invoiceAddress, value);
        }

        [NonPersistent]
        public bool IsDefaultInvoiceAddress
        {
            get { return this.Equals(Customer.DefaultInvoiceAddress); }
            set
            {
                if (value == true)
                {
                    Customer.DefaultInvoiceAddress = this;
                }
            }
        }

        [VisibleInListView(false)]
        public DeliveryGroup DeliveryGroup
        {
            get => deliveryGroup;
            set => SetPropertyValue(nameof(DeliveryGroup), ref deliveryGroup, value);
        }

        [VisibleInListView(false)]
        public DeliveryMethod DeliveryMethod
        {
            get => deliveryMethod;
            set => SetPropertyValue(nameof(DeliveryMethod), ref deliveryMethod, value);
        }

        [VisibleInListView(false)]
        public DeliveryRoute DeliveryRoute
        {
            get => deliveryRoute;
            set => SetPropertyValue(nameof(DeliveryRoute), ref deliveryRoute, value);
        }

        [VisibleInListView(false)]
        public TaxRate TaxRate
        {
            get => taxRate;
            set => SetPropertyValue(nameof(TaxRate), ref taxRate, value);
        }

        [XafDisplayName("FMD Despatch Action")]
        [VisibleInListView(false)]
        public MultiSaverDiscount FMDDespatchAction
        {
            get => fMDDespatchAction;
            set => SetPropertyValue(nameof(FMDDespatchAction), ref fMDDespatchAction, value);
        }
    }
}