﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Stock
{
    [DefaultClassOptions]
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class StockIssue : StockTransaction
    {
        public StockIssue(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.TransactionType = System.Helpers.StockConfig.TransactionType.StockAdjustmentIn;
            base.AfterConstruction();
        }
    }
}