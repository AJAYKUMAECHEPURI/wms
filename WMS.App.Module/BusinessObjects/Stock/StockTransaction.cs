﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.ProductVariant;
using WMS.App.Module.BusinessObjects.System;
using static WMS.App.Module.BusinessObjects.System.Helpers.StockConfig;

namespace WMS.App.Module.BusinessObjects.Stock
{
    [DefaultClassOptions]
    public class StockTransaction : CustomBaseObject
    {
        public StockTransaction(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private TransactionType transactionType;
        private StockLocation location;
        private string serialNumber;
        private string batchNumber;
        private int quantity;
        private Variant variant;

        public Variant Variant
        {
            get => variant;
            set => SetPropertyValue(nameof(Variant), ref variant, value);
        }

        public TransactionType TransactionType
        {
            get => transactionType;
            set => SetPropertyValue(nameof(TransactionType), ref transactionType, value);
        }

        public int Quantity
        {
            get => quantity;
            set => SetPropertyValue(nameof(Quantity), ref quantity, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string BatchNumber
        {
            get => batchNumber;
            set => SetPropertyValue(nameof(BatchNumber), ref batchNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SerialNumber
        {
            get => serialNumber;
            set => SetPropertyValue(nameof(SerialNumber), ref serialNumber, value);
        }

        public StockLocation Location
        {
            get => location;
            set => SetPropertyValue(nameof(Location), ref location, value);
        }

    }
}