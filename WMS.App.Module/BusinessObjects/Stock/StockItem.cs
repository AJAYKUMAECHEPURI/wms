﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System.ComponentModel;
using WMS.App.Module.BusinessObjects.ProductVariant;
using WMS.App.Module.BusinessObjects.System;

namespace WMS.App.Module.BusinessObjects.Stock
{
    [DefaultClassOptions]
    [NavigationItem("Stock")]
    public class StockItem : CustomBaseObject
    {
        public StockItem(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private StockBin bin;
        private string netCostFor;
        private int netCostPer;
        private int netCost;
        private int quantity;
        private DateTime expiryDate;
        private string batchNumber;
        private string serialNumber;
        private StockLocation location;
        private string variantDescription;
        private Variant variant;
        private string gS1BarCode;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string GS1BarCode
        {
            get => gS1BarCode;
            set => SetPropertyValue(nameof(GS1BarCode), ref gS1BarCode, value);
        }

        [Association("Variant-StockItem"), ReadOnly(true)]
        public Variant Variant
        {
            get => variant;
            set => SetPropertyValue(nameof(Variant), ref variant, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantDescription
        {
            get => variantDescription;
            set => SetPropertyValue(nameof(VariantDescription), ref variantDescription, value);
        }

        [ReadOnly(true)]
        public StockLocation Location
        {
            get => location;
            set => SetPropertyValue(nameof(Location), ref location, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize), ReadOnly(true)]
        public string SerialNumber
        {
            get => serialNumber;
            set => SetPropertyValue(nameof(SerialNumber), ref serialNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize), ReadOnly(true)]
        public string BatchNumber
        {
            get => batchNumber;
            set => SetPropertyValue(nameof(BatchNumber), ref batchNumber, value);
        }

        public DateTime ExpiryDate
        {
            get => expiryDate;
            set => SetPropertyValue(nameof(ExpiryDate), ref expiryDate, value);
        }

        public int Quantity
        {
            get => quantity;
            set => SetPropertyValue(nameof(Quantity), ref quantity, value);
        }

        public int NetCost
        {
            get => netCost;
            set => SetPropertyValue(nameof(NetCost), ref netCost, value);
        }

        public int NetCostPer
        {
            get => netCostPer;
            set => SetPropertyValue(nameof(NetCostPer), ref netCostPer, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string NetCostFor
        {
            get => netCostFor;
            set => SetPropertyValue(nameof(NetCostFor), ref netCostFor, value);
        }

        [ReadOnly(true)]
        public StockBin Bin
        {
            get => bin;
            set => SetPropertyValue(nameof(Bin), ref bin, value);
        }
    }
}