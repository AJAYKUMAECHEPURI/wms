﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.Stock
{
    [DefaultClassOptions]
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class StockReceipt : StockTransaction
    {
        public StockReceipt(Session session) : base(session)
        {
        }

        public override void AfterConstruction()
        {
            TransactionType = System.Helpers.StockConfig.TransactionType.StockAdjustmentIn;
        }
    }
}