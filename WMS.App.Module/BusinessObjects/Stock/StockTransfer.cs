﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;

namespace WMS.App.Module.BusinessObjects.Stock
{
    [DefaultClassOptions]
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class StockTransfer : StockTransaction
    {
        public StockTransfer(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.TransactionType = System.Helpers.StockConfig.TransactionType.StockTransfer;
            base.AfterConstruction();
        }

        private int transferQuantity;
        private StockLocation newLocation;
        private string newSerial;
        private string newBatch;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string NewBatch
        {
            get => newBatch;
            set => SetPropertyValue(nameof(NewBatch), ref newBatch, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string NewSerial
        {
            get => newSerial;
            set => SetPropertyValue(nameof(NewSerial), ref newSerial, value);
        }

        public StockLocation NewLocation
        {
            get => newLocation;
            set => SetPropertyValue(nameof(Location), ref newLocation, value);
        }

        public int TransferQuantity
        {
            get => transferQuantity;
            set => SetPropertyValue(nameof(TransferQuantity), ref transferQuantity, value);
        }

        protected override void OnSaving()
        {
            base.OnSaving();
        }
    }
}