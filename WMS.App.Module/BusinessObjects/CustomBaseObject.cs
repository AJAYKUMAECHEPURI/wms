﻿using DevExpress.ExpressApp.Security;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using Microsoft.Extensions.DependencyInjection;

namespace WMS.App.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem(false)]
    public class CustomBaseObject : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        // Use CodeRush to create XPO classes and properties with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/118557
        public CustomBaseObject(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaving()
        {
            var owner = Session.GetObjectByKey<ApplicationUser>(
            Session.ServiceProvider.GetRequiredService<ISecurityStrategyBase>().UserId);
            if (Session.IsNewObject(this))
            {
                DateCreated = DateTime.Now;
                CreatedBy = owner.UserName;
                LastModifiedBy = owner.UserName;
            }
            else
            {
                DateModified = DateTime.Now;
                LastModifiedBy = owner.UserName;
            }
        }

        private string lastModifiedBy;
        private string createdBy;
        private DateTime dateModified;
        private DateTime dateCreated;

        [VisibleInDashboards(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime DateCreated
        {
            get => dateCreated;
            set => SetPropertyValue(nameof(DateCreated), ref dateCreated, value);
        }

        [VisibleInDashboards(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime DateModified
        {
            get => dateModified;
            set => SetPropertyValue(nameof(DateModified), ref dateModified, value);
        }

        [VisibleInDashboards(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CreatedBy
        {
            get => createdBy;
            set => SetPropertyValue(nameof(CreatedBy), ref createdBy, value);
        }

        [VisibleInDashboards(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string LastModifiedBy
        {
            get => lastModifiedBy;
            set => SetPropertyValue(nameof(LastModifiedBy), ref lastModifiedBy, value);
        }
    }
}