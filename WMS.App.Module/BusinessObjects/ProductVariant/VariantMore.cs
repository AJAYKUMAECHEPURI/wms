﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using AggregatedAttribute = DevExpress.Xpo.AggregatedAttribute;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMore : BaseObject
    {
        public VariantMore(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Image = new VariantMoreImage(Session);
        }

        private VariantMoreThirdPartyLogistics logistic;
        private VariantMoreCompetitors competitor;
        private VariantMoreCategory category;
        private VariantMorePackaging packaging;
        private VariantMoreMessage message;
        private VariantMoreListing listing;
        private VariantMoreECommerce eCommerce;
        private VariantMoreDocument document;
        private VariantMoreImage image;

        [Aggregated, VisibleInListView(false)]
        public VariantMoreImage Image
        {
            get => image;
            set => SetPropertyValue(nameof(Image), ref image, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantMoreDocument Document
        {
            get => document;
            set => SetPropertyValue(nameof(Document), ref document, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantMoreECommerce ECommerce
        {
            get => eCommerce;
            set => SetPropertyValue(nameof(ECommerce), ref eCommerce, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantMoreListing Listing
        {
            get => listing;
            set => SetPropertyValue(nameof(Listing), ref listing, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantMoreMessage Message
        {
            get => message;
            set => SetPropertyValue(nameof(Message), ref message, value);
        }

        [Association("More-SuggestedItems"), Aggregated]
        public XPCollection<VariantMoreSuggestedItem> SuggestedItems
        {
            get
            {
                return GetCollection<VariantMoreSuggestedItem>(nameof(SuggestedItems));
            }
        }

        [Aggregated, VisibleInListView(false)]
        public VariantMorePackaging Packaging
        {
            get => packaging;
            set => SetPropertyValue(nameof(Packaging), ref packaging, value);
        }

        [Association("More-AlternateItem"), Aggregated, VisibleInListView(false)]
        public XPCollection<VariantMoreAlternateItem> AlternateItem
        {
            get { return GetCollection<VariantMoreAlternateItem>(nameof(AlternateItem)); }
        }

        [Aggregated, VisibleInListView(false)]
        public VariantMoreCategory Category
        {
            get => category;
            set => SetPropertyValue(nameof(Category), ref category, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantMoreCompetitors Competitor
        {
            get => competitor;
            set => SetPropertyValue(nameof(Competitor), ref competitor, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantMoreThirdPartyLogistics Logistic
        {
            get => logistic;
            set => SetPropertyValue(nameof(Logistic), ref logistic, value);
        }
    }
}