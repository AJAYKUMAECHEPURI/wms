﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;
using static WMS.App.Module.BusinessObjects.System.Helpers.VariantConfig;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMoreGoogleListing : BaseObject
    {
        public VariantMoreGoogleListing(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private VariantMoreListing listing;
        private string earlyRemovalDate;
        private bool noProductIdentifier;
        private bool isBundle;
        private bool groupByProduct;
        private bool adult;
        private bool multipack;
        private bool preOrder;
        private ConditionType conditionType;
        private GoogleProductCategory googleProductCategory;
        private string alternativeCode;
        private string accountName;
        private bool listInGoogle;

        public bool ListInGoogle
        {
            get => listInGoogle;
            set => SetPropertyValue(nameof(ListInGoogle), ref listInGoogle, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AccountName
        {
            get => accountName;
            set => SetPropertyValue(nameof(AccountName), ref accountName, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AlternativeCode
        {
            get => alternativeCode;
            set => SetPropertyValue(nameof(AlternativeCode), ref alternativeCode, value);
        }

        public GoogleProductCategory GoogleProductCategory
        {
            get => googleProductCategory;
            set => SetPropertyValue(nameof(GoogleProductCategory), ref googleProductCategory, value);
        }

        public ConditionType ConditionType
        {
            get => conditionType;
            set => SetPropertyValue(nameof(ConditionType), ref conditionType, value);
        }

        public bool PreOrder
        {
            get => preOrder;
            set => SetPropertyValue(nameof(PreOrder), ref preOrder, value);
        }

        public bool MultiPack
        {
            get => multipack;
            set => SetPropertyValue(nameof(MultiPack), ref multipack, value);
        }

        public bool Adult
        {
            get => adult;
            set => SetPropertyValue(nameof(Adult), ref adult, value);
        }

        public bool GroupByProduct
        {
            get => groupByProduct;
            set => SetPropertyValue(nameof(GroupByProduct), ref groupByProduct, value);
        }

        public bool IsBundle
        {
            get => isBundle;
            set => SetPropertyValue(nameof(IsBundle), ref isBundle, value);
        }

        public bool NoProductIdentifier
        {
            get => noProductIdentifier;
            set => SetPropertyValue(nameof(NoProductIdentifier), ref noProductIdentifier, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string EarlyRemovalDate
        {
            get => earlyRemovalDate;
            set => SetPropertyValue(nameof(EarlyRemovalDate), ref earlyRemovalDate, value);
        }

        [Association("Listing-GoogleListing")]
        public VariantMoreListing Listing
        {
            get => listing;
            set => SetPropertyValue(nameof(Listing), ref listing, value);
        }
    }
}