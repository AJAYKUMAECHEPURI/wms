﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Variant;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantSettingSale : CustomBaseObject
    {
        public VariantSettingSale(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool promptForTransSelect;
        private bool calcSOQtyFromAlloc;
        private bool excludeFromPriceListReport;
        private bool promptForWeightInEPOS;
        private bool doNotDisplayInEPOS;
        private VariantBackOrderRule variantBackOrderRule;
        private bool showSupplierOnAdd;
        private bool includeInAlsoBoughtCalculation;
        private bool disallowDescriptionEditInSalesOrder;
        private bool hideFromVariantFilter;
        private bool restrictToBuyingList;
        private int recommendedSalesMultipler;
        private VariantPostcodeDeliveryMethodRule postcodeDeliveryLevel;
        private int ageRestriction;
        private int priceCheckQuantityBreak;

        public int PriceCheckQuantityBreak
        {
            get => priceCheckQuantityBreak;
            set => SetPropertyValue(nameof(PriceCheckQuantityBreak), ref priceCheckQuantityBreak, value);
        }

        public int AgeRestriction
        {
            get => ageRestriction;
            set => SetPropertyValue(nameof(AgeRestriction), ref ageRestriction, value);
        }

        public VariantPostcodeDeliveryMethodRule PostcodeDeliveryLevel
        {
            get => postcodeDeliveryLevel;
            set => SetPropertyValue(nameof(PostcodeDeliveryLevel), ref postcodeDeliveryLevel, value);
        }

        public int RecommendedSalesMultiple
        {
            get => recommendedSalesMultipler;
            set => SetPropertyValue(nameof(RecommendedSalesMultiple), ref recommendedSalesMultipler, value);
        }

        public bool RestrictToBuyingList
        {
            get => restrictToBuyingList;
            set => SetPropertyValue(nameof(RestrictToBuyingList), ref restrictToBuyingList, value);
        }

        public bool TransformVariant
        {
            get => hideFromVariantFilter;
            set => SetPropertyValue(nameof(TransformVariant), ref hideFromVariantFilter, value);
        }

        public bool DisallowDescriptionEditInSalesOrder
        {
            get => disallowDescriptionEditInSalesOrder;
            set => SetPropertyValue(nameof(DisallowDescriptionEditInSalesOrder), ref disallowDescriptionEditInSalesOrder, value);
        }

        public bool IncludeInAlsoBoughtCalculation
        {
            get => includeInAlsoBoughtCalculation;
            set => SetPropertyValue(nameof(IncludeInAlsoBoughtCalculation), ref includeInAlsoBoughtCalculation, value);
        }

        public bool ShowSupplierOnAdd
        {
            get => showSupplierOnAdd;
            set => SetPropertyValue(nameof(ShowSupplierOnAdd), ref showSupplierOnAdd, value);
        }

        public VariantBackOrderRule VariantBackOrderRule
        {
            get => variantBackOrderRule;
            set => SetPropertyValue(nameof(VariantBackOrderRule), ref variantBackOrderRule, value);
        }

        public bool DoNotDisplayInEPOS
        {
            get => doNotDisplayInEPOS;
            set => SetPropertyValue(nameof(DoNotDisplayInEPOS), ref doNotDisplayInEPOS, value);
        }

        public bool PromptForWeightInEPOS
        {
            get => promptForWeightInEPOS;
            set => SetPropertyValue(nameof(PromptForWeightInEPOS), ref promptForWeightInEPOS, value);
        }

        public bool ExcludeFromPriceListReport
        {
            get => excludeFromPriceListReport;
            set => SetPropertyValue(nameof(ExcludeFromPriceListReport), ref excludeFromPriceListReport, value);
        }

        public bool CalcSOQtyFromAlloc
        {
            get => calcSOQtyFromAlloc;
            set => SetPropertyValue(nameof(CalcSOQtyFromAlloc), ref calcSOQtyFromAlloc, value);
        }

        public bool PromptForTansSelect
        {
            get => promptForTransSelect;
            set => SetPropertyValue(nameof(PromptForTansSelect), ref promptForTransSelect, value);
        }
    }
}