﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantSettingRobotic : CustomBaseObject
    {
        public VariantSettingRobotic(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool limitReplenishmentToOneBatchPerBin;
        private bool outputReplenishmentVariantTransactionLabel;
        private bool outputOneRobotPickingLabelIrrespectiveOfQuantity;
        private bool outputRobotPickingLable;
        private bool allowShippingFromStation;
        private bool requireScanPerItem;

        public bool RequireScanPerItem
        {
            get => requireScanPerItem;
            set => SetPropertyValue(nameof(RequireScanPerItem), ref requireScanPerItem, value);
        }

        public bool AllowShippingFromStation
        {
            get => allowShippingFromStation;
            set => SetPropertyValue(nameof(AllowShippingFromStation), ref allowShippingFromStation, value);
        }

        public bool OutputRobotPickingLabel
        {
            get => outputRobotPickingLable;
            set => SetPropertyValue(nameof(OutputRobotPickingLabel), ref outputRobotPickingLable, value);
        }

        public bool OutputOneRobotPickingLabelIrrespectiveOfQuantity
        {
            get => outputOneRobotPickingLabelIrrespectiveOfQuantity;
            set => SetPropertyValue(nameof(OutputOneRobotPickingLabelIrrespectiveOfQuantity), ref outputOneRobotPickingLabelIrrespectiveOfQuantity, value);
        }

        public bool OutputReplenishmentVariantTransactionLabel
        {
            get => outputReplenishmentVariantTransactionLabel;
            set => SetPropertyValue(nameof(OutputReplenishmentVariantTransactionLabel), ref outputReplenishmentVariantTransactionLabel, value);
        }

        public bool LimitReplenishmentToOneBatchPerBin
        {
            get => limitReplenishmentToOneBatchPerBin;
            set => SetPropertyValue(nameof(LimitReplenishmentToOneBatchPerBin), ref limitReplenishmentToOneBatchPerBin, value);
        }
    }
}