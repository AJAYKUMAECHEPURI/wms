﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMoreImage : BaseObject
    {
        public VariantMoreImage(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private FileData image;
        private bool showOnHHT;
        private string customTag;
        private bool hoverImage;
        private string altText;
        private string uRL;
        private bool uploaded;
        private bool exported;
        private bool defaultImage;
        private string note;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Note
        {
            get => note;
            set => SetPropertyValue(nameof(Note), ref note, value);
        }

        public bool DefaultImage
        {
            get => defaultImage;
            set => SetPropertyValue(nameof(DefaultImage), ref defaultImage, value);
        }

        public bool Exported
        {
            get => exported;
            set => SetPropertyValue(nameof(Exported), ref exported, value);
        }

        public bool Uploaded
        {
            get => uploaded;
            set => SetPropertyValue(nameof(Uploaded), ref uploaded, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string URL
        {
            get => uRL;
            set => SetPropertyValue(nameof(URL), ref uRL, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AltText
        {
            get => altText;
            set => SetPropertyValue(nameof(AltText), ref altText, value);
        }

        public bool HoverImage
        {
            get => hoverImage;
            set => SetPropertyValue(nameof(HoverImage), ref hoverImage, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CustomTag
        {
            get => customTag;
            set => SetPropertyValue(nameof(CustomTag), ref customTag, value);
        }

        public bool ShowOnHHT
        {
            get => showOnHHT;
            set => SetPropertyValue(nameof(ShowOnHHT), ref showOnHHT, value);
        }

        public FileData Image
        {
            get => image;
            set => SetPropertyValue(nameof(Image), ref image, value);
        }
    }
}