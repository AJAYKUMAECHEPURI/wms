﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantSettingReturn : CustomBaseObject
    {
        public VariantSettingReturn(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string inspectionInstructions;
        private string defaultQuarantineReason;
        private int noOfDaysToQuarantine;
        private bool quarantineReturnedStockByDefaultWhenBookingIn;
        private int warrantyLength;
        private int returnPolicyLength;
        private bool canBeReturned;

        public bool CanBeReturned
        {
            get => canBeReturned;
            set => SetPropertyValue(nameof(CanBeReturned), ref canBeReturned, value);
        }

        public int ReturnPolicyLength
        {
            get => returnPolicyLength;
            set => SetPropertyValue(nameof(ReturnPolicyLength), ref returnPolicyLength, value);
        }

        public int WarrantyLength
        {
            get => warrantyLength;
            set => SetPropertyValue(nameof(WarrantyLength), ref warrantyLength, value);
        }

        public bool QuarantineReturnedStockByDefaultWhenBookingIn
        {
            get => quarantineReturnedStockByDefaultWhenBookingIn;
            set => SetPropertyValue(nameof(QuarantineReturnedStockByDefaultWhenBookingIn), ref quarantineReturnedStockByDefaultWhenBookingIn, value);
        }

        public int NoOfDaysToQuarantine
        {
            get => noOfDaysToQuarantine;
            set => SetPropertyValue(nameof(NoOfDaysToQuarantine), ref noOfDaysToQuarantine, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string DefaultQuarantineReason
        {
            get => defaultQuarantineReason;
            set => SetPropertyValue(nameof(DefaultQuarantineReason), ref defaultQuarantineReason, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string InspectionInstructions
        {
            get => inspectionInstructions;
            set => SetPropertyValue(nameof(InspectionInstructions), ref inspectionInstructions, value);
        }
    }
}