﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;
using WMS.App.Module.BusinessObjects.System.Customer;
using static WMS.App.Module.BusinessObjects.System.Helpers.VariantConfig;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantPurchase : CustomBaseObject
    {
        public VariantPurchase(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int fixedCost;
        private POCostTrough pOCostTrough;
        private int minimumAcceptableShelfLife;
        private EstimatedCostSettings estimatedCostSetting;
        private DateTime lastAmendedOn;
        private int avgCostGoodsLastDays;
        private int costPer;
        private int estimatedCost;
        private bool costperBaseUnitInPO;
        private bool disallowDescEditPO;
        private bool allowUpdatePurchaseQtyIn;
        private bool absorbDiscSurchargeDefault;
        private bool absorbPOShippingByDefault;
        private bool onlyOrderFromRegisteredSuppliers;
        private bool autoCreateAlternativeSuppliers;
        private CostCenter costCenter;
        private DepartmentCode departmentCode;
        private NominalCode nominalCode;
        private TaxRate taxRate;

        public TaxRate TaxRate
        {
            get => taxRate;
            set => SetPropertyValue(nameof(TaxRate), ref taxRate, value);
        }

        public NominalCode NominalCode
        {
            get => nominalCode;
            set => SetPropertyValue(nameof(NominalCode), ref nominalCode, value);
        }

        public DepartmentCode DepartmentCode
        {
            get => departmentCode;
            set => SetPropertyValue(nameof(DepartmentCode), ref departmentCode, value);
        }

        public CostCenter CostCenter
        {
            get => costCenter;
            set => SetPropertyValue(nameof(CostCenter), ref costCenter, value);
        }

        public bool AutoCreateAlternativeSuppliers
        {
            get => autoCreateAlternativeSuppliers;
            set => SetPropertyValue(nameof(AutoCreateAlternativeSuppliers), ref autoCreateAlternativeSuppliers, value);
        }

        public bool OnlyOrderFromRegisteredSuppliers
        {
            get => onlyOrderFromRegisteredSuppliers;
            set => SetPropertyValue(nameof(OnlyOrderFromRegisteredSuppliers), ref onlyOrderFromRegisteredSuppliers, value);
        }

        public bool AbsorbPOShippingByDefault
        {
            get => absorbPOShippingByDefault;
            set => SetPropertyValue(nameof(AbsorbPOShippingByDefault), ref absorbPOShippingByDefault, value);
        }

        public bool AbsorbDiscSurchargeDefault
        {
            get => absorbDiscSurchargeDefault;
            set => SetPropertyValue(nameof(AbsorbDiscSurchargeDefault), ref absorbDiscSurchargeDefault, value);
        }

        public bool AllowUpdatePurchaseQtyIn
        {
            get => allowUpdatePurchaseQtyIn;
            set => SetPropertyValue(nameof(AllowUpdatePurchaseQtyIn), ref allowUpdatePurchaseQtyIn, value);
        }

        public bool DisallowDescEditPO
        {
            get => disallowDescEditPO;
            set => SetPropertyValue(nameof(DisallowDescEditPO), ref disallowDescEditPO, value);
        }

        public bool CostPerBaseUnitInPO
        {
            get => costperBaseUnitInPO;
            set => SetPropertyValue(nameof(CostPerBaseUnitInPO), ref costperBaseUnitInPO, value);
        }

        public int EstimatedCost
        {
            get => estimatedCost;
            set => SetPropertyValue(nameof(EstimatedCost), ref estimatedCost, value);
        }

        public int CostPer
        {
            get => costPer;
            set => SetPropertyValue(nameof(CostPer), ref costPer, value);
        }

        public DateTime LastAmendedOn
        {
            get => lastAmendedOn;
            set => SetPropertyValue(nameof(LastAmendedOn), ref lastAmendedOn, value);
        }

        public int AvgCostGoodsLastDays
        {
            get => avgCostGoodsLastDays;
            set => SetPropertyValue(nameof(AvgCostGoodsLastDays), ref avgCostGoodsLastDays, value);
        }

        public EstimatedCostSettings EstimatedCostSetting
        {
            get => estimatedCostSetting;
            set => SetPropertyValue(nameof(EstimatedCostSetting), ref estimatedCostSetting, value);
        }

        public int MinimumAcceptableShelfLife
        {
            get => minimumAcceptableShelfLife;
            set => SetPropertyValue(nameof(MinimumAcceptableShelfLife), ref minimumAcceptableShelfLife, value);
        }

        public POCostTrough POCostTrough
        {
            get => pOCostTrough;
            set => SetPropertyValue(nameof(POCostTrough), ref pOCostTrough, value);
        }

        public int FixedCost
        {
            get => fixedCost;
            set => SetPropertyValue(nameof(FixedCost), ref fixedCost, value);
        }
    }
}