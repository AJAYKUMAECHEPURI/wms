﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.VariantConfig;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantPurchaseReport : CustomBaseObject
    {
        public VariantPurchaseReport(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private BufferIntoPurchaseOrder bufferIntoPurchaseOrder;
        private int stockInBuffer;
        private LeadTimeToPurchaseOrder leadTimeToPurchaseOrder;
        private int leadTime;
        private int maxTransactionQuantityToExclude;
        private bool excludeTransFromPO;
        private bool excludeQuarantineFromPO;

        public bool ExcludeQuarantineFromPO
        {
            get => excludeQuarantineFromPO;
            set => SetPropertyValue(nameof(ExcludeQuarantineFromPO), ref excludeQuarantineFromPO, value);
        }

        public bool ExcludeTansFromPO
        {
            get => excludeTransFromPO;
            set => SetPropertyValue(nameof(ExcludeTansFromPO), ref excludeTransFromPO, value);
        }

        public int MaxTransactionQuantityToExclude
        {
            get => maxTransactionQuantityToExclude;
            set => SetPropertyValue(nameof(MaxTransactionQuantityToExclude), ref maxTransactionQuantityToExclude, value);
        }

        public int LeadTime
        {
            get => leadTime;
            set => SetPropertyValue(nameof(LeadTime), ref leadTime, value);
        }

        public LeadTimeToPurchaseOrder LeadTimeToPurchaseOrder
        {
            get => leadTimeToPurchaseOrder;
            set => SetPropertyValue(nameof(LeadTimeToPurchaseOrder), ref leadTimeToPurchaseOrder, value);
        }

        public int StockInBuffer
        {
            get => stockInBuffer;
            set => SetPropertyValue(nameof(StockInBuffer), ref stockInBuffer, value);
        }

        public BufferIntoPurchaseOrder BufferIntoPurchaseOrder
        {
            get => bufferIntoPurchaseOrder;
            set => SetPropertyValue(nameof(BufferIntoPurchaseOrder), ref bufferIntoPurchaseOrder, value);
        }
    }
}