﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Variant;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantSettingTransformation : CustomBaseObject
    {
        public VariantSettingTransformation(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool autoTansformIntoGoodsLocation;
        private bool printTransLabelsOnIn;
        private bool useTransactionBinNumber;
        private int bin;
        private int stockLoaction;
        private string variantToTransformInfo;
        private bool autoTransformOnBooking;
        private bool allTransformableForRepl;
        private int additionalCostWhenTransformingUp;
        private bool allowTransformUp;
        private bool addToTransformList;
        private int additionalCostWhenTransformingDown;
        private bool stockCanBeTransformed;

        public bool StockCanBeTransformed
        {
            get => stockCanBeTransformed;
            set => SetPropertyValue(nameof(StockCanBeTransformed), ref stockCanBeTransformed, value);
        }

        public int AdditionalCostWhenTransformingDown
        {
            get => additionalCostWhenTransformingDown;
            set => SetPropertyValue(nameof(AdditionalCostWhenTransformingDown), ref additionalCostWhenTransformingDown, value);
        }

        public bool AddToTransformList
        {
            get => addToTransformList;
            set => SetPropertyValue(nameof(AddToTransformList), ref addToTransformList, value);
        }

        public bool AllowTransformUp
        {
            get => allowTransformUp;
            set => SetPropertyValue(nameof(AllowTransformUp), ref allowTransformUp, value);
        }

        public int AdditionalCostWhenTransformingUp
        {
            get => additionalCostWhenTransformingUp;
            set => SetPropertyValue(nameof(AdditionalCostWhenTransformingUp), ref additionalCostWhenTransformingUp, value);
        }

        public bool AllTransformableForRep
        {
            get => allTransformableForRepl;
            set => SetPropertyValue(nameof(AllTransformableForRep), ref allTransformableForRepl, value);
        }

        public bool AutoTransformOnBooking
        {
            get => autoTransformOnBooking;
            set => SetPropertyValue(nameof(AutoTransformOnBooking), ref autoTransformOnBooking, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantToTransformInfo
        {
            get => variantToTransformInfo;
            set => SetPropertyValue(nameof(VariantToTransformInfo), ref variantToTransformInfo, value);
        }

        public int StockLocation
        {
            get => stockLoaction;
            set => SetPropertyValue(nameof(StockLocation), ref stockLoaction, value);
        }

        public int Bin
        {
            get => bin;
            set => SetPropertyValue(nameof(Bin), ref bin, value);
        }

        public bool UseTransactionBinNumber
        {
            get => useTransactionBinNumber;
            set => SetPropertyValue(nameof(UseTransactionBinNumber), ref useTransactionBinNumber, value);
        }

        public bool PrintTansLabelsOnIn
        {
            get => printTransLabelsOnIn;
            set => SetPropertyValue(nameof(PrintTansLabelsOnIn), ref printTransLabelsOnIn, value);
        }

        public bool AutoTransformIntoGoodsLocation
        {
            get => autoTansformIntoGoodsLocation;
            set => SetPropertyValue(nameof(AutoTransformIntoGoodsLocation), ref autoTansformIntoGoodsLocation, value);
        }

        [Association("VariantSettingTransformation-TransformVariant"), DevExpress.Xpo.Aggregated]
        public XPCollection<TransformVariant> TransformVariant
        {
            get
            {
                return GetCollection<TransformVariant>(nameof(TransformVariant));
            }
        }

        [Association("VariantSettingTransformation-ReplenishmentVariant"), DevExpress.Xpo.Aggregated]
        public XPCollection<ReplenishmentVariant> ReplenishmentVariant
        {
            get
            {
                return GetCollection<ReplenishmentVariant>(nameof(ReplenishmentVariant));
            }
        }
    }
}