﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantSettingManufacturing : CustomBaseObject
    {
        public VariantSettingManufacturing(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool autoUsePreBuildOnSOSave;
        private bool autoAllocCompOnOrder;
        private bool noBreakDownKitOnIn;
        private bool skipWOAllocOnSave;
        private string serialNumberSuffix;
        private string serialNumberPrefix;
        private bool printGoodsLabelManualWO;
        private bool subtractProdCostsFromBWO;
        private bool useRevCostInSOEstCost;
        private bool alwaysBuildToOrder;
        private bool kit;
        private bool manufacturing;

        public bool Manufacturing
        {
            get => manufacturing;
            set => SetPropertyValue(nameof(Manufacturing), ref manufacturing, value);
        }

        public bool Kit
        {
            get => kit;
            set => SetPropertyValue(nameof(Kit), ref kit, value);
        }

        public bool AlwaysBuildToOrder
        {
            get => alwaysBuildToOrder;
            set => SetPropertyValue(nameof(AlwaysBuildToOrder), ref alwaysBuildToOrder, value);
        }

        public bool UseRevCostInSOEstCost
        {
            get => useRevCostInSOEstCost;
            set => SetPropertyValue(nameof(UseRevCostInSOEstCost), ref useRevCostInSOEstCost, value);
        }

        public bool SubtractProdCostsFromBWO
        {
            get => subtractProdCostsFromBWO;
            set => SetPropertyValue(nameof(SubtractProdCostsFromBWO), ref subtractProdCostsFromBWO, value);
        }

        public bool PrintGoodsLabelManualWO
        {
            get => printGoodsLabelManualWO;
            set => SetPropertyValue(nameof(PrintGoodsLabelManualWO), ref printGoodsLabelManualWO, value);
        }

        public bool AutoUsePreBuildOnSOSave
        {
            get => autoUsePreBuildOnSOSave;
            set => SetPropertyValue(nameof(AutoUsePreBuildOnSOSave), ref autoUsePreBuildOnSOSave, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SerialNumberPrefix
        {
            get => serialNumberPrefix;
            set => SetPropertyValue(nameof(SerialNumberPrefix), ref serialNumberPrefix, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SerialNumberSuffix
        {
            get => serialNumberSuffix;
            set => SetPropertyValue(nameof(SerialNumberSuffix), ref serialNumberSuffix, value);
        }

        public bool SkipWOAllocOnSave
        {
            get => skipWOAllocOnSave;
            set => SetPropertyValue(nameof(SkipWOAllocOnSave), ref skipWOAllocOnSave, value);
        }

        public bool NoBreakDownKitOnIn
        {
            get => noBreakDownKitOnIn;
            set => SetPropertyValue(nameof(NoBreakDownKitOnIn), ref noBreakDownKitOnIn, value);
        }

        public bool AutoAllocCompOnOrder
        {
            get => autoAllocCompOnOrder;
            set => SetPropertyValue(nameof(AutoAllocCompOnOrder), ref autoAllocCompOnOrder, value);
        }
    }
}