﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMoreCompetitors : BaseObject
    {
        public VariantMoreCompetitors(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private DateTime endDate;
        private DateTime startDate;
        private string partNumber;
        private string sellingPrice;
        private string competitorName;
        private bool mainCompetitor;
        private bool activeCompetitor;

        public bool ActiveCompetitor
        {
            get => activeCompetitor;
            set => SetPropertyValue(nameof(ActiveCompetitor), ref activeCompetitor, value);
        }

        public bool MainCompetitor
        {
            get => mainCompetitor;
            set => SetPropertyValue(nameof(MainCompetitor), ref mainCompetitor, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CompetitorName
        {
            get => competitorName;
            set => SetPropertyValue(nameof(CompetitorName), ref competitorName, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SellingPrice
        {
            get => sellingPrice;
            set => SetPropertyValue(nameof(SellingPrice), ref sellingPrice, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PartNumber
        {
            get => partNumber;
            set => SetPropertyValue(nameof(PartNumber), ref partNumber, value);
        }

        public DateTime StartDate
        {
            get => startDate;
            set => SetPropertyValue(nameof(StartDate), ref startDate, value);
        }

        public DateTime EndDate
        {
            get => endDate;
            set => SetPropertyValue(nameof(EndDate), ref endDate, value);
        }
    }
}