﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMoreThirdPartyLogistics : BaseObject
    {
        public VariantMoreThirdPartyLogistics(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string adHocDescription3;
        private string adHocDescription2;
        private string adHocDescription1;
        private string adHocCharge3;
        private string adHocCharge2;
        private string adHocCharge1;
        private int returnRateThereAfter;
        private int returnQty;
        private int returnRate;
        private int deliveryRateThereAfter;
        private int deliveryQty;
        private int deliveryRate;
        private int packRateThereAfter;
        private int packQty;
        private int packRate;
        private int pickRateThereAfter;
        private int pickQty;
        private int pickRate;
        private int handlingRateThereAfter;
        private int handlingQty;
        private int handlingRate;
        private int putAwayRateThereAfter;
        private int putAwayQty;
        private int putAwayRate;
        private int receiveRateThereAfter;
        private int receiveQty;
        private int receiveRate;
        private string customer;
        private bool thirdPartyLogisticsVariant;

        public bool ThirdPartyLogisticsVariant
        {
            get => thirdPartyLogisticsVariant;
            set => SetPropertyValue(nameof(ThirdPartyLogisticsVariant), ref thirdPartyLogisticsVariant, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Customer
        {
            get => customer;
            set => SetPropertyValue(nameof(Customer), ref customer, value);
        }

        public int ReceiveRate
        {
            get => receiveRate;
            set => SetPropertyValue(nameof(ReceiveRate), ref receiveRate, value);
        }

        public int ReceiveQty
        {
            get => receiveQty;
            set => SetPropertyValue(nameof(ReceiveQty), ref receiveQty, value);
        }

        public int ReceiveRateThereAfter
        {
            get => receiveRateThereAfter;
            set => SetPropertyValue(nameof(ReceiveRateThereAfter), ref receiveRateThereAfter, value);
        }

        public int PutAwayRate
        {
            get => putAwayRate;
            set => SetPropertyValue(nameof(PutAwayRate), ref putAwayRate, value);
        }

        public int PutAwayQty
        {
            get => putAwayQty;
            set => SetPropertyValue(nameof(PutAwayQty), ref putAwayQty, value);
        }

        public int PutAwayRateThereAfter
        {
            get => putAwayRateThereAfter;
            set => SetPropertyValue(nameof(PutAwayRateThereAfter), ref putAwayRateThereAfter, value);
        }

        public int HandlingRate
        {
            get => handlingRate;
            set => SetPropertyValue(nameof(HandlingRate), ref handlingRate, value);
        }

        public int HandlingQty
        {
            get => handlingQty;
            set => SetPropertyValue(nameof(HandlingQty), ref handlingQty, value);
        }

        public int HandlingRateThereAfter
        {
            get => handlingRateThereAfter;
            set => SetPropertyValue(nameof(HandlingRateThereAfter), ref handlingRateThereAfter, value);
        }

        public int PickRate
        {
            get => pickRate;
            set => SetPropertyValue(nameof(PickRate), ref pickRate, value);
        }

        public int PickQty
        {
            get => pickQty;
            set => SetPropertyValue(nameof(PickQty), ref pickQty, value);
        }

        public int PickRateThereAfter
        {
            get => pickRateThereAfter;
            set => SetPropertyValue(nameof(PickRateThereAfter), ref pickRateThereAfter, value);
        }

        public int PackRate
        {
            get => packRate;
            set => SetPropertyValue(nameof(PackRate), ref packRate, value);
        }

        public int PackQty
        {
            get => packQty;
            set => SetPropertyValue(nameof(PackQty), ref packQty, value);
        }

        public int PackRateThereAfter
        {
            get => packRateThereAfter;
            set => SetPropertyValue(nameof(PackRateThereAfter), ref packRateThereAfter, value);
        }

        public int DeliveryRate
        {
            get => deliveryRate;
            set => SetPropertyValue(nameof(DeliveryRate), ref deliveryRate, value);
        }

        public int DeliveryQty
        {
            get => deliveryQty;
            set => SetPropertyValue(nameof(DeliveryQty), ref deliveryQty, value);
        }

        public int DeliveryRateThereAfter
        {
            get => deliveryRateThereAfter;
            set => SetPropertyValue(nameof(DeliveryRateThereAfter), ref deliveryRateThereAfter, value);
        }

        public int ReturnRate
        {
            get => returnRate;
            set => SetPropertyValue(nameof(ReturnRate), ref returnRate, value);
        }

        public int ReturnQty
        {
            get => returnQty;
            set => SetPropertyValue(nameof(ReturnQty), ref returnQty, value);
        }

        public int ReturnRateThereAfter
        {
            get => returnRateThereAfter;
            set => SetPropertyValue(nameof(ReturnRateThereAfter), ref returnRateThereAfter, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AdHocCharge1
        {
            get => adHocCharge1;
            set => SetPropertyValue(nameof(AdHocCharge1), ref adHocCharge1, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AdHocCharge2
        {
            get => adHocCharge2;
            set => SetPropertyValue(nameof(AdHocCharge2), ref adHocCharge2, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AdHocCharge3
        {
            get => adHocCharge3;
            set => SetPropertyValue(nameof(AdHocCharge3), ref adHocCharge3, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AdHocDescription1
        {
            get => adHocDescription1;
            set => SetPropertyValue(nameof(AdHocDescription1), ref adHocDescription1, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AdHocDescription2
        {
            get => adHocDescription2;
            set => SetPropertyValue(nameof(AdHocDescription2), ref adHocDescription2, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AdHocDescription3
        {
            get => adHocDescription3;
            set => SetPropertyValue(nameof(AdHocDescription3), ref adHocDescription3, value);
        }
    }
}