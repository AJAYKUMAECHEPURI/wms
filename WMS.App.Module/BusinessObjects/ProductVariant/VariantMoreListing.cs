﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMoreListing : BaseObject
    {
        public VariantMoreListing(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        [Association("Listing-AmazonListing"), DevExpress.Xpo.Aggregated]
        public XPCollection<VariantMoreAmazonListing> AmazonListing
        {
            get { return GetCollection<VariantMoreAmazonListing>(nameof(AmazonListing)); }
        }

        [Association("Listing-EBayListing"), DevExpress.Xpo.Aggregated]
        public XPCollection<VariantMoreEBayListing> EBayListing
        {
            get
            {
                return GetCollection<VariantMoreEBayListing>(nameof(EBayListing));
            }
        }

        [Association("Listing-GoogleListing"), DevExpress.Xpo.Aggregated]
        public XPCollection<VariantMoreGoogleListing> GoogleListing
        {
            get
            {
                return GetCollection<VariantMoreGoogleListing>(nameof(GoogleListing));
            }
        }
    }
}