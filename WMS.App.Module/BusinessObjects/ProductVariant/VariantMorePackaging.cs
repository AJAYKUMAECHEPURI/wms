﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;
using static WMS.App.Module.BusinessObjects.System.Helpers.SystemConfig;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMorePackaging : BaseObject
    {
        public VariantMorePackaging(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private UnitOfMeasure unitOfMeasure;
        private DateTime endDate;
        private DateTime startDate;
        private string packagingUnit;
        private int amount;
        private PackagingType packagingType;

        public PackagingType PackagingType
        {
            get => packagingType;
            set => SetPropertyValue(nameof(PackagingType), ref packagingType, value);
        }

        public int Amount
        {
            get => amount;
            set => SetPropertyValue(nameof(Amount), ref amount, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PackagingUnit
        {
            get => packagingUnit;
            set => SetPropertyValue(nameof(PackagingUnit), ref packagingUnit, value);
        }

        public DateTime StartDate
        {
            get => startDate;
            set => SetPropertyValue(nameof(StartDate), ref startDate, value);
        }

        public DateTime EndDate
        {
            get => endDate;
            set => SetPropertyValue(nameof(EndDate), ref endDate, value);
        }

        public UnitOfMeasure UnitOfMeasure
        {
            get => unitOfMeasure;
            set => SetPropertyValue(nameof(UnitOfMeasure), ref unitOfMeasure, value);
        }
    }
}