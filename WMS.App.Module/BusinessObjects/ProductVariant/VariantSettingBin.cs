﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Variant;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantSettingBin : BaseObject
    {
        public VariantSettingBin(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool restrictToSpecificBinCategory;
        private VariantBinCategory binCategory;

        public bool RestrictToSpecificBinCategory
        {
            get => restrictToSpecificBinCategory;
            set => SetPropertyValue(nameof(RestrictToSpecificBinCategory), ref restrictToSpecificBinCategory, value);
        }

        public VariantBinCategory BinCategory
        {
            get => binCategory;
            set => SetPropertyValue(nameof(BinCategory), ref binCategory, value);
        }
    }
}