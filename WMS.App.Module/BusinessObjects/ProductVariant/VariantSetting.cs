﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.VariantConfig;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantSetting : CustomBaseObject
    {
        public VariantSetting(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.Sale = new VariantSettingSale(Session);
            this.Despatch = new VariantSettingDespatch(Session);
            this.Manufacturing = new VariantSettingManufacturing(Session);
            this.Transformation = new VariantSettingTransformation(Session);
            this.Return = new VariantSettingReturn(Session);
            this.Bin = new VariantSettingBin(Session);
            this.MobileDevice = new VariantSettingMobileDevice(Session);
            this.Robotic = new VariantSettingRobotic(Session);
        }

        private VariantSettingRobotic robotic;
        private VariantSettingMobileDevice mobileDevice;
        private VariantSettingBin bin;
        private VariantSettingReturn @return;
        private VariantSettingTransformation transformation;
        private VariantSettingManufacturing manufacturing;
        private VariantSettingDespatch despatch;
        private VariantSettingSale sale;
        private string parcelFillFctor;
        private int parcelQuantiry;
        private bool treatAsParcel;
        private bool combineBatchTrans;
        private bool combineSerialNoTrans;
        private DespatchGoodsLabelOption despatchGoodsLabelOption;
        private string printSinglePOLabel;
        private string printSingleLabelOnBooking;
        private string printGoodsLabelOnBooking;
        private int maxQuantityPerReplenishment;
        private int replenishmentMultiplier;
        private string matrixOption;
        private string matrixColumn;
        private bool matrix;
        private bool fMDVerify;
        private bool satisfyDemandFromSingleTransaction;
        private bool autoAllocationToOnOrderTransaction;
        private bool allowDespatchOfOnOrderTransaction;
        private bool requireWidthWhenAddingTransaction;
        private bool requireLenghtWhenAddingTransaction;
        private bool giftCardVariant;
        private bool backToBack;
        private bool allowZeroSalesPrice;
        private bool templateVariant;
        private bool doNotTreatasPUQ;
        private bool disCountinued;
        private bool directSupply;
        private bool reorderAble;
        private bool nonStock;

        public bool NonStock
        {
            get => nonStock;
            set => SetPropertyValue(nameof(NonStock), ref nonStock, value);
        }

        public bool ReorderAble
        {
            get => reorderAble;
            set => SetPropertyValue(nameof(ReorderAble), ref reorderAble, value);
        }

        public bool DirectSupply
        {
            get => directSupply;
            set => SetPropertyValue(nameof(DirectSupply), ref directSupply, value);
        }

        public bool DisContinued
        {
            get => disCountinued;
            set => SetPropertyValue(nameof(DisContinued), ref disCountinued, value);
        }

        public bool DoNotTreatsPUQ
        {
            get => doNotTreatasPUQ;
            set => SetPropertyValue(nameof(DoNotTreatsPUQ), ref doNotTreatasPUQ, value);
        }

        public bool TemplateVariant
        {
            get => templateVariant;
            set => SetPropertyValue(nameof(TemplateVariant), ref templateVariant, value);
        }

        public bool AllowZeroSalesPrice
        {
            get => allowZeroSalesPrice;
            set => SetPropertyValue(nameof(AllowZeroSalesPrice), ref allowZeroSalesPrice, value);
        }

        public bool BackToBack
        {
            get => backToBack;
            set => SetPropertyValue(nameof(BackToBack), ref backToBack, value);
        }

        public bool GiftCardVariant
        {
            get => giftCardVariant;
            set => SetPropertyValue(nameof(GiftCardVariant), ref giftCardVariant, value);
        }

        public bool RequireLengthWhenAddingTransaction
        {
            get => requireLenghtWhenAddingTransaction;
            set => SetPropertyValue(nameof(RequireLengthWhenAddingTransaction), ref requireLenghtWhenAddingTransaction, value);
        }

        public bool RequireWidthWhenAddingTransaction
        {
            get => requireWidthWhenAddingTransaction;
            set => SetPropertyValue(nameof(RequireWidthWhenAddingTransaction), ref requireWidthWhenAddingTransaction, value);
        }

        public bool AllowDespatchOfOnOrderTransaction
        {
            get => allowDespatchOfOnOrderTransaction;
            set => SetPropertyValue(nameof(AllowDespatchOfOnOrderTransaction), ref allowDespatchOfOnOrderTransaction, value);
        }

        public bool AutoAllocationToOnOrderTransaction
        {
            get => autoAllocationToOnOrderTransaction;
            set => SetPropertyValue(nameof(AutoAllocationToOnOrderTransaction), ref autoAllocationToOnOrderTransaction, value);
        }

        public bool SatisfyDemandFromSingleTransaction
        {
            get => satisfyDemandFromSingleTransaction;
            set => SetPropertyValue(nameof(SatisfyDemandFromSingleTransaction), ref satisfyDemandFromSingleTransaction, value);
        }

        public bool FMDVerify
        {
            get => fMDVerify;
            set => SetPropertyValue(nameof(FMDVerify), ref fMDVerify, value);
        }

        public bool Matrix
        {
            get => matrix;
            set => SetPropertyValue(nameof(Matrix), ref matrix, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MatrixColumn
        {
            get => matrixColumn;
            set => SetPropertyValue(nameof(MatrixColumn), ref matrixColumn, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MatrixOption
        {
            get => matrixOption;
            set => SetPropertyValue(nameof(MatrixOption), ref matrixOption, value);
        }

        public int ReplenishmentMultiplier
        {
            get => replenishmentMultiplier;
            set => SetPropertyValue(nameof(ReplenishmentMultiplier), ref replenishmentMultiplier, value);
        }

        public int MaxQuantityPerReplenishment
        {
            get => maxQuantityPerReplenishment;
            set => SetPropertyValue(nameof(MaxQuantityPerReplenishment), ref maxQuantityPerReplenishment, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PrintGoodsLabelOnBooking
        {
            get => printGoodsLabelOnBooking;
            set => SetPropertyValue(nameof(PrintGoodsLabelOnBooking), ref printGoodsLabelOnBooking, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PrintSingleLabelOnBooking
        {
            get => printSingleLabelOnBooking;
            set => SetPropertyValue(nameof(PrintSingleLabelOnBooking), ref printSingleLabelOnBooking, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PrintSinglePOLabel
        {
            get => printSinglePOLabel;
            set => SetPropertyValue(nameof(PrintSinglePOLabel), ref printSinglePOLabel, value);
        }

        public DespatchGoodsLabelOption DespatchGoodsLabelOption
        {
            get => despatchGoodsLabelOption;
            set => SetPropertyValue(nameof(DespatchGoodsLabelOption), ref despatchGoodsLabelOption, value);
        }

        public bool CombineSerialNoTans
        {
            get => combineSerialNoTrans;
            set => SetPropertyValue(nameof(CombineSerialNoTans), ref combineSerialNoTrans, value);
        }

        public bool CombineBatchTans
        {
            get => combineBatchTrans;
            set => SetPropertyValue(nameof(CombineBatchTans), ref combineBatchTrans, value);
        }

        public bool TreatAsParcel
        {
            get => treatAsParcel;
            set => SetPropertyValue(nameof(TreatAsParcel), ref treatAsParcel, value);
        }

        public int ParcelQuantity
        {
            get => parcelQuantiry;
            set => SetPropertyValue(nameof(ParcelQuantity), ref parcelQuantiry, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ParcelFillFactor
        {
            get => parcelFillFctor;
            set => SetPropertyValue(nameof(ParcelFillFactor), ref parcelFillFctor, value);
        }

        [Aggregated]
        [VisibleInListView(false)]
        public VariantSettingSale Sale
        {
            get => sale;
            set => SetPropertyValue(nameof(Sale), ref sale, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantSettingDespatch Despatch
        {
            get => despatch;
            set => SetPropertyValue(nameof(Despatch), ref despatch, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantSettingManufacturing Manufacturing
        {
            get => manufacturing;
            set => SetPropertyValue(nameof(Manufacturing), ref manufacturing, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantSettingTransformation Transformation
        {
            get => transformation;
            set => SetPropertyValue(nameof(Transformation), ref transformation, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantSettingReturn Return
        {
            get => @return;
            set => SetPropertyValue(nameof(Return), ref @return, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantSettingBin Bin
        {
            get => bin;
            set => SetPropertyValue(nameof(Bin), ref bin, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantSettingMobileDevice MobileDevice
        {
            get => mobileDevice;
            set => SetPropertyValue(nameof(MobileDevice), ref mobileDevice, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantSettingRobotic Robotic
        {
            get => robotic;
            set => SetPropertyValue(nameof(Robotic), ref robotic, value);
        }

        [Association("VariantSettings-Supplier"), Aggregated, VisibleInListView(false)]
        public XPCollection<VariantPurchaseSupplier> VariantPurchaseSupplier
        {
            get
            {
                return GetCollection<VariantPurchaseSupplier>(nameof(VariantPurchaseSupplier));
            }
        }
    }
}