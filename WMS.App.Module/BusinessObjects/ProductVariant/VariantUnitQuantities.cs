﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Variant;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantUnitQuantities : BaseObject
    {
        public VariantUnitQuantities(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private Variant variant;
        private bool @default;
        private bool purchase;
        private bool sale;
        private int quantity;
        private UnitQuantity unitQuantity;
        private bool active;

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        public UnitQuantity UnitQuantity
        {
            get => unitQuantity;
            set => SetPropertyValue(nameof(UnitQuantity), ref unitQuantity, value);
        }

        public int Quantity
        {
            get => quantity;
            set => SetPropertyValue(nameof(Quantity), ref quantity, value);
        }

        public bool Sale
        {
            get => sale;
            set => SetPropertyValue(nameof(Sale), ref sale, value);
        }

        public bool Purchase
        {
            get => purchase;
            set => SetPropertyValue(nameof(Purchase), ref purchase, value);
        }

        public bool Default
        {
            get => @default;
            set => SetPropertyValue(nameof(Default), ref @default, value);
        }

        [Association("Variant-UnitQuantity")]
        public Variant Variant
        {
            get => variant;
            set => SetPropertyValue(nameof(Variant), ref variant, value);
        }
    }
}