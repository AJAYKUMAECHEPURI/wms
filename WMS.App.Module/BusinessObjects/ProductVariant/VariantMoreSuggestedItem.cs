﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMoreSuggestedItem : BaseObject
    {
        public VariantMoreSuggestedItem(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private VariantMore more;
        private SuggestItemCategory category;
        private int quantity;
        private string description;
        private Variant variant;

        public Variant Variant
        {
            get => variant;
            set => SetPropertyValue(nameof(Variant), ref variant, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public int Quantity
        {
            get => quantity;
            set => SetPropertyValue(nameof(Quantity), ref quantity, value);
        }

        public SuggestItemCategory Category
        {
            get => category;
            set => SetPropertyValue(nameof(Category), ref category, value);
        }

        [Association("More-SuggestedItems")]
        public VariantMore More
        {
            get => more;
            set => SetPropertyValue(nameof(More), ref more, value);
        }
    }
}