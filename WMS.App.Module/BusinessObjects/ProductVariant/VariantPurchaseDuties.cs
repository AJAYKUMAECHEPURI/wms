﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.VariantConfig;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantPurchaseDuties : CustomBaseObject
    {
        public VariantPurchaseDuties(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private Variant variant;
        private DateTime endDate;
        private DateTime startDate;
        private string valueType;
        private int value;
        private bool percentageOfAValue;
        private AppliesTo appliesTo;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public AppliesTo AppliesTo
        {
            get => appliesTo;
            set => SetPropertyValue(nameof(AppliesTo), ref appliesTo, value);
        }

        public bool PercentageOfAValue
        {
            get => percentageOfAValue;
            set => SetPropertyValue(nameof(PercentageOfAValue), ref percentageOfAValue, value);
        }

        public int Value
        {
            get => value;
            set => SetPropertyValue(nameof(Value), ref value, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ValueType
        {
            get => valueType;
            set => SetPropertyValue(nameof(ValueType), ref valueType, value);
        }

        public DateTime StartDate
        {
            get => startDate;
            set => SetPropertyValue(nameof(StartDate), ref startDate, value);
        }

        public DateTime EndDate
        {
            get => endDate;
            set => SetPropertyValue(nameof(EndDate), ref endDate, value);
        }

        [Association("Variant-Duties")]
        public Variant Variant
        {
            get => variant;
            set => SetPropertyValue(nameof(Variant), ref variant, value);
        }
    }
}