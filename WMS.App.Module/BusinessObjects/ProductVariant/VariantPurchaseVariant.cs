﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantPurchaseVariant : BaseObject
    {
        public VariantPurchaseVariant(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private Variant variant;
        private string code;
        private bool @default;

        public bool Default
        {
            get => @default;
            set => SetPropertyValue(nameof(Default), ref @default, value);
        }

        public string Code
        {
            get => code;
            set => SetPropertyValue(nameof(Code), ref code, value);
        }

        [Association("Variant-VariantPurchase")]
        public Variant Variant
        {
            get => variant;
            set => SetPropertyValue(nameof(Variant), ref variant, value);
        }
    }
}