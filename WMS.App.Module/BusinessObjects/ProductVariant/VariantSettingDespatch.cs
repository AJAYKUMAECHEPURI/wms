﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantSettingDespatch : CustomBaseObject
    {
        public VariantSettingDespatch(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string dangerousGoodsVolume;
        private string dangerousGoodsDescription;
        private string dangerousGoodsUNCode;
        private bool dangerousOrHazardousGoods;

        public bool DangerousOrHazardousGoods
        {
            get => dangerousOrHazardousGoods;
            set => SetPropertyValue(nameof(DangerousOrHazardousGoods), ref dangerousOrHazardousGoods, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string DangerousGoodsUNCode
        {
            get => dangerousGoodsUNCode;
            set => SetPropertyValue(nameof(DangerousGoodsUNCode), ref dangerousGoodsUNCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string DangerousGoodsDescription
        {
            get => dangerousGoodsDescription;
            set => SetPropertyValue(nameof(DangerousGoodsDescription), ref dangerousGoodsDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string DangerousGoodsVolume
        {
            get => dangerousGoodsVolume;
            set => SetPropertyValue(nameof(DangerousGoodsVolume), ref dangerousGoodsVolume, value);
        }
    }
}