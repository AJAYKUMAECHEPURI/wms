﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.Product;
using WMS.App.Module.BusinessObjects.Stock;
using WMS.App.Module.BusinessObjects.System.Variant;
using static WMS.App.Module.BusinessObjects.System.Helpers.VariantConfig;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class Variant : CustomBaseObject
    {
        public Variant(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.Setting = new VariantSetting(Session);
            this.Purchase = new VariantPurchase(Session);
            this.Report = new VariantPurchaseReport(Session);
            this.More = new VariantMore(Session);
        }

        private BusinessObjects.Product.Product product;
        private VariantMore more;
        private VariantPurchaseReport report;
        private VariantPurchase purchase;
        private VariantSetting setting;
        private CN22Type cN22Type;
        private string harmonisationCode;
        private string countryCodeOfOrigin;
        private string exportCommodityCode;
        private string importCommodityCode;
        private string commodityDescription;
        private DateTime dateOfLastStockTake;
        private string mainContact;
        private int depth;
        private string uOM;
        private int width;
        private int volume;
        private int length;
        private int weight;
        private bool stackable;
        private int caseDepth;
        private int caseWidth;
        private int caseQuantity;
        private int caseLength;
        private int caseWeight;
        private bool purchaseVariants;
        private bool salesVariants;
        private bool stockInWholeUnits;
        private string numberOfBaseUnit;
        private string manufacturerPartNumber;
        private VariantManufacturer manufacturer;
        private VariantDiscountGroup discountGroup;
        private VariantCategories categorie;
        private string abbreviatedDescription;
        private string description;
        private string eANCode;
        private ProductDefault defaultSetting;
        private string code;
        private string name;

        [Association("Product-Variants")]
        public Product.Product Product
        {
            get => product;
            set
            {
                SetPropertyValue(nameof(Product), value);
                if (value != null && Session.IsNewObject(this))
                {
                    //Check the formate for the variant creation.
                    string guid = Guid.NewGuid().ToString();
                    this.Code = guid.Split('-').First();
                }
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Code
        {
            get => code;
            set => SetPropertyValue(nameof(Code), ref code, value);
        }

        public ProductDefault DefaultSetting
        {
            get => defaultSetting;
            set => SetPropertyValue(nameof(DefaultSetting), ref defaultSetting, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string EANCode
        {
            get => eANCode;
            set => SetPropertyValue(nameof(EANCode), ref eANCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AbbreviatedDescription
        {
            get => abbreviatedDescription;
            set => SetPropertyValue(nameof(AbbreviatedDescription), ref abbreviatedDescription, value);
        }

        [VisibleInListView(false)]
        public VariantCategories Categories
        {
            get => categorie;
            set => SetPropertyValue(nameof(Categories), ref categorie, value);
        }

        [VisibleInListView(false)]
        public VariantDiscountGroup DiscountGroup
        {
            get => discountGroup;
            set => SetPropertyValue(nameof(DiscountGroup), ref discountGroup, value);
        }

        [VisibleInListView(false)]
        public VariantManufacturer Manufacturer
        {
            get => manufacturer;
            set => SetPropertyValue(nameof(Manufacturer), ref manufacturer, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ManufacturerPartNumber
        {
            get => manufacturerPartNumber;
            set => SetPropertyValue(nameof(ManufacturerPartNumber), ref manufacturerPartNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize), VisibleInListView(false)]
        public string NumberOfBaseUnit
        {
            get => numberOfBaseUnit;
            set => SetPropertyValue(nameof(NumberOfBaseUnit), ref numberOfBaseUnit, value);
        }

        [VisibleInListView(false)]
        public bool StockInWholeUnits
        {
            get => stockInWholeUnits;
            set => SetPropertyValue(nameof(StockInWholeUnits), ref stockInWholeUnits, value);
        }

        [VisibleInListView(false)]
        public bool SalesVariants
        {
            get => salesVariants;
            set => SetPropertyValue(nameof(SalesVariants), ref salesVariants, value);
        }

        [VisibleInListView(false)]
        public bool PurchaseVariants
        {
            get => purchaseVariants;
            set => SetPropertyValue(nameof(PurchaseVariants), ref purchaseVariants, value);
        }

        [VisibleInListView(false)]
        public int CaseWeight
        {
            get => caseWeight;
            set => SetPropertyValue(nameof(CaseWeight), ref caseWeight, value);
        }

        [VisibleInListView(false)]
        public int CaseLength
        {
            get => caseLength;
            set => SetPropertyValue(nameof(CaseLength), ref caseLength, value);
        }

        [VisibleInListView(false)]
        public int CaseQuantity
        {
            get => caseQuantity;
            set => SetPropertyValue(nameof(CaseQuantity), ref caseQuantity, value);
        }

        [VisibleInListView(false)]
        public int CaseWidth
        {
            get => caseWidth;
            set => SetPropertyValue(nameof(CaseWidth), ref caseWidth, value);
        }

        [VisibleInListView(false)]
        public int CaseDepth
        {
            get => caseDepth;
            set => SetPropertyValue(nameof(CaseDepth), ref caseDepth, value);
        }

        public bool Stackable
        {
            get => stackable;
            set => SetPropertyValue(nameof(Stackable), ref stackable, value);
        }

        [VisibleInListView(false)]
        public int Weight
        {
            get => weight;
            set => SetPropertyValue(nameof(Weight), ref weight, value);
        }

        [VisibleInListView(false)]
        public int Length
        {
            get => length;
            set => SetPropertyValue(nameof(Length), ref length, value);
        }

        [VisibleInListView(false)]
        public int Volume
        {
            get => volume;
            set => SetPropertyValue(nameof(Volume), ref volume, value);
        }

        [VisibleInListView(false)]
        public int Width
        {
            get => width;
            set => SetPropertyValue(nameof(Width), ref width, value);
        }

        [VisibleInListView(false)]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string UOM
        {
            get => uOM;
            set => SetPropertyValue(nameof(UOM), ref uOM, value);
        }

        [VisibleInListView(false)]
        public int Depth
        {
            get => depth;
            set => SetPropertyValue(nameof(Depth), ref depth, value);
        }

        [VisibleInListView(false)]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MainContact
        {
            get => mainContact;
            set => SetPropertyValue(nameof(MainContact), ref mainContact, value);
        }

        [VisibleInListView(false)]
        public DateTime DateOfLastStockTake
        {
            get => dateOfLastStockTake;
            set => SetPropertyValue(nameof(DateOfLastStockTake), ref dateOfLastStockTake, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize), VisibleInListView(false)]
        public string CommodityDescription
        {
            get => commodityDescription;
            set => SetPropertyValue(nameof(CommodityDescription), ref commodityDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize), VisibleInListView(false)]
        public string ImportCommodityCode
        {
            get => importCommodityCode;
            set => SetPropertyValue(nameof(ImportCommodityCode), ref importCommodityCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize), VisibleInListView(false)]
        public string ExportCommodityCode
        {
            get => exportCommodityCode;
            set => SetPropertyValue(nameof(ExportCommodityCode), ref exportCommodityCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize), VisibleInListView(false)]
        public string CountryCodeOfOrigin
        {
            get => countryCodeOfOrigin;
            set => SetPropertyValue(nameof(CountryCodeOfOrigin), ref countryCodeOfOrigin, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize), VisibleInListView(false)]
        public string HarmonizationCode
        {
            get => harmonisationCode;
            set => SetPropertyValue(nameof(HarmonizationCode), ref harmonisationCode, value);
        }

        [VisibleInListView(false)]
        public CN22Type CN22Type
        {
            get => cN22Type;
            set => SetPropertyValue(nameof(CN22Type), ref cN22Type, value);
        }

        [Aggregated, VisibleInListView(false)]
        public VariantSetting Setting
        {
            get => setting;
            set => SetPropertyValue(nameof(Setting), ref setting, value);
        }

        [Association("Variant-Sales"), Aggregated, VisibleInListView(false)]
        public XPCollection<VariantSale> Sales
        {
            get
            {
                return GetCollection<VariantSale>(nameof(Sales));
            }
        }

        [Aggregated, VisibleInListView(false)]
        public VariantPurchase Purchase
        {
            get => purchase;
            set => SetPropertyValue(nameof(Purchase), ref purchase, value);
        }

        [Association("Variant-VariantPurchase"), Aggregated, VisibleInListView(false)]
        public XPCollection<VariantPurchaseVariant> VariantPurchase
        {
            get
            {
                return GetCollection<VariantPurchaseVariant>(nameof(VariantPurchase));
            }
        }

        [Association("Variant-Duties"), Aggregated, VisibleInListView(false)]
        public XPCollection<VariantPurchaseDuties> Duties
        {
            get
            {
                return GetCollection<VariantPurchaseDuties>(nameof(Duties));
            }
        }

        [Aggregated, VisibleInListView(false)]
        public VariantPurchaseReport Report
        {
            get => report;
            set => SetPropertyValue(nameof(Report), ref report, value);
        }

        [Association("Variant-UnitQuantity"), Aggregated, VisibleInListView(false)]
        public XPCollection<VariantUnitQuantities> UnitQuantity
        {
            get { return GetCollection<VariantUnitQuantities>(nameof(UnitQuantity)); }
        }

        [Association("Variant-VariantSpecialOffer"), Aggregated, VisibleInListView(false)]
        public XPCollection<VariantSpecialOffer> SpecialOffer
        {
            get
            {
                return GetCollection<VariantSpecialOffer>(nameof(SpecialOffer));
            }
        }

        [Association("Variant-QuantityBreak"), Aggregated, VisibleInListView(false)]
        public XPCollection<VariantQuantityBreak> QuantityBreak
        {
            get
            {
                return GetCollection<VariantQuantityBreak>(nameof(QuantityBreak));
            }
        }

        [Aggregated, VisibleInListView(false)]
        public VariantMore More
        {
            get => more;
            set => SetPropertyValue(nameof(More), ref more, value);
        }

        [Association("Variant-StockItem")]
        public XPCollection<StockItem> StockItem
        {
            get
            {
                return GetCollection<StockItem>(nameof(StockItem));
            }
        }
    }
}