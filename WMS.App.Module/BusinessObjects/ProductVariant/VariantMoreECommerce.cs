﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMoreECommerce : BaseObject
    {
        public VariantMoreECommerce(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int amazomMaximumStockLevel;
        private int eBayStockLevelPercentage;
        private int eBayMaximumStockLevel;
        private int eBayMinimumStockLevel;
        private int eBayBufferStockLevel;
        private int eBayOverSellStockLevel;
        private int amazonStockLevelPercentage;
        private int amazonMinimumStockLevel;
        private int amazonBufferStockLevel;
        private int amazonOversellStockLevel;

        public int AmazonOversellStockLevel
        {
            get => amazonOversellStockLevel;
            set => SetPropertyValue(nameof(AmazonOversellStockLevel), ref amazonOversellStockLevel, value);
        }

        public int AmazonBufferStockLevel
        {
            get => amazonBufferStockLevel;
            set => SetPropertyValue(nameof(AmazonBufferStockLevel), ref amazonBufferStockLevel, value);
        }

        public int AmazonMinimumStockLevel
        {
            get => amazonMinimumStockLevel;
            set => SetPropertyValue(nameof(AmazonMinimumStockLevel), ref amazonMinimumStockLevel, value);
        }

        public int AmazonMaximumStockLevel
        {
            get => amazomMaximumStockLevel;
            set => SetPropertyValue(nameof(AmazonMaximumStockLevel), ref amazomMaximumStockLevel, value);
        }

        public int AmazonStockLevelPercentage
        {
            get => amazonStockLevelPercentage;
            set => SetPropertyValue(nameof(AmazonStockLevelPercentage), ref amazonStockLevelPercentage, value);
        }

        public int EBayOverSellStockLevel
        {
            get => eBayOverSellStockLevel;
            set => SetPropertyValue(nameof(EBayOverSellStockLevel), ref eBayOverSellStockLevel, value);
        }

        public int EBayBufferStockLevel
        {
            get => eBayBufferStockLevel;
            set => SetPropertyValue(nameof(EBayBufferStockLevel), ref eBayBufferStockLevel, value);
        }

        public int EBayMinimumStockLevel
        {
            get => eBayMinimumStockLevel;
            set => SetPropertyValue(nameof(EBayMinimumStockLevel), ref eBayMinimumStockLevel, value);
        }

        public int EBayMaximumStockLevel
        {
            get => eBayMaximumStockLevel;
            set => SetPropertyValue(nameof(EBayMaximumStockLevel), ref eBayMaximumStockLevel, value);
        }

        public int EBayStockLevelPercentage
        {
            get => eBayStockLevelPercentage;
            set => SetPropertyValue(nameof(EBayStockLevelPercentage), ref eBayStockLevelPercentage, value);
        }
    }
}