﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Variant;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantPurchaseSupplier : CustomBaseObject
    {
        public VariantPurchaseSupplier(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private VariantSetting setting;
        private string comment;
        private string purchasingRSP;
        private int multiplierQuantity;
        private int minOrderQuantity;
        private DateTime dateLastOrdered;
        private bool mainSupplier;
        private DateTime dateLastSupplied;
        private string additionalCost;
        private string retrospectiveDiscount;
        private string defaultStockInBuffer;
        private string guideLeadTime;
        private string supplierStockQty;
        private string additionalCostBase;
        private string standardCost;
        private VariantRebateGroup rebateGroup;
        private string supplierPartNumber;
        private string supplierDescription;
        private string supplierName;
        private bool active;

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SupplierName
        {
            get => supplierName;
            set => SetPropertyValue(nameof(SupplierName), ref supplierName, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SupplierDescription
        {
            get => supplierDescription;
            set => SetPropertyValue(nameof(SupplierDescription), ref supplierDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SupplierPartNumber
        {
            get => supplierPartNumber;
            set => SetPropertyValue(nameof(SupplierPartNumber), ref supplierPartNumber, value);
        }

        public VariantRebateGroup RebateGroup
        {
            get => rebateGroup;
            set => SetPropertyValue(nameof(RebateGroup), ref rebateGroup, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string StandardCost
        {
            get => standardCost;
            set => SetPropertyValue(nameof(StandardCost), ref standardCost, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AdditionalCostBase
        {
            get => additionalCostBase;
            set => SetPropertyValue(nameof(AdditionalCostBase), ref additionalCostBase, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SupplierStockQty
        {
            get => supplierStockQty;
            set => SetPropertyValue(nameof(SupplierStockQty), ref supplierStockQty, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string GuideLeadTime
        {
            get => guideLeadTime;
            set => SetPropertyValue(nameof(GuideLeadTime), ref guideLeadTime, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string DefaultStockInBuffer
        {
            get => defaultStockInBuffer;
            set => SetPropertyValue(nameof(DefaultStockInBuffer), ref defaultStockInBuffer, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string RetrospectiveDiscount
        {
            get => retrospectiveDiscount;
            set => SetPropertyValue(nameof(RetrospectiveDiscount), ref retrospectiveDiscount, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AdditionalCost
        {
            get => additionalCost;
            set => SetPropertyValue(nameof(AdditionalCost), ref additionalCost, value);
        }

        public DateTime DateLastSupplied
        {
            get => dateLastSupplied;
            set => SetPropertyValue(nameof(DateLastSupplied), ref dateLastSupplied, value);
        }

        public bool MainSupplier
        {
            get => mainSupplier;
            set => SetPropertyValue(nameof(MainSupplier), ref mainSupplier, value);
        }

        public DateTime DateLastOrdered
        {
            get => dateLastOrdered;
            set => SetPropertyValue(nameof(DateLastOrdered), ref dateLastOrdered, value);
        }

        public int MinOrderQuantity
        {
            get => minOrderQuantity;
            set => SetPropertyValue(nameof(MinOrderQuantity), ref minOrderQuantity, value);
        }

        public int MultiplierQuantity
        {
            get => multiplierQuantity;
            set => SetPropertyValue(nameof(MultiplierQuantity), ref multiplierQuantity, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PurchasingRSP
        {
            get => purchasingRSP;
            set => SetPropertyValue(nameof(PurchasingRSP), ref purchasingRSP, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Comment
        {
            get => comment;
            set => SetPropertyValue(nameof(Comment), ref comment, value);
        }

        [Association("VariantSettings-Supplier")]
        public VariantSetting Setting
        {
            get => setting;
            set => SetPropertyValue(nameof(Setting), ref setting, value);
        }
    }
}