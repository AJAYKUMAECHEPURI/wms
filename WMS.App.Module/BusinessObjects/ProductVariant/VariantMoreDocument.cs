﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.VariantConfig;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMoreDocument : BaseObject
    {
        public VariantMoreDocument(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private FileData file;
        private bool uploaded;
        private AutoPrintOptions autoPrintOptions;
        private string customTag;
        private string note;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Note
        {
            get => note;
            set => SetPropertyValue(nameof(Note), ref note, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CustomTag
        {
            get => customTag;
            set => SetPropertyValue(nameof(CustomTag), ref customTag, value);
        }

        public AutoPrintOptions AutoPrintOptions
        {
            get => autoPrintOptions;
            set => SetPropertyValue(nameof(AutoPrintOptions), ref autoPrintOptions, value);
        }

        public bool Uploaded
        {
            get => uploaded;
            set => SetPropertyValue(nameof(Uploaded), ref uploaded, value);
        }

        public FileData File
        {
            get => file;
            set => SetPropertyValue(nameof(File), ref file, value);
        }
    }
}