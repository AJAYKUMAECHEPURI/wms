﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMoreMessage : BaseObject
    {
        public VariantMoreMessage(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool showMessageOnEPOSOrder;
        private bool showMessageOnGoodsIn;
        private bool showMessageOnDespatch;
        private bool showMessageToPO;
        private bool showMessageToSO;
        private bool showMessageIndividually;
        private DateTime endDate;
        private DateTime startDate;
        private string message;
        private string title;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Title
        {
            get => title;
            set => SetPropertyValue(nameof(Title), ref title, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Message
        {
            get => message;
            set => SetPropertyValue(nameof(Message), ref message, value);
        }

        public DateTime StartDate
        {
            get => startDate;
            set => SetPropertyValue(nameof(StartDate), ref startDate, value);
        }

        public DateTime EndDate
        {
            get => endDate;
            set => SetPropertyValue(nameof(EndDate), ref endDate, value);
        }

        public bool ShowMessageIndividually
        {
            get => showMessageIndividually;
            set => SetPropertyValue(nameof(ShowMessageIndividually), ref showMessageIndividually, value);
        }

        public bool ShowMessageToSO
        {
            get => showMessageToSO;
            set => SetPropertyValue(nameof(ShowMessageToSO), ref showMessageToSO, value);
        }

        public bool ShowMessageToPO
        {
            get => showMessageToPO;
            set => SetPropertyValue(nameof(ShowMessageToPO), ref showMessageToPO, value);
        }

        public bool ShowMessageOnDespatch
        {
            get => showMessageOnDespatch;
            set => SetPropertyValue(nameof(ShowMessageOnDespatch), ref showMessageOnDespatch, value);
        }

        public bool ShowMessageOnGoodsIn
        {
            get => showMessageOnGoodsIn;
            set => SetPropertyValue(nameof(ShowMessageOnGoodsIn), ref showMessageOnGoodsIn, value);
        }

        public bool ShowMessageOnEPOSOrder
        {
            get => showMessageOnEPOSOrder;
            set => SetPropertyValue(nameof(ShowMessageOnEPOSOrder), ref showMessageOnEPOSOrder, value);
        }
    }
}