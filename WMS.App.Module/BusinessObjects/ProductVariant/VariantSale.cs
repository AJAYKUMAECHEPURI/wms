﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;
using WMS.App.Module.BusinessObjects.System.Customer;
using static WMS.App.Module.BusinessObjects.System.Helpers.VariantConfig;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantSale : CustomBaseObject
    {
        public VariantSale(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private Variant variant;
        private string minSalesMargin;
        private string minProfile;
        private string minNetSalesPrice;
        private string pricePer;
        private string rSPIncTax;
        private string rSPExcTax;
        private TaxCalDirection taxCalDirection;
        private CostCenter costCenter;
        private DepartmentCode departmentCode;
        private NominalCode nominalCode;
        private TaxRate salesTaxRate;
        private bool basePrice;
        private Currency currency;

        public Currency Currency
        {
            get => currency;
            set => SetPropertyValue(nameof(Currency), ref currency, value);
        }

        public bool BasePrice
        {
            get => basePrice;
            set => SetPropertyValue(nameof(BasePrice), ref basePrice, value);
        }

        public TaxRate SalesTaxRate
        {
            get => salesTaxRate;
            set => SetPropertyValue(nameof(SalesTaxRate), ref salesTaxRate, value);
        }

        public NominalCode NominalCode
        {
            get => nominalCode;
            set => SetPropertyValue(nameof(NominalCode), ref nominalCode, value);
        }

        public DepartmentCode DepartmentCode
        {
            get => departmentCode;
            set => SetPropertyValue(nameof(DepartmentCode), ref departmentCode, value);
        }

        public CostCenter CostCenter
        {
            get => costCenter;
            set => SetPropertyValue(nameof(CostCenter), ref costCenter, value);
        }

        public TaxCalDirection TaxCalDirection
        {
            get => taxCalDirection;
            set => SetPropertyValue(nameof(TaxCalDirection), ref taxCalDirection, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string RSPExcTax
        {
            get => rSPExcTax;
            set => SetPropertyValue(nameof(RSPExcTax), ref rSPExcTax, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string RSPIncTax
        {
            get => rSPIncTax;
            set => SetPropertyValue(nameof(RSPIncTax), ref rSPIncTax, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PricePer
        {
            get => pricePer;
            set => SetPropertyValue(nameof(PricePer), ref pricePer, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MinNetSalesPrice
        {
            get => minNetSalesPrice;
            set => SetPropertyValue(nameof(MinNetSalesPrice), ref minNetSalesPrice, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MinProfile
        {
            get => minProfile;
            set => SetPropertyValue(nameof(MinProfile), ref minProfile, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MinSalesMargin
        {
            get => minSalesMargin;
            set => SetPropertyValue(nameof(MinSalesMargin), ref minSalesMargin, value);
        }

        [Association("Variant-Sales")]
        public Variant Variant
        {
            get => variant;
            set => SetPropertyValue(nameof(Variant), ref variant, value);
        }
    }
}