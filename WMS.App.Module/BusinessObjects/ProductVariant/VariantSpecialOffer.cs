﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantSpecialOffer : CustomBaseObject
    {
        public VariantSpecialOffer(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private Variant variant;
        private bool newNormalRSP;
        private int price;
        private string discount;
        private string newRSP;
        private DateTime endDate;
        private DateTime startDate;
        private string description;
        private string specialOfferCode;
        private bool active;

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SpecialOfferCode
        {
            get => specialOfferCode;
            set => SetPropertyValue(nameof(SpecialOfferCode), ref specialOfferCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public DateTime StartDate
        {
            get => startDate;
            set => SetPropertyValue(nameof(StartDate), ref startDate, value);
        }

        public DateTime EndDate
        {
            get => endDate;
            set => SetPropertyValue(nameof(EndDate), ref endDate, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string NewRSP
        {
            get => newRSP;
            set => SetPropertyValue(nameof(NewRSP), ref newRSP, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Discount
        {
            get => discount;
            set => SetPropertyValue(nameof(Discount), ref discount, value);
        }

        public int Price
        {
            get => price;
            set => SetPropertyValue(nameof(Price), ref price, value);
        }

        public bool NewNormalRSP
        {
            get => newNormalRSP;
            set => SetPropertyValue(nameof(NewNormalRSP), ref newNormalRSP, value);
        }

        [Association("Variant-VariantSpecialOffer")]
        public Variant Variant
        {
            get => variant;
            set => SetPropertyValue(nameof(Variant), ref variant, value);
        }
    }
}