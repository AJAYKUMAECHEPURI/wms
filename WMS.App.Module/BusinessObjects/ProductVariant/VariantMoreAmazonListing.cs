﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System;
using static WMS.App.Module.BusinessObjects.System.Helpers.ProductConfig;
using static WMS.App.Module.BusinessObjects.System.Helpers.VariantConfig;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMoreAmazonListing : BaseObject
    {
        public VariantMoreAmazonListing(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private VariantMoreListing listing;
        private string swatchURL;
        private bool variationListing;
        private int maxQuantityPerPercel;
        private int maxOrderQuantity;
        private int numberOfItems;
        private int packQuantity;
        private string description;
        private int mSRP;
        private string merchantPartNumber;
        private string merchantCatalogNumber;
        private string manufacturer;
        private AmazonTaxCode amazonTaxCode;
        private string designer;
        private string brand;
        private string conditionDescription;
        private ConditionType conditionType;
        private DateTime releaseDate;
        private string alternateSellerSKU;
        private string productId;
        private ProductType productType;
        private string title;
        private string aSINTemplate;
        private string aSIN;
        private string shippingTemplate;
        private string marketPlace;
        private bool userCreated;
        private bool listed;
        private bool list;

        public bool List
        {
            get => list;
            set => SetPropertyValue(nameof(List), ref list, value);
        }

        public bool Listed
        {
            get => listed;
            set => SetPropertyValue(nameof(Listed), ref listed, value);
        }

        public bool UserCreated
        {
            get => userCreated;
            set => SetPropertyValue(nameof(UserCreated), ref userCreated, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MarketPlace
        {
            get => marketPlace;
            set => SetPropertyValue(nameof(MarketPlace), ref marketPlace, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ShippingTemplate
        {
            get => shippingTemplate;
            set => SetPropertyValue(nameof(ShippingTemplate), ref shippingTemplate, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ASIN
        {
            get => aSIN;
            set => SetPropertyValue(nameof(ASIN), ref aSIN, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ASINTemplate
        {
            get => aSINTemplate;
            set => SetPropertyValue(nameof(ASINTemplate), ref aSINTemplate, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Title
        {
            get => title;
            set => SetPropertyValue(nameof(Title), ref title, value);
        }

        public ProductType ProductType
        {
            get => productType;
            set => SetPropertyValue(nameof(ProductType), ref productType, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ProductId
        {
            get => productId;
            set => SetPropertyValue(nameof(ProductId), ref productId, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AlternateSellerSKU
        {
            get => alternateSellerSKU;
            set => SetPropertyValue(nameof(AlternateSellerSKU), ref alternateSellerSKU, value);
        }

        public DateTime ReleaseDate
        {
            get => releaseDate;
            set => SetPropertyValue(nameof(ReleaseDate), ref releaseDate, value);
        }

        public ConditionType ConditionType
        {
            get => conditionType;
            set => SetPropertyValue(nameof(ConditionType), ref conditionType, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ConditionDescription
        {
            get => conditionDescription;
            set => SetPropertyValue(nameof(ConditionDescription), ref conditionDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Brand
        {
            get => brand;
            set => SetPropertyValue(nameof(Brand), ref brand, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Designer
        {
            get => designer;
            set => SetPropertyValue(nameof(Designer), ref designer, value);
        }

        public AmazonTaxCode AmazonTaxCode
        {
            get => amazonTaxCode;
            set => SetPropertyValue(nameof(AmazonTaxCode), ref amazonTaxCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Manufacturer
        {
            get => manufacturer;
            set => SetPropertyValue(nameof(Manufacturer), ref manufacturer, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MerchantCatalogNumber
        {
            get => merchantCatalogNumber;
            set => SetPropertyValue(nameof(MerchantCatalogNumber), ref merchantCatalogNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string MerchantPartNumber
        {
            get => merchantPartNumber;
            set => SetPropertyValue(nameof(MerchantPartNumber), ref merchantPartNumber, value);
        }

        public int MSRP
        {
            get => mSRP;
            set => SetPropertyValue(nameof(MSRP), ref mSRP, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public int PackQuantity
        {
            get => packQuantity;
            set => SetPropertyValue(nameof(PackQuantity), ref packQuantity, value);
        }

        public int NumberOfItems
        {
            get => numberOfItems;
            set => SetPropertyValue(nameof(NumberOfItems), ref numberOfItems, value);
        }

        public int MaxOrderQuantity
        {
            get => maxOrderQuantity;
            set => SetPropertyValue(nameof(MaxOrderQuantity), ref maxOrderQuantity, value);
        }

        public int MaxQuantityPerParcel
        {
            get => maxQuantityPerPercel;
            set => SetPropertyValue(nameof(MaxQuantityPerParcel), ref maxQuantityPerPercel, value);
        }

        public bool VariationListing
        {
            get => variationListing;
            set => SetPropertyValue(nameof(VariationListing), ref variationListing, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SwatchURL
        {
            get => swatchURL;
            set => SetPropertyValue(nameof(SwatchURL), ref swatchURL, value);
        }

        [Association("Listing-AmazonListing")]
        public VariantMoreListing Listing
        {
            get => listing;
            set => SetPropertyValue(nameof(Listing), ref listing, value);
        }
    }
}