﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    public class VariantMoreEBayListing : BaseObject
    {
        public VariantMoreEBayListing(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private VariantMoreListing listing;
        private string hTMLTemplate;
        private bool useHTMLTemplate;
        private string description;
        private string subTitle;
        private string title;
        private string alternateCode;
        private string itemId;
        private string account;
        private bool active;

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Account
        {
            get => account;
            set => SetPropertyValue(nameof(Account), ref account, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ItemId
        {
            get => itemId;
            set => SetPropertyValue(nameof(ItemId), ref itemId, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AlternateCode
        {
            get => alternateCode;
            set => SetPropertyValue(nameof(AlternateCode), ref alternateCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Title
        {
            get => title;
            set => SetPropertyValue(nameof(Title), ref title, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SubTitle
        {
            get => subTitle;
            set => SetPropertyValue(nameof(SubTitle), ref subTitle, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public bool UseHTMLTemplate
        {
            get => useHTMLTemplate;
            set => SetPropertyValue(nameof(UseHTMLTemplate), ref useHTMLTemplate, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string HTMLTemplate
        {
            get => hTMLTemplate;
            set => SetPropertyValue(nameof(HTMLTemplate), ref hTMLTemplate, value);
        }

        [Association("Listing-EBayListing")]
        public VariantMoreListing Listing
        {
            get => listing;
            set => SetPropertyValue(nameof(Listing), ref listing, value);
        }
    }
}