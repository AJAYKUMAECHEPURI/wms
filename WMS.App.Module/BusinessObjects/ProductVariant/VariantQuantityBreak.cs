﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Variant;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantQuantityBreak : CustomBaseObject
    {
        public VariantQuantityBreak(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private Variant variant;
        private bool useQuantityBreak;

        public bool UseQuantityBreak
        {
            get => useQuantityBreak;
            set => SetPropertyValue(nameof(UseQuantityBreak), ref useQuantityBreak, value);
        }

        [Association("VariantQuantityBreak-QuantityBreak"), Aggregated]
        public XPCollection<QuantityBreak> QuantityBreak
        {
            get
            {
                return GetCollection<QuantityBreak>(nameof(QuantityBreak));
            }
        }

        [Association("Variant-QuantityBreak")]
        public Variant Variant
        {
            get => variant;
            set => SetPropertyValue(nameof(Variant), ref variant, value);
        }
    }
}