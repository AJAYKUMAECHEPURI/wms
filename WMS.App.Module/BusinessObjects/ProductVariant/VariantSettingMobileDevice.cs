﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.ProductVariant
{
    [DefaultClassOptions]
    [NavigationItem("Variant")]
    public class VariantSettingMobileDevice : CustomBaseObject
    {
        public VariantSettingMobileDevice(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string additionalInformation;
        private bool doNotAutoPopulateVariantCode;
        private int maxQuantityPerBin;
        private string batchNumberTemplate;
        private string serialNumberTemplate;
        private bool excludeFromTheHHT;
        private bool variantCanBeSoldViaMobileSales;
        private bool printPackScanLabel;
        private bool requireScanPerItem;

        public bool RequireScanPerItem
        {
            get => requireScanPerItem;
            set => SetPropertyValue(nameof(RequireScanPerItem), ref requireScanPerItem, value);
        }

        public bool PrintPackScanLabel
        {
            get => printPackScanLabel;
            set => SetPropertyValue(nameof(PrintPackScanLabel), ref printPackScanLabel, value);
        }

        public bool VariantCanBeSoldViaMobileSales
        {
            get => variantCanBeSoldViaMobileSales;
            set => SetPropertyValue(nameof(VariantCanBeSoldViaMobileSales), ref variantCanBeSoldViaMobileSales, value);
        }

        public bool ExcludeFromTheHHT
        {
            get => excludeFromTheHHT;
            set => SetPropertyValue(nameof(ExcludeFromTheHHT), ref excludeFromTheHHT, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SerialNumberTemplate
        {
            get => serialNumberTemplate;
            set => SetPropertyValue(nameof(SerialNumberTemplate), ref serialNumberTemplate, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string BatchNumberTemplate
        {
            get => batchNumberTemplate;
            set => SetPropertyValue(nameof(BatchNumberTemplate), ref batchNumberTemplate, value);
        }

        public int MaxQuantityPerBin
        {
            get => maxQuantityPerBin;
            set => SetPropertyValue(nameof(MaxQuantityPerBin), ref maxQuantityPerBin, value);
        }

        public bool DoNotAutoPopulateVariantCode
        {
            get => doNotAutoPopulateVariantCode;
            set => SetPropertyValue(nameof(DoNotAutoPopulateVariantCode), ref doNotAutoPopulateVariantCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AdditionalInformation
        {
            get => additionalInformation;
            set => SetPropertyValue(nameof(AdditionalInformation), ref additionalInformation, value);
        }
    }
}