﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    public class StockBin : BaseObject
    {
        public StockBin(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string binNumber;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string BinNumber
        {
            get => binNumber;
            set => SetPropertyValue(nameof(BinNumber), ref binNumber, value);
        }
    }
}