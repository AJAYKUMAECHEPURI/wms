﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System.Supplier
{
    [DefaultClassOptions]
    public class SupplierPaymentGroup : BaseObject
    {
        public SupplierPaymentGroup(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string description;
        private string paymentGroup;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PaymentGroup
        {
            get => paymentGroup;
            set => SetPropertyValue(nameof(PaymentGroup), ref paymentGroup, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }
    }
}