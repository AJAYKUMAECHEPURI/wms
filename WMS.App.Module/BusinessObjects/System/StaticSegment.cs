﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.Customer;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    public class StaticSegment : CustomBaseObject
    {
        public StaticSegment(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private CustomerContactMarketing marketing;
        private string name;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        [Association("Marketing-Segment")]
        public CustomerContactMarketing Marketing
        {
            get => marketing;
            set => SetPropertyValue(nameof(Marketing), ref marketing, value);
        }
    }
}