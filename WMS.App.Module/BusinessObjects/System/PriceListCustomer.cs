﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class PriceListCustomer : CustomBaseObject
    {
        public PriceListCustomer(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string statementTelephone;
        private string statementTown;
        private string statementAddress1;
        private string statementName;
        private string accountNumber;
        private PriceList priceList;

        public PriceList PriceList
        {
            get => priceList;
            set => SetPropertyValue(nameof(PriceList), ref priceList, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AccountNumber
        {
            get => accountNumber;
            set => SetPropertyValue(nameof(AccountNumber), ref accountNumber, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string StatementName
        {
            get => statementName;
            set => SetPropertyValue(nameof(StatementName), ref statementName, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string StatementAddress1
        {
            get => statementAddress1;
            set => SetPropertyValue(nameof(StatementAddress1), ref statementAddress1, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string StatementTown
        {
            get => statementTown;
            set => SetPropertyValue(nameof(StatementTown), ref statementTown, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string StatementTelephone
        {
            get => statementTelephone;
            set => SetPropertyValue(nameof(StatementTelephone), ref statementTelephone, value);
        }
    }
}