﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.SystemConfig;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class PriceList : CustomBaseObject
    {
        public PriceList(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private DefaultMethod defaultMethod;
        private string note;
        private string defaultOperation;
        private string defaultValue;
        private bool showInBatchEditPriceList;
        private bool changeRSPWithRSPModifier;
        private bool autoAddSalesVariants;
        private bool useGrossPrices;
        private bool overrideVariant;
        private NominalCode nominalCode;
        private Currency currency;
        private string reference;
        private string name;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Reference
        {
            get => reference;
            set => SetPropertyValue(nameof(Reference), ref reference, value);
        }

        public Currency Currency
        {
            get => currency;
            set => SetPropertyValue(nameof(Currency), ref currency, value);
        }

        public NominalCode NominalCode
        {
            get => nominalCode;
            set => SetPropertyValue(nameof(NominalCode), ref nominalCode, value);
        }

        public bool OverrideVariant
        {
            get => overrideVariant;
            set => SetPropertyValue(nameof(OverrideVariant), ref overrideVariant, value);
        }

        public bool UseGrossPrices
        {
            get => useGrossPrices;
            set => SetPropertyValue(nameof(UseGrossPrices), ref useGrossPrices, value);
        }

        public bool AutoAddSalesVariants
        {
            get => autoAddSalesVariants;
            set => SetPropertyValue(nameof(AutoAddSalesVariants), ref autoAddSalesVariants, value);
        }

        public bool ChangeRSPWithRSPModifier
        {
            get => changeRSPWithRSPModifier;
            set => SetPropertyValue(nameof(ChangeRSPWithRSPModifier), ref changeRSPWithRSPModifier, value);
        }

        public bool ShowInBatchEditPriceList
        {
            get => showInBatchEditPriceList;
            set => SetPropertyValue(nameof(ShowInBatchEditPriceList), ref showInBatchEditPriceList, value);
        }

        public DefaultMethod DefaultMethod
        {
            get => defaultMethod;
            set => SetPropertyValue(nameof(DefaultMethod), ref defaultMethod, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string DefaultValue
        {
            get => defaultValue;
            set => SetPropertyValue(nameof(DefaultValue), ref defaultValue, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string DefaultOperation
        {
            get => defaultOperation;
            set => SetPropertyValue(nameof(DefaultOperation), ref defaultOperation, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Note
        {
            get => note;
            set => SetPropertyValue(nameof(Note), ref note, value);
        }
    }
}