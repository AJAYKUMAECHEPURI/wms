﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class DeliveryMethodWeightBands : CustomBaseObject
    {
        public DeliveryMethodWeightBands(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private decimal price;
        private decimal cost;
        private decimal toWeight;
        private decimal fromWeight;
        private DeliveryMethod deliveryMethod;

        public DeliveryMethod DeliveryMethod
        {
            get => deliveryMethod;
            set => SetPropertyValue(nameof(DeliveryMethod), ref deliveryMethod, value);
        }

        public decimal FromWeight
        {
            get => fromWeight;
            set => SetPropertyValue(nameof(FromWeight), ref fromWeight, value);
        }

        public decimal ToWeight
        {
            get => toWeight;
            set => SetPropertyValue(nameof(ToWeight), ref toWeight, value);
        }

        public decimal Cost
        {
            get => cost;
            set => SetPropertyValue(nameof(Cost), ref cost, value);
        }

        public decimal Price
        {
            get => price;
            set => SetPropertyValue(nameof(Price), ref price, value);
        }
    }
}