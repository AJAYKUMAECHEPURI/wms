﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.ProductVariant;

namespace WMS.App.Module.BusinessObjects.System.Variant
{
    [DefaultClassOptions]
    public class TransformVariant : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        // Use CodeRush to create XPO classes and properties with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/118557
        public TransformVariant(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        private string numberOfBaseUnits;
        private string variantDescription;
        private string variantCode;
        private VariantSettingTransformation transformation;

        [Association("VariantSettingTransformation-TransformVariant")]
        public VariantSettingTransformation Transformation
        {
            get => transformation;
            set => SetPropertyValue(nameof(Transformation), ref transformation, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantCode
        {
            get => variantCode;
            set => SetPropertyValue(nameof(VariantCode), ref variantCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantDescription
        {
            get => variantDescription;
            set => SetPropertyValue(nameof(VariantDescription), ref variantDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string NumberOfBaseUnits
        {
            get => numberOfBaseUnits;
            set => SetPropertyValue(nameof(NumberOfBaseUnits), ref numberOfBaseUnits, value);
        }
    }
}