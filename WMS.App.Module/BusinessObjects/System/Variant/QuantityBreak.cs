﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.ProductVariant;

namespace WMS.App.Module.BusinessObjects.System.Variant
{
    [DefaultClassOptions]
    public class QuantityBreak : BaseObject
    {
        public QuantityBreak(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private VariantQuantityBreak quantityBreak;
        private string newRSP;
        private string quantityDiscount;
        private int quantityTo;
        private int quantityFrom;

        public int QuantityFrom
        {
            get => quantityFrom;
            set => SetPropertyValue(nameof(QuantityFrom), ref quantityFrom, value);
        }

        public int QuantityTo
        {
            get => quantityTo;
            set => SetPropertyValue(nameof(QuantityTo), ref quantityTo, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string QuantityDiscount
        {
            get => quantityDiscount;
            set => SetPropertyValue(nameof(QuantityDiscount), ref quantityDiscount, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string NewRSP
        {
            get => newRSP;
            set => SetPropertyValue(nameof(NewRSP), ref newRSP, value);
        }

        [Association("VariantQuantityBreak-QuantityBreak")]
        public VariantQuantityBreak VariantQuantityBreak
        {
            get => quantityBreak;
            set => SetPropertyValue(nameof(VariantQuantityBreak), ref quantityBreak, value);
        }
    }
}