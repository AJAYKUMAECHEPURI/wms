﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System.Variant
{
    [DefaultClassOptions]
    public class VariantBinCategory : BaseObject
    {
        public VariantBinCategory(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string name;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string LimitReplenishmentToOneBatchPerBin
        {
            get => name;
            set => SetPropertyValue(nameof(LimitReplenishmentToOneBatchPerBin), ref name, value);
        }
    }
}