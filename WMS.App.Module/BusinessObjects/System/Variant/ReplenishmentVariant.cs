﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.ProductVariant;

namespace WMS.App.Module.BusinessObjects.System.Variant
{
    [DefaultClassOptions]
    public class ReplenishmentVariant : BaseObject
    {
        public ReplenishmentVariant(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int order;
        private string numberOfBaseUnits;
        private string variantDescription;
        private string variantCode;
        private VariantSettingTransformation transformation;

        [Association("VariantSettingTransformation-ReplenishmentVariant")]
        public VariantSettingTransformation Transformation
        {
            get => transformation;
            set => SetPropertyValue(nameof(Transformation), ref transformation, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantCode
        {
            get => variantCode;
            set => SetPropertyValue(nameof(VariantCode), ref variantCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantDescription
        {
            get => variantDescription;
            set => SetPropertyValue(nameof(VariantDescription), ref variantDescription, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string NumberOfBaseUnits
        {
            get => numberOfBaseUnits;
            set => SetPropertyValue(nameof(NumberOfBaseUnits), ref numberOfBaseUnits, value);
        }

        public int Order
        {
            get => order;
            set => SetPropertyValue(nameof(Order), ref order, value);
        }
    }
}