﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System.Variant
{
    [DefaultClassOptions]
    public class VariantPostcodeDeliveryMethodRule : BaseObject
    {
        public VariantPostcodeDeliveryMethodRule(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}