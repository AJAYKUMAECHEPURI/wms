﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System.Variant
{
    [DefaultClassOptions]
    public class UnitQuantity : BaseObject
    {
        public UnitQuantity(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool ePOSDefault;
        private string quantity;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Quantity
        {
            get => quantity;
            set => SetPropertyValue(nameof(Quantity), ref quantity, value);
        }

        public bool EPOSDefault
        {
            get => ePOSDefault;
            set => SetPropertyValue(nameof(EPOSDefault), ref ePOSDefault, value);
        }
    }
}