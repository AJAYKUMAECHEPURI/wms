﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class DeliveryMethodCourierInfo : CustomBaseObject
    {
        public DeliveryMethodCourierInfo(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool alwaysOutputCommInvoice;
        private bool fulfilmentMode;
        private bool suppressAutoOutputofShipmentLabel;
        private bool outputCN22Label;
        private bool insuredService;
        private bool sMSNotification;
        private bool faxNotification;
        private bool emailNotification;
        private bool pODRequired;
        private bool collectCashOnly;
        private bool paymentOnDelivery;
        private bool returnShipmentRequired;
        private bool saturdayDelivery;
        private string addressInfo2;
        private string addressInfo1;
        private string alternateeCommerceCarrierCode;
        private string eCommerceCarrierCode;
        private string packageType;
        private string serviceCategory;
        private string notifyLevel;
        private string serviceTrigger;
        private string serviceLevelCode;
        private string courierName;
        private DeliveryMethod deliveryMethod;

        public DeliveryMethod DeliveryMethod
        {
            get => deliveryMethod;
            set => SetPropertyValue(nameof(DeliveryMethod), ref deliveryMethod, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CourierName
        {
            get => courierName;
            set => SetPropertyValue(nameof(CourierName), ref courierName, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ServiceLevelCode
        {
            get => serviceLevelCode;
            set => SetPropertyValue(nameof(ServiceLevelCode), ref serviceLevelCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ServiceTrigger
        {
            get => serviceTrigger;
            set => SetPropertyValue(nameof(ServiceTrigger), ref serviceTrigger, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string NotifyLevel
        {
            get => notifyLevel;
            set => SetPropertyValue(nameof(NotifyLevel), ref notifyLevel, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ServiceCategory
        {
            get => serviceCategory;
            set => SetPropertyValue(nameof(ServiceCategory), ref serviceCategory, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PackageType
        {
            get => packageType;
            set => SetPropertyValue(nameof(PackageType), ref packageType, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        public string ECommerceCarrierCode
        {
            get => eCommerceCarrierCode;
            set => SetPropertyValue(nameof(ECommerceCarrierCode), ref eCommerceCarrierCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        public string AlternateCommerceCarrierCode
        {
            get => alternateeCommerceCarrierCode;
            set => SetPropertyValue(nameof(AlternateCommerceCarrierCode), ref alternateeCommerceCarrierCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        public string AddressInfo1
        {
            get => addressInfo1;
            set => SetPropertyValue(nameof(AddressInfo1), ref addressInfo1, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        public string AddressInfo2
        {
            get => addressInfo2;
            set => SetPropertyValue(nameof(AddressInfo2), ref addressInfo2, value);
        }

        [VisibleInListView(false)]
        public bool SaturdayDelivery
        {
            get => saturdayDelivery;
            set => SetPropertyValue(nameof(SaturdayDelivery), ref saturdayDelivery, value);
        }

        [VisibleInListView(false)]
        public bool ReturnShipmentRequired
        {
            get => returnShipmentRequired;
            set => SetPropertyValue(nameof(ReturnShipmentRequired), ref returnShipmentRequired, value);
        }

        [VisibleInListView(false)]
        public bool PaymentOnDelivery
        {
            get => paymentOnDelivery;
            set => SetPropertyValue(nameof(PaymentOnDelivery), ref paymentOnDelivery, value);
        }

        [VisibleInListView(false)]
        public bool CollectCashOnly
        {
            get => collectCashOnly;
            set => SetPropertyValue(nameof(CollectCashOnly), ref collectCashOnly, value);
        }

        [VisibleInListView(false)]
        public bool PODRequired
        {
            get => pODRequired;
            set => SetPropertyValue(nameof(PODRequired), ref pODRequired, value);
        }

        [VisibleInListView(false)]
        public bool EmailNotification
        {
            get => emailNotification;
            set => SetPropertyValue(nameof(EmailNotification), ref emailNotification, value);
        }

        [VisibleInListView(false)]
        public bool FaxNotification
        {
            get => faxNotification;
            set => SetPropertyValue(nameof(FaxNotification), ref faxNotification, value);
        }

        [VisibleInListView(false)]
        public bool SMSNotification
        {
            get => sMSNotification;
            set => SetPropertyValue(nameof(SMSNotification), ref sMSNotification, value);
        }

        [VisibleInListView(false)]
        public bool InsuredService
        {
            get => insuredService;
            set => SetPropertyValue(nameof(InsuredService), ref insuredService, value);
        }

        [VisibleInListView(false)]
        public bool OutputCN22Label
        {
            get => outputCN22Label;
            set => SetPropertyValue(nameof(OutputCN22Label), ref outputCN22Label, value);
        }

        [VisibleInListView(false)]
        public bool SuppressAutoOutputOfShipmentLabel
        {
            get => suppressAutoOutputofShipmentLabel;
            set => SetPropertyValue(nameof(SuppressAutoOutputOfShipmentLabel), ref suppressAutoOutputofShipmentLabel, value);
        }

        [VisibleInListView(false)]
        public bool FulfillmentMode
        {
            get => fulfilmentMode;
            set => SetPropertyValue(nameof(FulfillmentMode), ref fulfilmentMode, value);
        }

        [VisibleInListView(false)]
        public bool AlwaysOutputCommInvoice
        {
            get => alwaysOutputCommInvoice;
            set => SetPropertyValue(nameof(AlwaysOutputCommInvoice), ref alwaysOutputCommInvoice, value);
        }
    }
}