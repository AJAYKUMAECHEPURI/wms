﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class PriceListItems : CustomBaseObject
    {
        public PriceListItems(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private int sellingPrice;
        private int qtyTo;
        private int qtyFrom;
        private int marginPercentage;
        private int discountPrice;
        private int discountPercentage;
        private string variantDescription;
        private ProductVariant.Variant variantCode;
        private PriceList priceList;

        public PriceList PriceList
        {
            get => priceList;
            set => SetPropertyValue(nameof(PriceList), ref priceList, value);
        }

        public ProductVariant.Variant VariantCode
        {
            get => variantCode;
            set => SetPropertyValue(nameof(VariantCode), ref variantCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantDescription
        {
            get => variantDescription;
            set => SetPropertyValue(nameof(VariantDescription), ref variantDescription, value);
        }

        public int DiscountPercentage
        {
            get => discountPercentage;
            set => SetPropertyValue(nameof(DiscountPercentage), ref discountPercentage, value);
        }

        public int DiscountPrice
        {
            get => discountPrice;
            set => SetPropertyValue(nameof(DiscountPrice), ref discountPrice, value);
        }

        public int MarginPercentage
        {
            get => marginPercentage;
            set => SetPropertyValue(nameof(MarginPercentage), ref marginPercentage, value);
        }

        public int QtyFrom
        {
            get => qtyFrom;
            set => SetPropertyValue(nameof(QtyFrom), ref qtyFrom, value);
        }

        public int QtyTo
        {
            get => qtyTo;
            set => SetPropertyValue(nameof(QtyTo), ref qtyTo, value);
        }

        public int SellingPrice
        {
            get => sellingPrice;
            set => SetPropertyValue(nameof(SellingPrice), ref sellingPrice, value);
        }
    }
}