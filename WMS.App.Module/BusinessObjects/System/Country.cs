﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class Country : CustomBaseObject
    {
        public Country(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool fedExOutboundETD;
        private bool outputCN22Label;
        private bool fexExInboundETD;
        private bool eUIntrastat;
        private bool gSPCountry;
        private bool eCSalesList;
        private string phoneCountryCode;
        private string iSO3166Symbol;
        private string code;
        private string country;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CountryName
        {
            get => country;
            set => SetPropertyValue(nameof(CountryName), ref country, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Code
        {
            get => code;
            set => SetPropertyValue(nameof(Code), ref code, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ISO3166Symbol
        {
            get => iSO3166Symbol;
            set => SetPropertyValue(nameof(ISO3166Symbol), ref iSO3166Symbol, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PhoneCountryCode
        {
            get => phoneCountryCode;
            set => SetPropertyValue(nameof(PhoneCountryCode), ref phoneCountryCode, value);
        }

        public bool ECSalesList
        {
            get => eCSalesList;
            set => SetPropertyValue(nameof(ECSalesList), ref eCSalesList, value);
        }

        public bool GSPCountry
        {
            get => gSPCountry;
            set => SetPropertyValue(nameof(GSPCountry), ref gSPCountry, value);
        }

        public bool EUIntrastat
        {
            get => eUIntrastat;
            set => SetPropertyValue(nameof(EUIntrastat), ref eUIntrastat, value);
        }

        public bool FexExInboundETD
        {
            get => fexExInboundETD;
            set => SetPropertyValue(nameof(FexExInboundETD), ref fexExInboundETD, value);
        }

        public bool OutputCN22Label
        {
            get => outputCN22Label;
            set => SetPropertyValue(nameof(OutputCN22Label), ref outputCN22Label, value);
        }

        public bool FedExOutboundETD
        {
            get => fedExOutboundETD;
            set => SetPropertyValue(nameof(FedExOutboundETD), ref fedExOutboundETD, value);
        }
    }
}