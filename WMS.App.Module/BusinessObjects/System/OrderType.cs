﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Customer;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class OrderType : CustomBaseObject
    {
        public OrderType(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool standard;
        private string childClientName;
        private string childClientId;
        private bool overrideDiscountCostCenter;
        private bool overrideDeliveryCostCenter;
        private bool ocerrideDiscountDeliveryCode;
        private bool overrideDeliveryDepartmentCode;
        private DepartmentCode departmentCode;
        private bool overrideDiscountNominalCode;
        private bool overrideDeliveryNominalCode;
        private NominalCode nominalCode;
        private string description;
        private bool active;

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public NominalCode NominalCode
        {
            get => nominalCode;
            set => SetPropertyValue(nameof(NominalCode), ref nominalCode, value);
        }

        public bool OverrideDeliveryNominalCode
        {
            get => overrideDeliveryNominalCode;
            set => SetPropertyValue(nameof(OverrideDeliveryNominalCode), ref overrideDeliveryNominalCode, value);
        }

        public bool OverrideDiscountNominalCode
        {
            get => overrideDiscountNominalCode;
            set => SetPropertyValue(nameof(OverrideDiscountNominalCode), ref overrideDiscountNominalCode, value);
        }

        public DepartmentCode DepartmentCode
        {
            get => departmentCode;
            set => SetPropertyValue(nameof(DepartmentCode), ref departmentCode, value);
        }

        public bool OverrideDeliveryDepartmentCode
        {
            get => overrideDeliveryDepartmentCode;
            set => SetPropertyValue(nameof(OverrideDeliveryDepartmentCode), ref overrideDeliveryDepartmentCode, value);
        }

        public bool OverrideDiscountDeliveryCode
        {
            get => ocerrideDiscountDeliveryCode;
            set => SetPropertyValue(nameof(OverrideDiscountDeliveryCode), ref ocerrideDiscountDeliveryCode, value);
        }

        public bool OverrideDeliveryCostCenter
        {
            get => overrideDeliveryCostCenter;
            set => SetPropertyValue(nameof(OverrideDeliveryCostCenter), ref overrideDeliveryCostCenter, value);
        }

        public bool OverrideDiscountCostCenter
        {
            get => overrideDiscountCostCenter;
            set => SetPropertyValue(nameof(OverrideDiscountCostCenter), ref overrideDiscountCostCenter, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ChildClientId
        {
            get => childClientId;
            set => SetPropertyValue(nameof(ChildClientId), ref childClientId, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ChildClientName
        {
            get => childClientName;
            set => SetPropertyValue(nameof(ChildClientName), ref childClientName, value);
        }

        public bool Standard
        {
            get => standard;
            set => SetPropertyValue(nameof(Standard), ref standard, value);
        }
    }
}