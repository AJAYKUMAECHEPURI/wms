﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class TaxRate : CustomBaseObject
    {
        public TaxRate(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool includeInUKHMRCVATReturn;
        private string description;
        private decimal taxRate;
        private string taxCode;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string TaxCode
        {
            get => taxCode;
            set => SetPropertyValue(nameof(TaxCode), ref taxCode, value);
        }

        public decimal Tax_Rate
        {
            get => taxRate;
            set => SetPropertyValue(nameof(Tax_Rate), ref taxRate, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public bool IncludeInUKHMRCVATReturn
        {
            get => includeInUKHMRCVATReturn;
            set => SetPropertyValue(nameof(IncludeInUKHMRCVATReturn), ref includeInUKHMRCVATReturn, value);
        }
    }
}