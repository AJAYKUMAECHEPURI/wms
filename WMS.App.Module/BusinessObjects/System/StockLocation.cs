﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    public class StockLocation : BaseObject
    {
        public StockLocation(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Detail = new StockLocationDetail(Session);
            Setting = new StockLocationSetting(Session);
        }

        private StockLocationSetting setting;
        private StockLocationDetail detail;

        [DevExpress.Xpo.Aggregated]
        public StockLocationDetail Detail
        {
            get => detail;
            set => SetPropertyValue(nameof(Detail), ref detail, value);
        }

        [DevExpress.Xpo.Aggregated]
        public StockLocationSetting Setting
        {
            get => setting;
            set => SetPropertyValue(nameof(Setting), ref setting, value);
        }
    }
}