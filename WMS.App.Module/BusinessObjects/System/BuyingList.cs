﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class BuyingList : CustomBaseObject
    {
        public BuyingList(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string lastAmendedBy;
        private DateTime lastAmendedOn;
        private string inputBy;
        private DateTime inputOn;
        private string name;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        public DateTime InputOn
        {
            get => inputOn;
            set => SetPropertyValue(nameof(InputOn), ref inputOn, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string InputBy
        {
            get => inputBy;
            set => SetPropertyValue(nameof(InputBy), ref inputBy, value);
        }

        public DateTime LastAmendedOn
        {
            get => lastAmendedOn;
            set => SetPropertyValue(nameof(LastAmendedOn), ref lastAmendedOn, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string LastAmendedBy
        {
            get => lastAmendedBy;
            set => SetPropertyValue(nameof(LastAmendedBy), ref lastAmendedBy, value);
        }
    }
}