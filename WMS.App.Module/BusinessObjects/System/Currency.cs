﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class Currency : CustomBaseObject
    {
        public Currency(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string amendedBy;
        private DateTime amendedOn;
        private string inputBy;
        private DateTime inputOn;
        private int changeBankNominalCode;
        private string accountsPackageCode;
        private string colour;
        private string exchangeRate;
        private string ePOSSymbol;
        private string symbol;
        private string iSO4217Symbol;
        private string code;
        private bool baseCurrency;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public Boolean BaseCurrency
        {
            get => baseCurrency;
            set => SetPropertyValue(nameof(BaseCurrency), ref baseCurrency, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Code
        {
            get => code;
            set => SetPropertyValue(nameof(Code), ref code, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ISO4217Symbol
        {
            get => iSO4217Symbol;
            set => SetPropertyValue(nameof(ISO4217Symbol), ref iSO4217Symbol, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Symbol
        {
            get => symbol;
            set => SetPropertyValue(nameof(Symbol), ref symbol, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string EPOSSymbol
        {
            get => ePOSSymbol;
            set => SetPropertyValue(nameof(EPOSSymbol), ref ePOSSymbol, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ExchangeRate
        {
            get => exchangeRate;
            set => SetPropertyValue(nameof(ExchangeRate), ref exchangeRate, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Color
        {
            get => colour;
            set => SetPropertyValue(nameof(Color), ref colour, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AccountsPackageCode
        {
            get => accountsPackageCode;
            set => SetPropertyValue(nameof(AccountsPackageCode), ref accountsPackageCode, value);
        }

        public Int32 ChangeBankNominalCode
        {
            get => changeBankNominalCode;
            set => SetPropertyValue(nameof(ChangeBankNominalCode), ref changeBankNominalCode, value);
        }

        public DateTime InputOn
        {
            get => inputOn;
            set => SetPropertyValue(nameof(InputOn), ref inputOn, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public String InputBy
        {
            get => inputBy;
            set => SetPropertyValue(nameof(InputBy), ref inputBy, value);
        }

        public DateTime AmendedOn
        {
            get => amendedOn;
            set => SetPropertyValue(nameof(AmendedOn), ref amendedOn, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public String AmendedBy
        {
            get => amendedBy;
            set => SetPropertyValue(nameof(AmendedBy), ref amendedBy, value);
        }
    }
}