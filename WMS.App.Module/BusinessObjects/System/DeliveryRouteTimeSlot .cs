﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Customer;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class DeliveryRouteTimeSlot : CustomBaseObject
    {
        public DeliveryRouteTimeSlot(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private bool assign;
        private int maxOrders;
        private DateTime endTime;
        private DateTime startTime;
        private string description;
        private DeliveryRoute deliveryRoute;

        public DeliveryRoute DeliveryRoute
        {
            get => deliveryRoute;
            set => SetPropertyValue(nameof(DeliveryRoute), ref deliveryRoute, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public DateTime StartTime
        {
            get => startTime;
            set => SetPropertyValue(nameof(StartTime), ref startTime, value);
        }

        public DateTime EndTime
        {
            get => endTime;
            set => SetPropertyValue(nameof(EndTime), ref endTime, value);
        }

        public int MaxOrders
        {
            get => maxOrders;
            set => SetPropertyValue(nameof(MaxOrders), ref maxOrders, value);
        }

        public bool Assign
        {
            get => assign;
            set => SetPropertyValue(nameof(Assign), ref assign, value);
        }
    }
}