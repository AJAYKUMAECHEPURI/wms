﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Customer;
using static WMS.App.Module.BusinessObjects.System.Helpers.SystemConfig;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class DeliveryMethod : CustomBaseObject
    {
        public DeliveryMethod(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private DispatchProcess dispatchProcess;
        private DeliveryMethodType deliveryMethodType;
        private decimal additionalPalletCost;
        private decimal additionalParcelCost;
        private decimal firstPalletCost;
        private decimal firstParcelCost;
        private bool calculateCostByParcel_Pallet;
        private decimal additionalWeightPrice;
        private decimal additionalWeightCost;
        private decimal weightIncrement;
        private decimal baseMaximumWeight;
        private bool requiresSignatureOnDespatch;
        private bool excludeFromFreeDeliveryThreshold;
        private bool returnsDeliveryMethod;
        private bool ownTransportFacility;
        private bool doNotShowInEPOS;
        private bool doNotShowInSalesOrderEntry;
        private int maxParcelWeight;
        private int leadTime;
        private string hHTReferenceCode;
        private int warnOnShipWhenWeightAbove;
        private int priority;
        private Currency currency;
        private CostCenter costCentre;
        private DepartmentCode departmentCode;
        private TaxRate taxRate;
        private NominalCode nominalCode;
        private int upperOrderLimit;
        private int lowerOrderLimit;
        private bool grossDelivery;
        private int price;
        private int baseCost;
        private string deliveryMethodName;
        private bool hideFromDangerousOrHazardocusGood;
        private bool active;

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        public bool HideFromDangerousOrHazardousGood
        {
            get => hideFromDangerousOrHazardocusGood;
            set => SetPropertyValue(nameof(HideFromDangerousOrHazardousGood), ref hideFromDangerousOrHazardocusGood, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string DeliveryMethodName
        {
            get => deliveryMethodName;
            set => SetPropertyValue(nameof(DeliveryMethodName), ref deliveryMethodName, value);
        }

        public int BaseCost
        {
            get => baseCost;
            set => SetPropertyValue(nameof(BaseCost), ref baseCost, value);
        }

        public int Price
        {
            get => price;
            set => SetPropertyValue(nameof(Price), ref price, value);
        }

        public bool GrossDelivery
        {
            get => grossDelivery;
            set => SetPropertyValue(nameof(GrossDelivery), ref grossDelivery, value);
        }

        public int LowerOrderLimit
        {
            get => lowerOrderLimit;
            set => SetPropertyValue(nameof(LowerOrderLimit), ref lowerOrderLimit, value);
        }

        public int UpperOrderLimit
        {
            get => upperOrderLimit;
            set => SetPropertyValue(nameof(UpperOrderLimit), ref upperOrderLimit, value);
        }

        public NominalCode NominalCode
        {
            get => nominalCode;
            set => SetPropertyValue(nameof(NominalCode), ref nominalCode, value);
        }

        public TaxRate TaxRate
        {
            get => taxRate;
            set => SetPropertyValue(nameof(TaxRate), ref taxRate, value);
        }

        //[DataSourceCriteria("Type = 'DepartmentCode' AND Status = True")]
        public DepartmentCode DepartmentCode
        {
            get => departmentCode;
            set => SetPropertyValue(nameof(DepartmentCode), ref departmentCode, value);
        }

        //[DataSourceCriteria("Type = 'CostCenter' AND Status = True")]
        [VisibleInListView(false)]
        public CostCenter CostCenter
        {
            get => costCentre;
            set => SetPropertyValue(nameof(CostCenter), ref costCentre, value);
        }

        public Currency Currency
        {
            get => currency;
            set => SetPropertyValue(nameof(Currency), ref currency, value);
        }

        public int Priority
        {
            get => priority;
            set => SetPropertyValue(nameof(Priority), ref priority, value);
        }

        [VisibleInListView(false)]
        public int WarnOnShipWhenWeightAbove
        {
            get => warnOnShipWhenWeightAbove;
            set => SetPropertyValue(nameof(WarnOnShipWhenWeightAbove), ref warnOnShipWhenWeightAbove, value);
        }

        [VisibleInListView(false)]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string HHTReferenceCode
        {
            get => hHTReferenceCode;
            set => SetPropertyValue(nameof(HHTReferenceCode), ref hHTReferenceCode, value);
        }

        [VisibleInListView(false)]
        public DeliveryMethodType DeliveryMethodType
        {
            get => deliveryMethodType;
            set => SetPropertyValue(nameof(DeliveryMethodType), ref deliveryMethodType, value);
        }

        [VisibleInListView(false)]
        public DispatchProcess DispatchProcess
        {
            get => dispatchProcess;
            set => SetPropertyValue(nameof(DispatchProcess), ref dispatchProcess, value);
        }

        [VisibleInListView(false)]
        public int LeadTime
        {
            get => leadTime;
            set => SetPropertyValue(nameof(LeadTime), ref leadTime, value);
        }

        [VisibleInListView(false)]
        public int MaxParcelWeight
        {
            get => maxParcelWeight;
            set => SetPropertyValue(nameof(MaxParcelWeight), ref maxParcelWeight, value);
        }

        [VisibleInListView(false)]
        public bool DoNotShowInSalesOrderEntry
        {
            get => doNotShowInSalesOrderEntry;
            set => SetPropertyValue(nameof(DoNotShowInSalesOrderEntry), ref doNotShowInSalesOrderEntry, value);
        }

        [VisibleInListView(false)]
        public bool DoNotShowInEPOS
        {
            get => doNotShowInEPOS;
            set => SetPropertyValue(nameof(DoNotShowInEPOS), ref doNotShowInEPOS, value);
        }

        [VisibleInListView(false)]
        public bool OwnTransportFacility
        {
            get => ownTransportFacility;
            set => SetPropertyValue(nameof(OwnTransportFacility), ref ownTransportFacility, value);
        }

        [VisibleInListView(false)]
        public bool ReturnsDeliveryMethod
        {
            get => returnsDeliveryMethod;
            set => SetPropertyValue(nameof(ReturnsDeliveryMethod), ref returnsDeliveryMethod, value);
        }

        [VisibleInListView(false)]
        public bool ExcludeFromFreeDeliveryThreshold
        {
            get => excludeFromFreeDeliveryThreshold;
            set => SetPropertyValue(nameof(ExcludeFromFreeDeliveryThreshold), ref excludeFromFreeDeliveryThreshold, value);
        }

        [VisibleInListView(false)]
        public bool RequiresSignatureOnDespatch
        {
            get => requiresSignatureOnDespatch;
            set => SetPropertyValue(nameof(RequiresSignatureOnDespatch), ref requiresSignatureOnDespatch, value);
        }

        [VisibleInListView(false)]
        public decimal BaseMaximumWeight
        {
            get => baseMaximumWeight;
            set => SetPropertyValue(nameof(BaseMaximumWeight), ref baseMaximumWeight, value);
        }

        [VisibleInListView(false)]
        public decimal WeightIncrement
        {
            get => weightIncrement;
            set => SetPropertyValue(nameof(WeightIncrement), ref weightIncrement, value);
        }

        [VisibleInListView(false)]
        public decimal AdditionalWeightCost
        {
            get => additionalWeightCost;
            set => SetPropertyValue(nameof(AdditionalWeightCost), ref additionalWeightCost, value);
        }

        [VisibleInListView(false)]
        public decimal AdditionalWeightPrice
        {
            get => additionalWeightPrice;
            set => SetPropertyValue(nameof(AdditionalWeightPrice), ref additionalWeightPrice, value);
        }

        [VisibleInListView(false)]
        public bool CalculateCostByParcel_Pallet
        {
            get => calculateCostByParcel_Pallet;
            set => SetPropertyValue(nameof(CalculateCostByParcel_Pallet), ref calculateCostByParcel_Pallet, value);
        }

        [VisibleInListView(false)]
        public decimal FirstParcelCost
        {
            get => firstParcelCost;
            set => SetPropertyValue(nameof(FirstParcelCost), ref firstParcelCost, value);
        }

        [VisibleInListView(false)]
        public decimal FirstPalletCost
        {
            get => firstPalletCost;
            set => SetPropertyValue(nameof(FirstPalletCost), ref firstPalletCost, value);
        }

        [VisibleInListView(false)]
        public decimal AdditionalParcelCost
        {
            get => additionalParcelCost;
            set => SetPropertyValue(nameof(AdditionalParcelCost), ref additionalParcelCost, value);
        }

        [VisibleInListView(false)]
        public decimal AdditionalPalletCost
        {
            get => additionalPalletCost;
            set => SetPropertyValue(nameof(AdditionalPalletCost), ref additionalPalletCost, value);
        }
    }
}