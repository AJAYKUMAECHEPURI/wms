﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Customer;
using static WMS.App.Module.BusinessObjects.System.Helpers.SystemConfig;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class LoyaltyScheme : CustomBaseObject
    {
        public LoyaltyScheme(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private IncludeVariants includeVariants;
        private PointsReductionWhenSpendingPoints pointsReductionWhenSpendingPoints;
        private int maximumPointSpend;
        private int minimumPointSpend;
        private int loyalityPointValue;
        private bool earnPartialPoint;
        private bool excludeOrderWithGlobalDiscount;
        private bool excludeDiscountedItems;
        private int spendforOnePoint;
        private CostCenter costCenter;
        private DepartmentCode departmentCode;
        private string description;
        private bool active;

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public DepartmentCode DepartmentCode
        {
            get => departmentCode;
            set => SetPropertyValue(nameof(DepartmentCode), ref departmentCode, value);
        }

        public CostCenter CostCenter
        {
            get => costCenter;
            set => SetPropertyValue(nameof(CostCenter), ref costCenter, value);
        }

        public int SpendForOnePoint
        {
            get => spendforOnePoint;
            set => SetPropertyValue(nameof(SpendForOnePoint), ref spendforOnePoint, value);
        }

        public bool ExcludeDiscountedItems
        {
            get => excludeDiscountedItems;
            set => SetPropertyValue(nameof(ExcludeDiscountedItems), ref excludeDiscountedItems, value);
        }

        public bool ExcludeOrderWithGlobalDiscount
        {
            get => excludeOrderWithGlobalDiscount;
            set => SetPropertyValue(nameof(ExcludeOrderWithGlobalDiscount), ref excludeOrderWithGlobalDiscount, value);
        }

        public bool EarnPartialPoint
        {
            get => earnPartialPoint;
            set => SetPropertyValue(nameof(EarnPartialPoint), ref earnPartialPoint, value);
        }

        public IncludeVariants IncludeVariants
        {
            get => includeVariants;
            set => SetPropertyValue(nameof(IncludeVariants), ref includeVariants, value);
        }

        public PointsReductionWhenSpendingPoints PointsReductionWhenSpendingPoints
        {
            get => pointsReductionWhenSpendingPoints;
            set => SetPropertyValue(nameof(PointsReductionWhenSpendingPoints), ref pointsReductionWhenSpendingPoints, value);
        }

        public int LoyaltyPointValue
        {
            get => loyalityPointValue;
            set => SetPropertyValue(nameof(LoyaltyPointValue), ref loyalityPointValue, value);
        }

        public int MinimumPointSpend
        {
            get => minimumPointSpend;
            set => SetPropertyValue(nameof(MinimumPointSpend), ref minimumPointSpend, value);
        }

        public int MaximumPointSpend
        {
            get => maximumPointSpend;
            set => SetPropertyValue(nameof(MaximumPointSpend), ref maximumPointSpend, value);
        }

        [Association("LoyaltyScheme-Variants"), Aggregated]
        public XPCollection<LoyaltySchemeVariants> Variants
        {
            get { return GetCollection<LoyaltySchemeVariants>(nameof(Variants)); }
        }
    }
}