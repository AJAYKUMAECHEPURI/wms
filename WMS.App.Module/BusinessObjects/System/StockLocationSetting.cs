﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    public class StockLocationSetting : BaseObject
    {
        public StockLocationSetting(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private StockBin defaultRecyclingBin;
        private StockBin locationPickingBin;
        private bool enforceStorageCondition;
        private bool allowDeliveryToTheLocation;
        private bool allowDeliveryFromtheLocation;
        private string aPIPassword;
        private string aPIUserName;
        private string transferSupplier;
        private string transferCustomer;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string TransferCustomer
        {
            get => transferCustomer;
            set => SetPropertyValue(nameof(TransferCustomer), ref transferCustomer, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string TransferSupplier
        {
            get => transferSupplier;
            set => SetPropertyValue(nameof(TransferSupplier), ref transferSupplier, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string APIUserName
        {
            get => aPIUserName;
            set => SetPropertyValue(nameof(APIUserName), ref aPIUserName, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string APIPassword
        {
            get => aPIPassword;
            set => SetPropertyValue(nameof(APIPassword), ref aPIPassword, value);
        }

        public bool AllowDeliveryFromTheLocation
        {
            get => allowDeliveryFromtheLocation;
            set => SetPropertyValue(nameof(AllowDeliveryFromTheLocation), ref allowDeliveryFromtheLocation, value);
        }

        public bool AllowDeliveryToTheLocation
        {
            get => allowDeliveryToTheLocation;
            set => SetPropertyValue(nameof(AllowDeliveryToTheLocation), ref allowDeliveryToTheLocation, value);
        }

        public bool EnforceStorageCondition
        {
            get => enforceStorageCondition;
            set => SetPropertyValue(nameof(EnforceStorageCondition), ref enforceStorageCondition, value);
        }

        public StockBin LocationPickingBin
        {
            get => locationPickingBin;
            set => SetPropertyValue(nameof(LocationPickingBin), ref locationPickingBin, value);
        }

        public StockBin DefaultRecyclingBin
        {
            get => defaultRecyclingBin;
            set => SetPropertyValue(nameof(DefaultRecyclingBin), ref defaultRecyclingBin, value);
        }
    }
}