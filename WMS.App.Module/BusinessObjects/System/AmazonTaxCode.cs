﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    public class AmazonTaxCode : BaseObject
    {
        public AmazonTaxCode(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string description;
        private string taxCode;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string TaxCode
        {
            get => taxCode;
            set => SetPropertyValue(nameof(TaxCode), ref taxCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }
    }
}