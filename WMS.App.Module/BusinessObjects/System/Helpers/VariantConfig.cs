﻿namespace WMS.App.Module.BusinessObjects.System.Helpers
{
    public static class VariantConfig
    {
        public enum CN22Type
        {
            Other, Gift, Commercial, Sample, Document
        }

        public enum DespatchGoodsLabelOption
        {
            PrintOneLabelPerLineItem,
            PrintMultipleLabelsPerLineItem
        }

        public enum TaxCalDirection
        {
            FromNet,
            FromGross
        }

        public enum EstimatedCostSettings
        {
            LastCostPaid,
            AverageCostOfGoodsPresentlyInStockReCalOnPositiveAndNegativeTransaction,
            AverageCostOfGoodsPresentlyInStockReCalOnPositiveTransaction,
            ManuallyUpdated,
            LastCostOfGoodsOrdered,
            MainSupplierSpecialCost,
            LastPositiveTransactionCost,
            MainSupplierLastCost,
            MainSupplierStandardCost
        }

        public enum POCostTrough
        {
            LastCostMainSupplier,
            LastCostSameSupplier,
            LastCostAnySupplier,
            CheapestCostMainSupplier,
            CheapestCostSameSupplier,
            CheapestCostAnySupplier,
            FixedCostSameSupplier,
            StandardCostSameSupplier
        }

        public enum AppliesTo
        {
            GSP, NonGSP, AllCountry
        }

        public enum LeadTimeToPurchaseOrder
        {
            MainSupplierGuideLeadTime,
            MaximumGuideLeadTime,
            FixedGuideLeadTime,
            MinimumGuideLeadTime,
            AverageGuideLeadTime
        }

        public enum BufferIntoPurchaseOrder
        {
            MainSupplierStockInBuffer,
            MaximumStockInBuffer,
            MinimumStockInBuffer,
            FixedStockInBuffer,
            AverageStockInBuffer
        }

        public enum AutoPrintOptions
        {
            Never, PickStart, PickComplement
        }

        public enum ConditionType
        {
            New
        }

        public enum TransactionType
        {
            StockAdjustmentIn,
            StockAdjustmentOut
        }
    }
}