﻿using System.ComponentModel.DataAnnotations;

namespace WMS.App.Module.BusinessObjects.System.Helpers
{
    public static class ProductConfig
    {
        public enum ProductType
        {
            ServiceItem,
            NonTransactional,
            UseVariantUnitQuantities
        }

        public enum UsageMethod
        {
            FIFO, SoonestExpiryDate, Manual, WalkRoute, SmallestTransactionalQuantity
        }

        public enum ForceAllocationMethod
        {
            OnlyAllocateInDespatch, AlwaysForceAllocateBeforeDespatch, AllowForceAllocateBeforeDespatch
        }

        public enum ShowUsageFor
        {
            Product, Variant
        }

        public enum ScanPattern
        {
            [Display(Name = "B/N, S/N, B/N, S/N ")]
            BNSNBNSN,

            [Display(Name = "B/N, S/N, S/N, S/N ")]
            BNSNSNSN
        }

        public enum DataType
        {
            Character,
            Logical,
            Numerical,
            Datetime
        }

        public enum BarcodeMode
        {
            Pattern,
            Delimiter
        }

        public enum DecimalPosition
        {
            DecimalProvider, FixedDecimal, DecimalUsingFirstQ
        }

        public enum Mode
        {
            Manual,
            LinkedField,
            Property
        }

        public enum AutoPrintOption
        {
            Never,
            PickStart,
            PickComplete
        }
    }
}