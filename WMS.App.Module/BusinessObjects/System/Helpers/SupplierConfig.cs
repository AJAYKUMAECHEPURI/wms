﻿namespace WMS.App.Module.BusinessObjects.System.Helpers
{
    public static class SupplierConfig
    {
        public enum SupplierPOType
        {
            PurchaseOrder
        }

        public enum SupplierDayFrom
        {
            InvoiceDate
        }

        public enum SettlementFrom
        {
            InvoiceDate
        }

        public enum AutoHoldOnOverDue
        {
            DoNotChange, OverCurrent, OverPeriod1, OverPeriod2, OverPeriod3
        }

        public enum PurchaseInvoiceDetailLevel
        {
            Summarizers,
            Detailed
        }

        public enum CalculationPeriod
        {
            Month
        }

        public enum PaymentFrequency
        {
            Month
        }

        public enum PaymentMethod
        {
            BACS
        }

        public enum ShowOn
        {
            None, OrderOpen, OrderSave
        }
    }
}