﻿namespace WMS.App.Module.BusinessObjects.System.Helpers
{
    public static class SystemConfig
    {
        public enum AddersType
        {
            Delivery,
            Invoice
        }

        public enum Types
        {
            CustomerType,
            CustomerStatus,
            CustomerSource,
            DeliveryRoute,
            ContactJobRole,
            DepartmentCode,
            PaymentTerms,
            NotificationTriggerType,
            CustomerDiscountStructure,
            CostCenter
        }

        public enum SalesPriceIncTaxOverride
        {
            None,
            WithoutTax,
            WithTax
        }

        public enum GenerateCustomerStatementsOutputType
        {
            None, Email, Print
        }

        public enum GenerateCreditControlLettersOutputType
        {
            None, Email, Print
        }

        public enum IncludeVariants
        {
            ALL, Specific
        }

        public enum PointsReductionWhenSpendingPoints
        {
            None, Min, Max
        }

        public enum EarnPointForOrderCreatedIn
        {
            MainSystem, eComers
        }

        public enum AutoPlaceOverCreditTerms
        {
            DoNotChange, OverCurrent, OverPeriod1, OverPeriod2, OverPeriod3
        }

        public enum AutoHoldOnAgedBalance
        {
            DoNotChange, OverCurrent, OverPeriod1, OverPeriod2, OverPeriod3
        }

        public enum Posting
        {
            DetailedAccountTransaction, SummarizedAccountsTransactions
        }

        public enum DefaultMethod
        {
            DiscountPrice, MarginPercentage, DiscountPercentage, RPSModifier, CostModifier, MarkupPercentage
        }

        public enum DeliveryMethodType
        {
            StandardPricing, WeightBandPricing, CalculatedPricing
        }

        public enum DefaultDeliveryAddressDeliveryGroup
        {
            None
        }

        public enum DispatchProcess
        {
            Pick_ship,
            Pick_pack_ship
        }

        public enum FMDDespatchAction
        {
            None
        }

        public enum AutoApplyDiscountCode
        {
            None
        }

        public enum DiscountMethod
        {
            BuyXGetYForZ,
            GetXForY,
            GetXForYGroup
        }

        public enum UnitOfMeasure
        {
            Gram,
            Kilo
        }
    }
}