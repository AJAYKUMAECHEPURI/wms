﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class DeliveryGroup : CustomBaseObject
    {
        public DeliveryGroup(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string courier;
        private decimal upperOrderLimit;
        private decimal lowerOrderLimit;
        private NominalCode nominalCode;
        private TaxRate taxRate;
        private decimal price;
        private decimal baseCost;
        private bool active;
        private DeliveryMethod deliveryMethod;
        private string deliveryGroupName;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string DeliveryGroupName
        {
            get => deliveryGroupName;
            set => SetPropertyValue(nameof(DeliveryGroupName), ref deliveryGroupName, value);
        }

        public DeliveryMethod DeliveryMethod
        {
            get => deliveryMethod;
            set => SetPropertyValue(nameof(DeliveryMethod), ref deliveryMethod, value);
        }

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        public decimal BaseCost
        {
            get => baseCost;
            set => SetPropertyValue(nameof(BaseCost), ref baseCost, value);
        }

        public decimal Price
        {
            get => price;
            set => SetPropertyValue(nameof(Price), ref price, value);
        }

        public TaxRate TaxRate
        {
            get => taxRate;
            set => SetPropertyValue(nameof(TaxRate), ref taxRate, value);
        }

        public NominalCode NominalCode
        {
            get => nominalCode;
            set => SetPropertyValue(nameof(NominalCode), ref nominalCode, value);
        }

        public decimal LowerOrderLimit
        {
            get => lowerOrderLimit;
            set => SetPropertyValue(nameof(LowerOrderLimit), ref lowerOrderLimit, value);
        }

        public decimal UpperOrderLimit
        {
            get => upperOrderLimit;
            set => SetPropertyValue(nameof(UpperOrderLimit), ref upperOrderLimit, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Courier
        {
            get => courier;
            set => SetPropertyValue(nameof(Courier), ref courier, value);
        }
    }
}