﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    public class PackagingType : BaseObject
    {
        public PackagingType(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private PackagingType category;
        private bool recyclable;
        private string code;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Code
        {
            get => code;
            set => SetPropertyValue(nameof(Code), ref code, value);
        }

        public bool Recyclable
        {
            get => recyclable;
            set => SetPropertyValue(nameof(Recyclable), ref recyclable, value);
        }

        public PackagingType Category
        {
            get => category;
            set => SetPropertyValue(nameof(Category), ref category, value);
        }
    }
}