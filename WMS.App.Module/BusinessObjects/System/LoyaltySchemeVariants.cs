﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class LoyaltySchemeVariants : CustomBaseObject
    {
        public LoyaltySchemeVariants(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private LoyaltyScheme loyaltyScheme;
        private int overridePoint;
        private bool overridePointValue;
        private ProductVariant.Variant variant;

        public ProductVariant.Variant Variant
        {
            get => variant;
            set => SetPropertyValue(nameof(Variant), ref variant, value);
        }

        public bool OverridePointValue
        {
            get => overridePointValue;
            set => SetPropertyValue(nameof(OverridePointValue), ref overridePointValue, value);
        }

        public int OverridePoint
        {
            get => overridePoint;
            set => SetPropertyValue(nameof(OverridePoint), ref overridePoint, value);
        }

        [Association("LoyaltyScheme-Variants")]
        public LoyaltyScheme HideFromDangerousOrHazardousGood
        {
            get => loyaltyScheme;
            set => SetPropertyValue(nameof(HideFromDangerousOrHazardousGood), ref loyaltyScheme, value);
        }
    }
}