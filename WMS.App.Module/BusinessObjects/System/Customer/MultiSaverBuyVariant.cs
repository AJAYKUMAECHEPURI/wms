﻿using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System.Customer
{
    [DefaultClassOptions]
    [Appearance("MultiSaverRule1", TargetItems = "MultiSaverGetVariant,MultiSaverVariantGroup", Criteria = "DiscountMethod = 'GetXForY'", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide)]
    [Appearance("MultiSaverRule2", TargetItems = "MultiSaverGetVariant", Criteria = "DiscountMethod = null", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide)]
    public class MultiSaverBuyVariant : BaseObject
    {
        public MultiSaverBuyVariant(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private MultiSaverVariantGroup multiSaverVariantGroup;
        private MultiSaverDiscount multiSaverDiscount;
        private int order;
        private string variantDescription;
        private string variantCode;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantCode
        {
            get => variantCode;
            set => SetPropertyValue(nameof(VariantCode), ref variantCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantDescription
        {
            get => variantDescription;
            set => SetPropertyValue(nameof(VariantDescription), ref variantDescription, value);
        }

        public int Order
        {
            get => order;
            set => SetPropertyValue(nameof(Order), ref order, value);
        }

        [Association("MultiSaverDiscount-MultiSaverBuyVariant")]
        public MultiSaverDiscount MultiSaverDiscount
        {
            get => multiSaverDiscount;
            set => SetPropertyValue(nameof(MultiSaverDiscount), ref multiSaverDiscount, value);
        }

        [Association("MultiSaverBuyVariant-MultiSaverGetVariants"), Aggregated]
        public XPCollection<MultiSaverGetVariant> MultiSaverGetVariant
        {
            get
            {
                return GetCollection<MultiSaverGetVariant>(nameof(MultiSaverGetVariant));
            }
        }

        [VisibleInDetailView(false)]
        [Association("MultiSaverVariantGroup-MultiSaverBuyVariants")]
        public MultiSaverVariantGroup MultiSaverVariantGroup
        {
            get => multiSaverVariantGroup;
            set => SetPropertyValue(nameof(MultiSaverVariantGroup), ref multiSaverVariantGroup, value);
        }

        [VisibleInDetailView(false)]
        public string DiscountMethod
        {
            get
            {
                if (multiSaverDiscount != null)
                {
                    return multiSaverDiscount.DiscountMethod.ToString();
                }
                return null;
            }
        }
    }
}