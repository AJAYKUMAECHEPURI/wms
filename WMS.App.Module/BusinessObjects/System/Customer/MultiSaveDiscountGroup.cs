﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System.Customer
{
    [DefaultClassOptions]
    public class MultiSaveDiscountGroup : BaseObject
    {
        public MultiSaveDiscountGroup(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private Currency currency;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public Currency Currency
        {
            get => currency;
            set => SetPropertyValue(nameof(Currency), ref currency, value);
        }

        [Association("MultiSaveDiscountGroup-MultiSaverDiscount"), DevExpress.Xpo.Aggregated]
        public XPCollection<MultiSaverDiscount> MultiSaverDiscount
        {
            get
            {
                return GetCollection<MultiSaverDiscount>(nameof(MultiSaverDiscount));
            }
        }
    }
}