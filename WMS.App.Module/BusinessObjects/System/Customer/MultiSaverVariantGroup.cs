﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System.Customer
{
    [DefaultClassOptions]
    public class MultiSaverVariantGroup : BaseObject
    {
        public MultiSaverVariantGroup(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private MultiSaverDiscount multiSaverDiscount;
        private int quantiry;
        private string description;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        public int Quantity
        {
            get => quantiry;
            set => SetPropertyValue(nameof(Quantity), ref quantiry, value);
        }

        [Association("MultiSaverDiscount-MultiSaverVariantGroup")]
        public MultiSaverDiscount MultiSaverDiscount
        {
            get => multiSaverDiscount;
            set => SetPropertyValue(nameof(MultiSaverDiscount), ref multiSaverDiscount, value);
        }

        [Association("MultiSaverVariantGroup-MultiSaverBuyVariants")]
        public XPCollection<MultiSaverBuyVariant> MultiSaverBuyVariants
        {
            get
            {
                return GetCollection<MultiSaverBuyVariant>(nameof(MultiSaverBuyVariants));
            }
        }

        [VisibleInDetailView(false)]
        public string DiscountMethod
        {
            get { return multiSaverDiscount.DiscountMethod.ToString(); }
        }
    }
}