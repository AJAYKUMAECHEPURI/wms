﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WMS.App.Module.BusinessObjects.System.Customer
{
    [DefaultClassOptions]
    public class MultiSaverGetVariant : BaseObject
    {
        public MultiSaverGetVariant(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private MultiSaverBuyVariant multiSaverBuyVariant;
        private int order;
        private string variantDescription;
        private string variantCode;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantCode
        {
            get => variantCode;
            set => SetPropertyValue(nameof(VariantCode), ref variantCode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string VariantDescription
        {
            get => variantDescription;
            set => SetPropertyValue(nameof(VariantDescription), ref variantDescription, value);
        }

        public int Order
        {
            get => order;
            set => SetPropertyValue(nameof(Order), ref order, value);
        }

        [Association("MultiSaverBuyVariant-MultiSaverGetVariants")]
        public MultiSaverBuyVariant MultiSaverBuyVariant
        {
            get => multiSaverBuyVariant;
            set => SetPropertyValue(nameof(MultiSaverBuyVariant), ref multiSaverBuyVariant, value);
        }
    }
}