﻿using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using static WMS.App.Module.BusinessObjects.System.Helpers.SystemConfig;

namespace WMS.App.Module.BusinessObjects.System.Customer
{
    [DefaultClassOptions]
    [Appearance("MultiSaverRule1", TargetItems = "MultiSaverVariantGroup", Criteria = "DiscountMethod = 'BuyXGetYForZ'", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide)]
    [Appearance("MultiSaverRule3", TargetItems = "MultiSaverBuyVariant,BuyQuantity,GetQuantity,ApplyToGreaterThanBuyQuantity", Criteria = "DiscountMethod = 'GetXForYGroup'", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide)]
    [Appearance("MultiSaverRule5", TargetItems = "MultiSaverVariantGroup,GetQuantity", Criteria = "DiscountMethod = 'GetXForY'", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide)]
    public class MultiSaverDiscount : BaseObject
    {
        public MultiSaverDiscount(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private MultiSaveDiscountGroup multiSaveDiscountGroup;
        private int discountPercentage;
        private int discount;
        private int price;
        private bool applyToGreaterThanBuyQuantity;
        private int getQuantity;
        private int buyQuantity;
        private DateTime endDate;
        private DateTime startDate;
        private DiscountMethod discountMethod;
        private bool useGrossPrice;
        private Currency currency;
        private string orderNarrative;
        private string description;
        private bool active;

        public bool Active
        {
            get => active;
            set => SetPropertyValue(nameof(Active), ref active, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [RuleRequiredField("MultiSaverDiscount.Description", DefaultContexts.Save, "Description Should not be Empty")]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string OrderNarrative
        {
            get => orderNarrative;
            set => SetPropertyValue(nameof(OrderNarrative), ref orderNarrative, value);
        }

        public Currency Currency
        {
            get => currency;
            set => SetPropertyValue(nameof(Currency), ref currency, value);
        }

        public bool UseGrossPrice
        {
            get => useGrossPrice;
            set => SetPropertyValue(nameof(UseGrossPrice), ref useGrossPrice, value);
        }

        public DiscountMethod DiscountMethod
        {
            get => discountMethod;
            set => SetPropertyValue(nameof(DiscountMethod), ref discountMethod, value);
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public DateTime StartDate
        {
            get => startDate;
            set => SetPropertyValue(nameof(StartDate), ref startDate, value);
        }

        public DateTime EndDate
        {
            get => endDate;
            set => SetPropertyValue(nameof(EndDate), ref endDate, value);
        }

        public int BuyQuantity
        {
            get => buyQuantity;
            set => SetPropertyValue(nameof(BuyQuantity), ref buyQuantity, value);
        }

        public int GetQuantity
        {
            get => getQuantity;
            set => SetPropertyValue(nameof(GetQuantity), ref getQuantity, value);
        }

        public bool ApplyToGreaterThanBuyQuantity
        {
            get => applyToGreaterThanBuyQuantity;
            set => SetPropertyValue(nameof(ApplyToGreaterThanBuyQuantity), ref applyToGreaterThanBuyQuantity, value);
        }

        public int Price
        {
            get => price;
            set => SetPropertyValue(nameof(Price), ref price, value);
        }

        public int Discount
        {
            get => discount;
            set => SetPropertyValue(nameof(Discount), ref discount, value);
        }

        public int DiscountPercentage
        {
            get => discountPercentage;
            set => SetPropertyValue(nameof(DiscountPercentage), ref discountPercentage, value);
        }

        [Association("MultiSaveDiscountGroup-MultiSaverDiscount")]
        public MultiSaveDiscountGroup MultiSaveDiscountGroup
        {
            get => multiSaveDiscountGroup;
            set => SetPropertyValue(nameof(MultiSaveDiscountGroup), ref multiSaveDiscountGroup, value);
        }

        [Association("MultiSaverDiscount-MultiSaverVariantGroup"), Aggregated]
        public XPCollection<MultiSaverVariantGroup> MultiSaverVariantGroup
        {
            get
            {
                return GetCollection<MultiSaverVariantGroup>(nameof(MultiSaverVariantGroup));
            }
        }

        [Association("MultiSaverDiscount-MultiSaverBuyVariant"), Aggregated]
        public XPCollection<MultiSaverBuyVariant> MultiSaverBuyVariant
        {
            get
            {
                return GetCollection<MultiSaverBuyVariant>(nameof(MultiSaverBuyVariant));
            }
        }
    }
}