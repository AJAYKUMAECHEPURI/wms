﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WMS.App.Module.BusinessObjects.System.Customer;

namespace WMS.App.Module.BusinessObjects.System
{
    [DefaultClassOptions]
    [NavigationItem("System")]
    public class StockLocationDetail : CustomBaseObject
    {
        public StockLocationDetail(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private CostCenter costCenter;
        private bool onlySendAllocatedToFulfilment;
        private bool fullfillmentWarehouse;
        private string comments;
        private string reference;
        private string category;
        private string email;
        private string fax;
        private string telephone;
        private string postcode;
        private Country country;
        private string town;
        private string address3;
        private string address2;
        private string address1;
        private string contact;
        private string name;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Contact
        {
            get => contact;
            set => SetPropertyValue(nameof(Contact), ref contact, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address1
        {
            get => address1;
            set => SetPropertyValue(nameof(Address1), ref address1, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address2
        {
            get => address2;
            set => SetPropertyValue(nameof(Address2), ref address2, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address3
        {
            get => address3;
            set => SetPropertyValue(nameof(Address3), ref address3, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Town
        {
            get => town;
            set => SetPropertyValue(nameof(Town), ref town, value);
        }

        public Country Country
        {
            get => country;
            set => SetPropertyValue(nameof(Country), ref country, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Postcode
        {
            get => postcode;
            set => SetPropertyValue(nameof(Postcode), ref postcode, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Telephone
        {
            get => telephone;
            set => SetPropertyValue(nameof(Telephone), ref telephone, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Fax
        {
            get => fax;
            set => SetPropertyValue(nameof(Fax), ref fax, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Email
        {
            get => email;
            set => SetPropertyValue(nameof(Email), ref email, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Category
        {
            get => category;
            set => SetPropertyValue(nameof(Category), ref category, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Reference
        {
            get => reference;
            set => SetPropertyValue(nameof(Reference), ref reference, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Comments
        {
            get => comments;
            set => SetPropertyValue(nameof(Comments), ref comments, value);
        }

        public bool FulfillmentWarehouse
        {
            get => fullfillmentWarehouse;
            set => SetPropertyValue(nameof(FulfillmentWarehouse), ref fullfillmentWarehouse, value);
        }

        public bool OnlySendAllocatedToFulfillment
        {
            get => onlySendAllocatedToFulfilment;
            set => SetPropertyValue(nameof(OnlySendAllocatedToFulfillment), ref onlySendAllocatedToFulfilment, value);
        }

        public CostCenter CostCenter
        {
            get => costCenter;
            set => SetPropertyValue(nameof(CostCenter), ref costCenter, value);
        }
    }
}