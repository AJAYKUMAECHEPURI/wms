﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using WMS.App.Module.BusinessObjects.ProductVariant;
using WMS.App.Module.BusinessObjects.Stock;
using WMS.App.Module.BusinessObjects.System;
using WMS.App.Module.Models;
using static WMS.App.Module.BusinessObjects.System.Helpers.StockConfig;

namespace WMS.App.Module.Actions
{
    public class StockAction : BaseAction, IStockAction
    {
        public StockAction(IObjectSpace objectSpace) : base(objectSpace)
        {
        }

        public bool CreateTransaction(StockTransaction stockTransaction)
        {
            try
            {
                CriteriaOperator complexCriteria = CriteriaOperator.And(
                new BinaryOperator(nameof(stockTransaction.BatchNumber), stockTransaction.BatchNumber),
                new BinaryOperator(nameof(stockTransaction.SerialNumber), stockTransaction.SerialNumber),
                new BinaryOperator(nameof(stockTransaction.Variant), stockTransaction.Variant),
                new BinaryOperator(nameof(stockTransaction.Location), stockTransaction.Location)
                );
                StockItem stockItem = FindData<StockItem>(complexCriteria);
                if (stockItem == null)
                {
                    AddStockItem(stockTransaction);
                    return true;
                }
                else
                {
                    //IssueStock
                    UpdateStockItem(stockTransaction, stockItem);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool TransferStock(StockItem from, StockItem to, int Quantity)
        {
            try
            {
                if (!ValidateTransfer(from, to) || Quantity <= 0)
                {
                    return false;
                }
                var fromTrans = _objectSpace.CreateObject<StockTransaction>();

                fromTrans.Quantity = Quantity;
                fromTrans.TransactionType = TransactionType.StockAdjustmentOut;
                fromTrans.Variant = from.Variant;
                fromTrans.Location = from.Location;
                fromTrans.BatchNumber = from.BatchNumber;
                fromTrans.SerialNumber = from.SerialNumber;
                from.Quantity -= Quantity;

                var toTrans = _objectSpace.CreateObject<StockTransaction>();

                toTrans.Quantity = Quantity;
                toTrans.TransactionType = TransactionType.StockAdjustmentIn;
                toTrans.Variant = from.Variant;
                toTrans.Location = from.Location;
                toTrans.BatchNumber = from.BatchNumber;
                toTrans.SerialNumber = from.SerialNumber;
                toTrans.Quantity += Quantity;
                to.Quantity += Quantity;
            }
            catch (Exception) { throw; }
            return false;
        }

        public ResponseModel UpdateStockItem(StockTransaction stockTransaction)
        {
            try
            {
                CriteriaOperator complexCriteria = CriteriaOperator.And(
                new BinaryOperator(nameof(stockTransaction.BatchNumber), stockTransaction.BatchNumber),
                new BinaryOperator(nameof(stockTransaction.SerialNumber), stockTransaction.SerialNumber),
                new BinaryOperator(nameof(stockTransaction.Variant), stockTransaction.Variant),
                new BinaryOperator(nameof(stockTransaction.Location), stockTransaction.Location)
                );
                StockItem stockItem = FindData<StockItem>(complexCriteria);
                if (stockItem != null)
                {
                    if (stockTransaction.TransactionType == TransactionType.StockAdjustmentIn)
                    {
                        stockItem.Quantity += stockTransaction.Quantity;
                    }
                    if (stockTransaction.TransactionType == TransactionType.StockAdjustmentOut)
                    {
                        stockItem.Quantity -= stockTransaction.Quantity;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return ResponseModel;
        }

        private void AddStockItem(StockTransaction stockTransaction)
        {
            StockItem stockItem = _objectSpace.CreateObject<StockItem>();
            stockItem.BatchNumber = stockTransaction.BatchNumber;
            stockItem.Variant = _objectSpace.GetObjectByKey<Variant>(stockTransaction.Variant.Oid);
            stockItem.Location = _objectSpace.GetObjectByKey<StockLocation>(stockTransaction.Location.Oid);
            stockItem.SerialNumber = stockTransaction.SerialNumber;

            if (stockTransaction.TransactionType == TransactionType.StockAdjustmentIn)
            {
                stockItem.Quantity += stockTransaction.Quantity;
            }
            if (stockTransaction.TransactionType == TransactionType.StockAdjustmentOut)
            {
                stockItem.Quantity -= stockTransaction.Quantity;
            }
        }

        private void UpdateStockItem(StockTransaction stockTransaction, StockItem stockItem)
        {
            if (stockTransaction.TransactionType == TransactionType.StockAdjustmentIn)
            {
                stockItem.Quantity += stockTransaction.Quantity;
            }
            if (stockTransaction.TransactionType == TransactionType.StockAdjustmentOut)
            {
                stockItem.Quantity -= stockTransaction.Quantity;
            }
        }

        private bool ValidateTransfer(StockItem from, StockItem to)
        {
            if (from.Variant != to.Variant) return false;
            return true;
        }

        public void Dispose()
        {
            //Dispose();
        }
    }
}