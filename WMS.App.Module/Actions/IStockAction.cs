﻿using WMS.App.Module.BusinessObjects.Stock;
using WMS.App.Module.Models;

namespace WMS.App.Module.Actions
{
    public interface IStockAction : IDisposable
    {
        bool CreateTransaction(StockTransaction stockTransaction);

        ResponseModel UpdateStockItem(StockTransaction stockTransaction);

        bool TransferStock(StockItem from, StockItem to, int Quantity);
    }
}