﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using WMS.App.Module.Models;

namespace WMS.App.Module.Actions
{
    public class BaseAction
    {
        public IObjectSpace _objectSpace = null;

        public ResponseModel ResponseModel { get; set; }

        public BaseAction(IObjectSpace objectSpace)
        {
            _objectSpace = objectSpace;
            ResponseModel = new ResponseModel
            {
                Result = false
            };
        }

        public T FindData<T>(CriteriaOperator complexCriteria) where T : class
        {
            return _objectSpace.FindObject<T>(complexCriteria, true);
        }
    }
}